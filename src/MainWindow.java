import core.debug.TableModelUtil;
import ui.action.exchange.ExchangeItem;
import ui.action.sell.SellProductRetail;
import ui.action.sell.SellProductWholesale;
import ui.action.sell.SellReturn;
import ui.action.stock.EditStockDialog;
import ui.action.stock.EntryProductInStock;
import ui.action.warranty.ProcessWarrantyDialog;
import ui.manage.*;
import ui.report.ReturnedItemReportDialog;
import ui.report.SellReportInvoiceWise;
import ui.report.SellReportsItemWise;
import ui.report.StockInventoryReport;
import ui.report.exchange.ExchangeReportDateChooser;
import ui.report.offerletter.OfferLetterGenerator;
import ui.report.profit.ProfitDateChooserDialog;
import ui.report.statistics.StatisticsDateChooserDialog;
import ui.report.warranty.WarrantyReportDateChooser;
import ui.tools.AboutDialog;
import ui.tools.BackupDBDialog;
import ui.tools.GenerateSerials;
import ui.tools.ResetDatabaseDialog;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
/*
 * Created by JFormDesigner on Fri Apr 11 10:09:57 PDT 2014
 */


/**
 * @author unknown
 */
public class MainWindow extends JFrame {
    BufferedImage wPic;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
    String toDateStr, fromDateStr;

    //add SellReturn
    private JMenuItem returnMenu;

    //Todo note: if you have installed JFormDesigner, then 'Return Sold Item' menu will disappear.
    //because JFormDesigner

    public MainWindow() {
        //  Debug.disableDebug();
        loadImage();
        initComponents();
        LoginWindow loginDialog = new LoginWindow(MainWindow.this);
        loginDialog.setVisible(true);
        //   this.setVisible(true);

        setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
        toDateStr = simpleDateFormat.format(new Date(System.currentTimeMillis()));
        toDateStr = toDateStr.substring(0, 10).concat(" 00:00:00");
        fromDateStr = toDateStr.substring(0, 10).concat(" 23:59:59");
        loadData();
    }

    public static void main(String args[]) {
        try {
            // Set System L&F
            //  UIManager.setLookAndFeel(new WebLookAndFeel());
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException e) {
            // handle exception
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                MainWindow window = new MainWindow();
                window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                //   window.setExtendedState(JFrame.MAXIMIZED_BOTH);
                //        window.setSize(new Dimension(1124, 700));

                //     window.setVisible(true);
            }
        });

    }

    void loadImage() {
        try {
            wPic = ImageIO.read(new File("res/main_Window_logo.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setIconImage(Toolkit.getDefaultToolkit().getImage("res/icon.png"));
    }

    private void loadData() {


        Connection connection = null;
        Statement statement = null;

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");

            //     String sql = "SELECT PRODUCT , SERIAL, COST, DATE FROM STOCK_INVENTORY WHERE SOLD = 'false' GROUP BY PRODUCT;";
            String sql = "SELECT " +
                    "STOCK_INVENTORY.PRODUCT," +
                    "COUNT(STOCK_INVENTORY.PRODUCT) AS 'NO. OF ITEMS', " +
                    "PRODUCTS.RETAIL AS 'RETAIL PRICE', " +
                    "PRODUCTS.WHOLESALE AS 'WHOLESALE PRICE'" +
                    "FROM STOCK_INVENTORY, PRODUCTS " +
                    "WHERE SOLD = 'false' AND " +
                    "STOCK_INVENTORY.PRODUCT = PRODUCTS.NAME " +
                    "GROUP BY STOCK_INVENTORY.PRODUCT;";
            statement = connection.createStatement();
            table1.setModel(TableModelUtil.resultSetToNonEditableTableModel(statement.executeQuery(sql)));

            //  sql = "SELECT CUSTOMER, PRODUCT, SERIAL, COST FROM SELL_RECORDS WHERE DATE BETWEEN '" + toDateStr + "' AND '" + fromDateStr + "';";
            sql = "SELECT PRODUCT, CUSTOMER, TOTAL(COST) AS TOTAL " +
                    " FROM SELL_RECORDS " +
                    " WHERE DATE BETWEEN '" + toDateStr + "' AND '" + fromDateStr + "'" +
                    "GROUP BY PRODUCT;";

            //   Debug.print(sql, "SQlll");
            table2.setModel(TableModelUtil.resultSetToNonEditableTableModel(statement.executeQuery(sql)));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);

            }
        }
    }

    private void entryStockActionPerformed(ActionEvent e) {
        EntryProductInStock entryProductInStock = new EntryProductInStock(this);
        entryProductInStock.setVisible(true);
    }

    private void menuItem1ActionPerformed(ActionEvent e) {
        SellProductRetail sellProductRetail = new SellProductRetail(this);
        sellProductRetail.setVisible(true);
    }

    private void menuItem2ActionPerformed(ActionEvent e) {
        SellProductWholesale sellProductWholesale = new SellProductWholesale(this);
        sellProductWholesale.setVisible(true);
    }

    private void menuItem4ActionPerformed(ActionEvent e) {
        SellReturn sellReturn = new SellReturn(this);
        sellReturn.setVisible(true);
    }

    private void menuItem5ActionPerformed(ActionEvent e) {
        SellReportsItemWise sellReportsItemWise = new SellReportsItemWise(this);
        sellReportsItemWise.setVisible(true);

    }

    private void addEditCustomersActionPerformed(ActionEvent e) {
        ManageCustomers manageCustomers = new ManageCustomers(this);
        manageCustomers.setVisible(true);
    }

    private void addEditSuppliersActionPerformed(ActionEvent e) {
        ManageSuppliers manageSuppliers = new ManageSuppliers(this);
        manageSuppliers.setVisible(true);
    }

    private void addEditProductsActionPerformed(ActionEvent e) {
        ManageProducts manageProducts = new ManageProducts(this);
        manageProducts.setVisible(true);
    }

    private void menuItem9ActionPerformed(ActionEvent e) {
        ExchangeItem exchangeItem = new ExchangeItem(this);
        exchangeItem.setVisible(true);
    }

    private void menuItemSellReportInvoiceWiseActionPerformed(ActionEvent e) {
        SellReportInvoiceWise sellReportInvoiceWise = new SellReportInvoiceWise(this);
        sellReportInvoiceWise.setVisible(true);
    }

    private void inventoryReportMenuItemActionPerformed(ActionEvent e) {
        StockInventoryReport stockInventoryReport = new StockInventoryReport(this);
        stockInventoryReport.setVisible(true);
    }

    private void warrantyMenuItemActionPerformed(ActionEvent e) {
        ProcessWarrantyDialog processWarrantyDialog = new ProcessWarrantyDialog(this);
        processWarrantyDialog.setVisible(true);
    }

    private void serialGeneratorMenuItemActionPerformed(ActionEvent e) {
        GenerateSerials generateSerials = new GenerateSerials(this);
        generateSerials.setVisible(true);
    }

    private void saleStatisticsMenuItemActionPerformed(ActionEvent e) {
        StatisticsDateChooserDialog stat = new StatisticsDateChooserDialog(this);
        stat.setVisible(true);
    }

    private void profitReportMenuItemActionPerformed(ActionEvent e) {
        ProfitDateChooserDialog profitDateChooserDialog = new ProfitDateChooserDialog(this);
        profitDateChooserDialog.setVisible(true);
    }

    private void resetMenuItemActionPerformed(ActionEvent e) {
        ResetDatabaseDialog reset = new ResetDatabaseDialog(this);
        reset.setVisible(true);
    }

    private void exchangeReportMenuItemActionPerformed(ActionEvent e) {
        ExchangeReportDateChooser exchangeReportDateChooser = new ExchangeReportDateChooser(this);
        exchangeReportDateChooser.setVisible(true);
    }

    private void processedWarrantyReportItemActionPerformed(ActionEvent e) {
        WarrantyReportDateChooser warrantyReportDateChooser = new WarrantyReportDateChooser(this);
        warrantyReportDateChooser.setVisible(true);
    }

    private void addEditUsersActionPerformed(ActionEvent e) {
        ManageUsers manageUsers = new ManageUsers(this);
        manageUsers.setVisible(true);
    }

    private void backupDatabaseMenuItemActionPerformed(ActionEvent e) {
        BackupDBDialog backupDBDialog = new BackupDBDialog(this);
        backupDBDialog.setVisible(true);
    }

    private void thisWindowGainedFocus(WindowEvent e) {
        loadData();
    }

    private void menuItem6ActionPerformed(ActionEvent e) {

    }

    private void menuItem14ActionPerformed(ActionEvent e) {
        AboutDialog aboutDialog = new AboutDialog(this);
        aboutDialog.setVisible(true);
    }

    private void offerletterActionPerformed(ActionEvent e) {
        OfferLetterGenerator offerLetterGenerator = new OfferLetterGenerator(this);
        offerLetterGenerator.setVisible(true);
    }

    private void editStockMenuItemActionPerformed(ActionEvent e) {
        EditStockDialog editStockDialog = new EditStockDialog(this);
        editStockDialog.setVisible(true);
    }

    private void menuItemReturnedItemReportActionPerformed(ActionEvent e) {
        ReturnedItemReportDialog returnedItemReportDialog = new ReturnedItemReportDialog(this);
        returnedItemReportDialog.setVisible(true);
    }

    private void menuItemManageCustomerCatagoryActionPerformed(ActionEvent e) {
        ManageCustomerCatagory manageCustomerCatagory = new ManageCustomerCatagory(this);
        manageCustomerCatagory.setVisible(true);
    }

    private void returnSoldItemMenuItemActionPerformed(ActionEvent e) {
        SellReturn sellReturn = new SellReturn(this);
        sellReturn.setVisible(true);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        menuBar1 = new JMenuBar();
        menu1 = new JMenu();
        entryStock = new JMenuItem();
        menuItem1 = new JMenuItem();
        menuItem2 = new JMenuItem();
        warrantyMenuItem = new JMenuItem();
        menuItem9 = new JMenuItem();
        returnSoldItemMenuItem = new JMenuItem();
        menu2 = new JMenu();
        menuItem5 = new JMenuItem();
        menuItemSellReportInvoiceWise = new JMenuItem();
        saleStatisticsMenuItem = new JMenuItem();
        inventoryReportMenuItem = new JMenuItem();
        profitReportMenuItem = new JMenuItem();
        exchangeReportMenuItem = new JMenuItem();
        processedWarrantyReportItem = new JMenuItem();
        offerletter = new JMenuItem();
        menuItemReturnedItemReport = new JMenuItem();
        menu3 = new JMenu();
        addEditCustomers = new JMenuItem();
        addEditSuppliers = new JMenuItem();
        addEditProducts = new JMenuItem();
        addEditUsers = new JMenuItem();
        editStockMenuItem = new JMenuItem();
        menuItemManageCustomerCatagory = new JMenuItem();
        menu4 = new JMenu();
        serialGeneratorMenuItem = new JMenuItem();
        backupDatabaseMenuItem = new JMenuItem();
        resetMenuItem = new JMenuItem();
        menu5 = new JMenu();
        menuItem14 = new JMenuItem();
        panel1 = new JPanel();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();
        label1 = new JLabel(new ImageIcon(wPic));
        panel2 = new JPanel();
        scrollPane2 = new JScrollPane();
        table2 = new JTable();

        //======== this ========
        setTitle("All Car Decor ");
        addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowGainedFocus(WindowEvent e) {
                thisWindowGainedFocus(e);
            }
        });
        Container contentPane = getContentPane();

        //======== menuBar1 ========
        {

            //======== menu1 ========
            {
                menu1.setText("Invoice");

                //---- entryStock ----
                entryStock.setText("New Stock Entry");
                entryStock.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        entryStockActionPerformed(e);
                    }
                });
                menu1.add(entryStock);

                //---- menuItem1 ----
                menuItem1.setText("New Retail Sale");
                menuItem1.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        menuItem1ActionPerformed(e);
                    }
                });
                menu1.add(menuItem1);

                //---- menuItem2 ----
                menuItem2.setText("New Wholesale Sale");
                menuItem2.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        menuItem2ActionPerformed(e);
                    }
                });
                menu1.add(menuItem2);

                //---- warrantyMenuItem ----
                warrantyMenuItem.setText("Process Warranty Claims");
                warrantyMenuItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        warrantyMenuItemActionPerformed(e);
                    }
                });
                menu1.add(warrantyMenuItem);

                //---- menuItem9 ----
                menuItem9.setText("Exchange Sold Item");
                menuItem9.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        menuItem9ActionPerformed(e);
                    }
                });
                menu1.add(menuItem9);

                //---- returnSoldItemMenuItem ----
                returnSoldItemMenuItem.setText("Return Sold Item");
                returnSoldItemMenuItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        returnSoldItemMenuItemActionPerformed(e);
                    }
                });
                menu1.add(returnSoldItemMenuItem);
            }
            menuBar1.add(menu1);

            //======== menu2 ========
            {
                menu2.setText("Reports");

                //---- menuItem5 ----
                menuItem5.setText("Item Wise Sale Report");
                menuItem5.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        menuItem5ActionPerformed(e);
                    }
                });
                menu2.add(menuItem5);

                //---- menuItemSellReportInvoiceWise ----
                menuItemSellReportInvoiceWise.setText("Invoice Wise Sale Report");
                menuItemSellReportInvoiceWise.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        menuItemSellReportInvoiceWiseActionPerformed(e);
                    }
                });
                menu2.add(menuItemSellReportInvoiceWise);

                //---- saleStatisticsMenuItem ----
                saleStatisticsMenuItem.setText("Sale Statistics");
                saleStatisticsMenuItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        saleStatisticsMenuItemActionPerformed(e);
                    }
                });
                menu2.add(saleStatisticsMenuItem);

                //---- inventoryReportMenuItem ----
                inventoryReportMenuItem.setText("Inventory Report");
                inventoryReportMenuItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        inventoryReportMenuItemActionPerformed(e);
                    }
                });
                menu2.add(inventoryReportMenuItem);

                //---- profitReportMenuItem ----
                profitReportMenuItem.setText("Profit Report");
                profitReportMenuItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        profitReportMenuItemActionPerformed(e);
                    }
                });
                menu2.add(profitReportMenuItem);

                //---- exchangeReportMenuItem ----
                exchangeReportMenuItem.setText("Item Exchange Report");
                exchangeReportMenuItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        exchangeReportMenuItemActionPerformed(e);
                    }
                });
                menu2.add(exchangeReportMenuItem);

                //---- processedWarrantyReportItem ----
                processedWarrantyReportItem.setText("Processed Warranty Report");
                processedWarrantyReportItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        processedWarrantyReportItemActionPerformed(e);
                    }
                });
                menu2.add(processedWarrantyReportItem);

                //---- offerletter ----
                offerletter.setText("Offer Letter Generator");
                offerletter.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        offerletterActionPerformed(e);
                    }
                });
                menu2.add(offerletter);

                //---- menuItemReturnedItemReport ----
                menuItemReturnedItemReport.setText("Returned Items Report");
                menuItemReturnedItemReport.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        menuItemReturnedItemReportActionPerformed(e);
                    }
                });
                menu2.add(menuItemReturnedItemReport);
            }
            menuBar1.add(menu2);

            //======== menu3 ========
            {
                menu3.setText("Manage");

                //---- addEditCustomers ----
                addEditCustomers.setText("Add/Edit Customers");
                addEditCustomers.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        addEditCustomersActionPerformed(e);
                    }
                });
                menu3.add(addEditCustomers);

                //---- addEditSuppliers ----
                addEditSuppliers.setText("Add/Edit Suppliers");
                addEditSuppliers.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        addEditSuppliersActionPerformed(e);
                    }
                });
                menu3.add(addEditSuppliers);

                //---- addEditProducts ----
                addEditProducts.setText("Add/Edit Products");
                addEditProducts.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        addEditProductsActionPerformed(e);
                    }
                });
                menu3.add(addEditProducts);

                //---- addEditUsers ----
                addEditUsers.setText("Add/Edit Users");
                addEditUsers.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        addEditUsersActionPerformed(e);
                    }
                });
                menu3.add(addEditUsers);

                //---- editStockMenuItem ----
                editStockMenuItem.setText("Edit Stock Products");
                editStockMenuItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        editStockMenuItemActionPerformed(e);
                    }
                });
                menu3.add(editStockMenuItem);

                //---- menuItemManageCustomerCatagory ----
                menuItemManageCustomerCatagory.setText("Manage Customer Catagory");
                menuItemManageCustomerCatagory.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        menuItemManageCustomerCatagoryActionPerformed(e);
                    }
                });
                menu3.add(menuItemManageCustomerCatagory);
            }
            menuBar1.add(menu3);

            //======== menu4 ========
            {
                menu4.setText("Tools");

                //---- serialGeneratorMenuItem ----
                serialGeneratorMenuItem.setText("Serial Tag Generator");
                serialGeneratorMenuItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        serialGeneratorMenuItemActionPerformed(e);
                    }
                });
                menu4.add(serialGeneratorMenuItem);

                //---- backupDatabaseMenuItem ----
                backupDatabaseMenuItem.setText("Backup Database");
                backupDatabaseMenuItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        backupDatabaseMenuItemActionPerformed(e);
                    }
                });
                menu4.add(backupDatabaseMenuItem);

                //---- resetMenuItem ----
                resetMenuItem.setText("Reset Database");
                resetMenuItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        resetMenuItemActionPerformed(e);
                    }
                });
                menu4.add(resetMenuItem);
            }
            menuBar1.add(menu4);

            //======== menu5 ========
            {
                menu5.setText("Help");

                //---- menuItem14 ----
                menuItem14.setText("About");
                menuItem14.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        menuItem14ActionPerformed(e);
                    }
                });
                menu5.add(menuItem14);
            }
            menuBar1.add(menu5);
        }

        //======== panel1 ========
        {
            panel1.setBorder(new TitledBorder("Available Items in Stock"));


            //======== scrollPane1 ========
            {
                scrollPane1.setViewportView(table1);
            }

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
                panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 423, Short.MAX_VALUE)
                        .addContainerGap())
            );
            panel1Layout.setVerticalGroup(
                panel1Layout.createParallelGroup()
                    .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 390, Short.MAX_VALUE)
            );
        }

        //---- label1 ----
        label1.setText("'");

        //======== panel2 ========
        {
            panel2.setBorder(new TitledBorder("Sales Today"));

            //======== scrollPane2 ========
            {
                scrollPane2.setViewportView(table2);
            }

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
                panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                        .addContainerGap())
            );
            panel2Layout.setVerticalGroup(
                panel2Layout.createParallelGroup()
                    .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 390, Short.MAX_VALUE)
            );
        }

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addComponent(menuBar1, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 77, Short.MAX_VALUE)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(label1, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                        .addComponent(panel2, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(10, 10, 10))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addComponent(menuBar1, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap())
        );
        setSize(990, 570);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    } // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JMenuBar menuBar1;
    private JMenu menu1;
    private JMenuItem entryStock;
    private JMenuItem menuItem1;
    private JMenuItem menuItem2;
    private JMenuItem warrantyMenuItem;
    private JMenuItem menuItem9;
    private JMenuItem returnSoldItemMenuItem;
    private JMenu menu2;
    private JMenuItem menuItem5;
    private JMenuItem menuItemSellReportInvoiceWise;
    private JMenuItem saleStatisticsMenuItem;
    private JMenuItem inventoryReportMenuItem;
    private JMenuItem profitReportMenuItem;
    private JMenuItem exchangeReportMenuItem;
    private JMenuItem processedWarrantyReportItem;
    private JMenuItem offerletter;
    private JMenuItem menuItemReturnedItemReport;
    private JMenu menu3;
    private JMenuItem addEditCustomers;
    private JMenuItem addEditSuppliers;
    private JMenuItem addEditProducts;
    private JMenuItem addEditUsers;
    private JMenuItem editStockMenuItem;
    private JMenuItem menuItemManageCustomerCatagory;
    private JMenu menu4;
    private JMenuItem serialGeneratorMenuItem;
    private JMenuItem backupDatabaseMenuItem;
    private JMenuItem resetMenuItem;
    private JMenu menu5;
    private JMenuItem menuItem14;
    private JPanel panel1;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JLabel label1;
    private JPanel panel2;
    private JScrollPane scrollPane2;
    private JTable table2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

}
