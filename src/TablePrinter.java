import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.builder.component.VerticalListBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

/**
 * Created by Tareq on 6/5/2014.
 */
public class TablePrinter {
    private void showReport() {

    }

    private JasperReportBuilder build() {
        return report().columns();
    }


    private ComponentBuilder<?, ?> createTitleComponent() {
        VerticalListBuilder list = cmp.verticalList();
        list.add(
                cmp.image("res\\logo.png").setFixedDimension(150, 60),
                cmp.text("All Car Decor").setStyle(stl.style().bold().setFontSize(18)).setHorizontalAlignment(HorizontalAlignment.LEFT),
                cmp.text("145, South Shahjahanpur "),
                cmp.text("Dhaka, Bangladesh"),
                cmp.text("+880 1713115944, +880 1613115944"));

        return list;
    }

}
