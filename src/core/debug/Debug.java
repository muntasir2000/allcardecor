package core.debug;

/**
 * Created by Muntasir on 4/24/2014.
 */
public class Debug {
    private static boolean isDebugEnabled = true; //enable system wide debugging

    public static void enableDebug() {
        isDebugEnabled = true;
    }

    public static void disableDebug() {
        isDebugEnabled = false;
    }

    public static void print(String arg, String tag) {
        if (isDebugEnabled) {
            System.out.println("----------------------------");
            System.out.println(tag + ": " + arg);
            System.out.println("----------------------------");
        }
    }
}
