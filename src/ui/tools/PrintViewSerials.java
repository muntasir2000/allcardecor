package ui.tools;

import core.debug.Debug;
import net.sf.dynamicreports.examples.Templates;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.builder.component.HorizontalListBuilder;
import net.sf.dynamicreports.report.builder.component.VerticalListBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.constant.SplitType;

import java.util.ArrayList;
import java.util.Date;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

/**
 * Created by Tareq on 6/6/2014.
 */
public class PrintViewSerials {

    ArrayList<Integer> serials;
    String productName;
    StyleBuilder style, textStyle, serialStyle;
    int price;

    public PrintViewSerials(ArrayList<Integer> serials, String productName, int price) {
        this.serials = serials;
        this.productName = productName;
        this.price = price;
    }

    public JasperReportBuilder build() {
        style = stl.style().setBorder(stl.penDashed());
        textStyle = stl.style().setFontSize(9).setHorizontalAlignment(HorizontalAlignment.CENTER);
        serialStyle = stl.style().setFontSize(9).setHorizontalAlignment(HorizontalAlignment.CENTER).setBold(true);

        return report()
                .setTemplate(template().setBarcodeHeight(60))
                .title(cmp.text("Serials for " + productName).setStyle(Templates.bold12CenteredStyle)
                        , cmp.text(new Date()).setStyle(Templates.bold12CenteredStyle),
                        cmp.verticalGap(10))
                .summary(
                        barcodes()
                )
                .setSummarySplitType(SplitType.IMMEDIATE);
        //  .show(false);


    }

    private ComponentBuilder<?, ?> barcodes() {
        VerticalListBuilder verticalListBuilder = cmp.verticalList();
        verticalListBuilder.setGap(15);


        int serialIndex = 0;
        for (int i = 1; i <= Math.ceil(serials.size() / 4.0); i++) {
            HorizontalListBuilder list = cmp.horizontalList();
            list.setGap(15);
            int printedThisLine = 0;
            for (int j = 0; j < 4 && j < serials.size() && serialIndex < serials.size(); j++) {
                list.add(barcodeCell(serials.get(serialIndex)));
                printedThisLine++;
                serialIndex++;
                Debug.print("i and j: " + i + " " + j, "XXXXXXXXXXXXXX");
            }
            Debug.print("printedThisLine: " + printedThisLine, "XXXXXXXXXXXXXXX");

            if (printedThisLine < 4) {
                for (int x = 1; x <= 4 - printedThisLine; x++) {
                    list.add(emptyBarcodeCell());
                    Debug.print("Emptybarcode cells: " + x, "XXXXXXXXXXXXX");
                }
            }
            verticalListBuilder.add(list);
        }

        return verticalListBuilder;
    }

    private ComponentBuilder barcodeCell(int serial) {
        VerticalListBuilder productAndSerialText = cmp.verticalList(
                //     cmp.text("All Car Decor").setStyle(textStyle),
                //    cmp.line(),
                cmp.image("res\\serial_logo.png").setFixedDimension(66, 25),
                cmp.text(productName).setStyle(textStyle),
                cmp.text(price + " TK").setStyle(textStyle),
                //    cmp.text("Serial no. ").setStyle(textStyle),
                cmp.text("S.N. " + serial).setStyle(serialStyle)

        );

        HorizontalListBuilder cell = cmp.horizontalList(bcode.dataMatrix(serial + ""), productAndSerialText).setStyle(style);
        return cell;
    }

    private ComponentBuilder emptyBarcodeCell() {
        VerticalListBuilder productAndSerialText = cmp.verticalList(cmp.text(emptyProductName(productName)).setStyle(textStyle), cmp.text("xxxxxx xx. ").setStyle(textStyle.setHorizontalAlignment(HorizontalAlignment.CENTER)), cmp.text(emptySerial(serials.get(0))));
        HorizontalListBuilder cell = cmp.horizontalList(bcode.dataMatrix("XXXXXXX"), productAndSerialText).setStyle(style);
        return cell;
    }

    private String emptyProductName(String productName) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < productName.length(); i++) {
            sb.append("x");
        }
        return sb.toString();
    }

    private String emptySerial(int serial) {
        StringBuilder sb = new StringBuilder();
        while (serial > 0) {
            serial /= 10;
            sb.append("x");
        }
        sb.append("x");
        return sb.toString();
    }


}
