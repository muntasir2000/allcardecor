/*
 * Created by JFormDesigner on Fri Jun 06 00:45:20 ALMT 2014
 */

package ui.tools;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * @author Tareq Muntasir
 */
public class BackupDBDialog extends JDialog {

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JLabel label1;
    private JButton saveDB;
    private JLabel label2;
    private JButton restoreBtn;
    private JButton close;

    public BackupDBDialog(Frame owner) {
        super(owner);
        initComponents();
    }

    public BackupDBDialog(Dialog owner) {
        super(owner);
        initComponents();
    }

    private static void copyFileUsingFileChannels(File source, File dest)
            throws IOException {
        FileChannel inputChannel = null;
        FileChannel outputChannel = null;
        try {
            inputChannel = new FileInputStream(source).getChannel();
            outputChannel = new FileOutputStream(dest).getChannel();
            outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
        } finally {
            if (inputChannel != null)
                inputChannel.close();
            if (outputChannel != null)
                outputChannel.close();
        }
    }

    private void saveDBActionPerformed(ActionEvent e) {
        File toFile;
        File fromFile = new File("records.db");
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.getName().contains(".db") || file.isDirectory();
            }

            @Override
            public String getDescription() {
                return ".db files";
            }
        });
        fileChooser.setAcceptAllFileFilterUsed(false);
        //  fileChooser.setSelectedFile(new File("records.db"));
        int response = fileChooser.showSaveDialog(this);
        if (response == JFileChooser.APPROVE_OPTION) {
            toFile = fileChooser.getSelectedFile();
            String file_name = toFile.toString();
            if (!file_name.endsWith(".db"))
                file_name += ".db";
            File toFileWithExtension = new File(file_name);
            try {
                copyFileUsingFileChannels(fromFile, toFileWithExtension);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }


    }

    private void restoreBtnActionPerformed(ActionEvent e) {
        File fromFile;
        File toFile = new File("records.db");
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.getName().contains(".db") || file.isDirectory();
            }

            @Override
            public String getDescription() {
                return ".db files";
            }
        });
        fileChooser.setAcceptAllFileFilterUsed(false);
        int response = fileChooser.showOpenDialog(this);
        if (response == JFileChooser.APPROVE_OPTION) {
            fromFile = fileChooser.getSelectedFile();
            try {
                copyFileUsingFileChannels(fromFile, toFile);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void closeActionPerformed(ActionEvent e) {
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        label1 = new JLabel();
        saveDB = new JButton();
        label2 = new JLabel();
        restoreBtn = new JButton();
        close = new JButton();

        //======== this ========
        setTitle("Backup/Restore Database");
        setModal(true);
        Container contentPane = getContentPane();

        //---- label1 ----
        label1.setText("Backup database to a file");

        //---- saveDB ----
        saveDB.setText("Select a file");
        saveDB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveDBActionPerformed(e);
            }
        });

        //---- label2 ----
        label2.setText("Restore from a file");

        //---- restoreBtn ----
        restoreBtn.setText("Select a file");
        restoreBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                restoreBtnActionPerformed(e);
            }
        });

        //---- close ----
        close.setText("Close");
        close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                closeActionPerformed(e);
            }
        });

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                        .addComponent(close, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                        .addComponent(label1)
                                                        .addComponent(label2))
                                                .addGap(57, 57, 57)
                                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                        .addComponent(saveDB, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(restoreBtn, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE))))
                                .addContainerGap(8, Short.MAX_VALUE))
        );
        contentPaneLayout.setVerticalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label1)
                                        .addComponent(saveDB))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label2)
                                        .addComponent(restoreBtn))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(close)
                                .addContainerGap())
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
