/*
 * Created by JFormDesigner on Thu Jun 05 23:48:15 ALMT 2014
 */

package ui.tools;

import net.sf.dynamicreports.report.exception.DRException;
import ui.init.AddNewProduct;
import ui.manage.UpdateTableInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Tareq Muntasir
 */
public class GenerateSerials extends JDialog implements UpdateTableInterface {

    Integer range;
    private HashMap<String, Integer> productList;
    private HashMap<String, Integer> priceList;
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JLabel label1;
    private JComboBox cbProduct;
    private JLabel label2;
    private JFormattedTextField countTf;
    private JButton btnAddProduct;
    private JLabel label3;
    private JTextField tfProductCode;
    private JButton btnGenerate;

    public GenerateSerials(Frame owner) {
        super(owner);
        initComponents();
        loadData();
    }

    void loadData() {
        productList = new HashMap<String, Integer>();
        priceList = new HashMap<String, Integer>();
//        cbProduct.addItem("");

        Connection connection = null;
        java.sql.Statement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            statement = connection.createStatement();


            String sql = "SELECT NAME, RETAIL, PRODUCT_CODE FROM PRODUCTS;";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                productList.put(resultSet.getString("NAME"), resultSet.getInt("PRODUCT_CODE"));
                priceList.put(resultSet.getString("NAME"), resultSet.getInt("RETAIL"));
            }

            //ADD ARRAYLIST ELEMENTS TO RESPECTIVE CONTAINERS
            //   cbProduct.removeAllItems();
            for (String ss : productList.keySet()) {

                cbProduct.addItem(ss);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void cbProductActionPerformed(ActionEvent e) {
        if (cbProduct.getSelectedItem() != null)
            tfProductCode.setText(productList.get(cbProduct.getSelectedItem().toString()) + "");
    }

    private void btnAddProductActionPerformed(ActionEvent e) {
        AddNewProduct addNewProduct = new AddNewProduct(this);
        addNewProduct.setVisible(true);
    }

    private void thisWindowGainedFocus(WindowEvent e) {
        cbProduct.removeAllItems();
        loadData();
    }

    private void btnGenerateActionPerformed(ActionEvent e) {
        ArrayList<Integer> listOfSerial = new ArrayList<Integer>();
        Integer count;
        if (countTf.getValue() != null) {
            count = new Integer(countTf.getValue().toString());
        } else {
            JOptionPane.showMessageDialog(this, "Insert how many serials to be generated", "Input Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        range = getLastSerial();
        for (Integer i = 1; i <= count; i++) {
            listOfSerial.add(convert(range, i));
        }
        boolean errorPrintingSerials = false;
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        try {
            String productName = cbProduct.getSelectedItem().toString();

            PrintViewSerials printSerials = new PrintViewSerials(listOfSerial, productName, priceList.get(productName));
            printSerials.build().show(false);

        } catch (DRException e1) {
            this.setCursor(Cursor.getDefaultCursor());
            JOptionPane.showMessageDialog(this, "Fatal Error. Could not generate serials. ", "Error", JOptionPane.ERROR_MESSAGE);
            errorPrintingSerials = true;
        }
        this.setCursor(Cursor.getDefaultCursor());

        if (!errorPrintingSerials) {
            setLastSerial(count);
        }
    }

    private Integer convert(int range, int i) {
        String productCode = tfProductCode.getText();
        String serial = (range + i) + "";
        return new Integer(productCode + serial);
    }

    private int getLastSerial() {
        Connection connection = null;
        java.sql.Statement statement = null;
        int lastSerial = 0;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            statement = connection.createStatement();

            String sql = "SELECT LAST_SERIAL FROM SERIAL_RECORD WHERE PRODUCT = '" + cbProduct.getSelectedItem().toString() + "';";
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                lastSerial = resultSet.getInt("LAST_SERIAL");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return lastSerial;
    }

    private void setLastSerial(int count) {
        Connection connection = null;
        java.sql.Statement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            statement = connection.createStatement();

            String sql = "UPDATE SERIAL_RECORD SET LAST_SERIAL = " + (count + range) + " WHERE PRODUCT = '" + cbProduct.getSelectedItem().toString() + "';";
            statement.executeUpdate(sql);
            statement.close();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        label1 = new JLabel();
        cbProduct = new JComboBox();
        label2 = new JLabel();
        countTf = new JFormattedTextField(NumberFormat.getInstance());
        btnAddProduct = new JButton();
        label3 = new JLabel();
        tfProductCode = new JTextField();
        btnGenerate = new JButton();

        //======== this ========
        setTitle("Generate Serials");
        addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowGainedFocus(WindowEvent e) {
                thisWindowGainedFocus(e);
            }
        });
        Container contentPane = getContentPane();

        //---- label1 ----
        label1.setText("Product");

        //---- cbProduct ----
        cbProduct.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cbProductActionPerformed(e);
            }
        });

        //---- label2 ----
        label2.setText("Number of Serials to be generated");

        //---- btnAddProduct ----
        btnAddProduct.setText("Add New Product");
        btnAddProduct.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnAddProductActionPerformed(e);
            }
        });

        //---- label3 ----
        label3.setText("Product Code");

        //---- tfProductCode ----
        tfProductCode.setEditable(false);

        //---- btnGenerate ----
        btnGenerate.setText("Generate");
        btnGenerate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnGenerateActionPerformed(e);
            }
        });

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                                .addComponent(label1)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                        .addComponent(btnAddProduct)
                                                        .addComponent(cbProduct, GroupLayout.PREFERRED_SIZE, 274, GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                                        .addGroup(GroupLayout.Alignment.LEADING, contentPaneLayout.createSequentialGroup()
                                                                .addComponent(label3)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(tfProductCode))
                                                        .addComponent(label2))
                                                .addGap(33, 33, 33)
                                                .addComponent(countTf, GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)))
                                .addContainerGap())
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addGap(138, 138, 138)
                                .addComponent(btnGenerate, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(142, Short.MAX_VALUE))
        );
        contentPaneLayout.setVerticalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label1)
                                        .addComponent(cbProduct, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAddProduct)
                                .addGap(15, 15, 15)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label3)
                                        .addComponent(tfProductCode, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addGap(32, 32, 32)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label2)
                                        .addComponent(countTf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addGap(27, 27, 27)
                                .addComponent(btnGenerate, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(9, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    @Override
    public void updateTable() {

    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
