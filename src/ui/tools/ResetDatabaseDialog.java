/*
 * Created by JFormDesigner on Thu Jun 05 22:04:55 ALMT 2014
 */

package ui.tools;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Tareq Muntasir
 */
public class ResetDatabaseDialog extends JDialog {


    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JLabel label1;
    private JLabel label2;
    private JButton resetBtn;

    public ResetDatabaseDialog(Frame owner) {
        super(owner);
        initComponents();
    }

    public ResetDatabaseDialog(Dialog owner) {
        super(owner);
        initComponents();
    }

    void reset() {
        Connection connection = null;
        java.sql.Statement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            statement = connection.createStatement();

            String sql = "DELETE FROM CUSTOMER;";
            statement.executeUpdate(sql);
            sql = "DELETE FROM EXCHANGED_ITEM;";
            statement.executeUpdate(sql);
            sql = "DELETE FROM PRODUCTS";
            statement.executeUpdate(sql);
            sql = "DELETE FROM SELL_INVOICE;";
            statement.executeUpdate(sql);
            sql = "DELETE FROM SELL_RECORDS;";
            statement.executeUpdate(sql);
            sql = "DELETE FROM STOCK_INVENTORY;";
            statement.executeUpdate(sql);
            sql = "DELETE FROM STOCK_INVOICE;";
            statement.executeUpdate(sql);
            sql = "DELETE FROM SUPPLIER;";
            statement.executeUpdate(sql);
            sql = "DELETE FROM USERS;";
            statement.executeUpdate(sql);
            sql = "DELETE FROM WARRANTY_PROCESSING;";
            statement.executeUpdate(sql);
            sql = "DELETE FROM SERIAL_RECORD;";
            statement.executeUpdate(sql);
            sql = "DELETE FROM RETURNED_ITEMS";
            statement.executeUpdate(sql);
            sql = "DELETE FROM GRADE";
            statement.executeUpdate(sql);



            //RESET AUTOINCREMENT IDS
            sql = "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'CUSTOMER';";
            statement.executeUpdate(sql);
            sql = "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'EXCHANGED_ITEM';";
            statement.executeUpdate(sql);
            sql = "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'PRODUCTS';";
            statement.executeUpdate(sql);
            sql = "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'RETURNED_ITEMS';";
            statement.executeUpdate(sql);
            sql = "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'SELL_INVOICE';";
            statement.executeUpdate(sql);
            sql = "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'SELL_RECORD';";
            statement.executeUpdate(sql);
            sql = "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'STOCK_INVENTORY';";
            statement.executeUpdate(sql);
            sql = "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'STOCK_INVOICE';";
            statement.executeUpdate(sql);
            sql = "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'SUPPLIER';";
            statement.executeUpdate(sql);
            sql = "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'WARRANTY_PROCESSING';";
            statement.executeUpdate(sql);
            sql = "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'USERS';";
            statement.executeUpdate(sql);
            sql = "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'SERIAL_RECORD';";
            statement.executeUpdate(sql);

            sql = "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'RETURNED_ITEMS';";
            statement.executeUpdate(sql);
            sql = "INSERT INTO USERS (NAME, PASSWORD) VALUES('admin', 'admin');";
            statement.executeUpdate(sql);

            connection.commit();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, "Error resetting database!", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    void vacuum() {
        Connection connection = null;
        Statement statement = null;

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            statement.executeUpdate("VACUUM;");
            connection.commit();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);

            }
        }
    }

    private void resetBtnActionPerformed(ActionEvent e) {
        int response = JOptionPane.showConfirmDialog(this, "Are you sure to delete all records from database? This cannot be undone.", "Confirm", JOptionPane.OK_CANCEL_OPTION);
        if (response == JOptionPane.OK_OPTION) {
            reset();
            vacuum();
            this.dispose();
        }

    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        label1 = new JLabel();
        label2 = new JLabel();
        resetBtn = new JButton();

        //======== this ========
        setTitle("Reset Database");
        setModal(true);
        Container contentPane = getContentPane();

        //---- label1 ----
        label1.setText("Warning! Resetting will delete all records from database.");
        label1.setFont(new Font("Tahoma", Font.PLAIN, 12));

        //---- label2 ----
        label2.setText("This cannot be undone.");
        label2.setFont(new Font("Tahoma", Font.PLAIN, 12));

        //---- resetBtn ----
        resetBtn.setText("Reset");
        resetBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetBtnActionPerformed(e);
            }
        });

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addComponent(label1, GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addComponent(label2)
                                                .addGap(0, 240, Short.MAX_VALUE)))
                                .addContainerGap())
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addGap(149, 149, 149)
                                .addComponent(resetBtn, GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(164, Short.MAX_VALUE))
        );
        contentPaneLayout.setVerticalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(label1, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(label2)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(resetBtn, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(12, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
