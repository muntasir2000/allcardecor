/*
 * Created by JFormDesigner on Sun Apr 19 20:06:12 BDT 2015
 */

package ui.manage;

import net.proteanit.sql.DbUtils;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.HashMap;

/**
 * @author Tareq Muntasir
 */
public class ManageCustomerCatagory extends JDialog {

    private static String UPDATE_TOTAL_AMOUNT_TRIGGER = "UPDATE_CUSTOMER_TOTAL_AMOUNT";
    private static String UPDATE_DISCOUNT_TRIGGER = "UPDATE_CUSTOMER_DISCOUNT";
    private HashMap<Integer, Integer> gradeToTotalRangeMap;
    private HashMap<Integer, Integer> gradeToDiscountRangeMap;

    public ManageCustomerCatagory(Frame owner) {
        super(owner);
        initComponents();
        loadCurrentGradesIntoTable();
        this.setTitle("Manage Customer Catagory");
    }



    private void loadCurrentGradesIntoTable(){
        Connection connection = null;
        Statement statement = null;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");

            String sql = "SELECT " +
                            "GRADE_NAME AS 'Grade Name', " +
                            "GRADE_MAX_AMOUNT AS 'Grade Limit', " +
                            "GRADE_DISCOUNT AS 'Discount' " +
                         "FROM GRADE ;";
            statement = connection.createStatement();

            table1.setModel(DbUtils.resultSetToTableModel(statement.executeQuery(sql)));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            if(connection!=null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Database closing error");
            }
        }
    }


    private boolean isAllInputsValid() {
        String gradeName = null;
        int gradeNo = 0;
        int gradeDiscount = 0;

        //check inputs for grade 1
        gradeName = tfGrade1Name.getText();
        try {
            gradeNo = Integer.parseInt(spGrade1Amount.getValue().toString());
            gradeDiscount = Integer.parseInt(spGrade1Discount.getValue().toString());
        } catch (NumberFormatException e) {
            return false;
        }

        if (gradeName.equals("") && gradeNo == 0 && gradeDiscount == 0) {
            return false;
        }


        //check for inputs in grade 2
        gradeName = tfGrade2Name.getText();
        try {
            gradeNo = Integer.parseInt(spGrade2Amount.getValue().toString());
            gradeDiscount = Integer.parseInt(spGrade2Discount.getValue().toString());
        } catch (NumberFormatException e) {
            return false;
        }

        if (gradeName.equals("") && gradeNo == 0 && gradeDiscount == 0) {
            return false;
        }


        //check for inputs in grade 3
        gradeName = tfGrade3Name.getText();
        try {
            gradeNo = Integer.parseInt(spGrade3Amount.getValue().toString());
            gradeDiscount = Integer.parseInt(spGrade3Discount.getValue().toString());
        } catch (NumberFormatException e) {
            return false;
        }

        if (gradeName.equals("") && gradeNo == 0 && gradeDiscount == 0) {
            return false;
        }


        //check for inputs in grade 4
        gradeName = tfGrade4Name.getText();
        try {
            gradeDiscount = Integer.parseInt(spGrade4Discount.getValue().toString());
        } catch (NumberFormatException e) {
            return false;
        }

        if (gradeName.equals("") && gradeDiscount == 0) {
            return false;
        }

        return true;
    }

    private int determineCustomerGrade(int totalSaleAmount) {
        int maxSaleAmount = 0;
        for (int gradeNo = 1; gradeNo <= gradeToTotalRangeMap.size(); gradeNo++) {
            int gradeAmountUpperLimit = gradeToTotalRangeMap.get(gradeNo);

            //we only set limits upto grade 3. Anything greater than
            // grade 3 Upper limit should be considered as grade 4
            if (gradeAmountUpperLimit > maxSaleAmount)
                maxSaleAmount = gradeAmountUpperLimit;

            if (totalSaleAmount < gradeAmountUpperLimit)
                return gradeNo;
        }

        //the customer is in grade 4. because it exceeds grade 3 upper limit
        if (totalSaleAmount > maxSaleAmount)
            return 4;
        else
            return 0; // something bad happened. 0 should never be returned.
    }

    private PreparedStatement updateSingleCustomerInfo(Connection connection, int customerId, int customerTotalSale) throws SQLException {
        PreparedStatement statement = null;

        int newGrade = determineCustomerGrade(customerTotalSale);
        int newDiscount = gradeToDiscountRangeMap.get(newGrade);

        String sql = "UPDATE CUSTOMER " +
                "SET " +
                "GRADE = ?," +
                "DISCOUNT = ? " +
                "WHERE ID = ?;";

        statement = connection.prepareStatement(sql);
        statement.setInt(1, newGrade);
        statement.setInt(2, newDiscount);
        statement.setInt(3, customerId);

        return statement;

    }

    private void recalculateExistingCustomersGradeAndDiscount(Connection connection) throws SQLException {

        Statement statement = null;
        ResultSet rs;

        String sqlCustomerSQL = "SELECT * FROM CUSTOMER ";
        statement = connection.createStatement();
        rs = statement.executeQuery(sqlCustomerSQL);

        while (rs.next()) {
            int totalAmount = rs.getInt("TOTALAMOUNT");
            int id = rs.getInt("ID");

            updateSingleCustomerInfo(connection, id, totalAmount).execute();
        }

        rs.close();
        statement.close();
    }

    private void publishToGradesTable(Connection connection) throws SQLException{

        //first delete all data from grade table.
        Statement deleteStatement = null;
        String deleteSQL = "DELETE FROM GRADE;";
        deleteStatement = connection.createStatement();
        deleteStatement.execute(deleteSQL);



        PreparedStatement preparedStatement = null;
        String sql = "INSERT INTO GRADE " +
                "(GRADE_NO, GRADE_NAME, GRADE_MAX_AMOUNT, GRADE_DISCOUNT) " +
                "VALUES(?, ?, ?, ? );";
        preparedStatement = connection.prepareStatement(sql);

        //enter grade 1 infos
        preparedStatement.setInt(1,1);
        preparedStatement.setString(2, tfGrade1Name.getText());
        preparedStatement.setInt(3, gradeToTotalRangeMap.get(1));
        preparedStatement.setInt(4, gradeToDiscountRangeMap.get(1));

        preparedStatement.execute();

        //enter grade 2 infos
        preparedStatement.setInt(1,2);
        preparedStatement.setString(2, tfGrade2Name.getText());
        preparedStatement.setInt(3, gradeToTotalRangeMap.get(2));
        preparedStatement.setInt(4, gradeToDiscountRangeMap.get(2));

        preparedStatement.execute();

        //enter grade 3 infos
        preparedStatement.setInt(1,3);
        preparedStatement.setString(2, tfGrade3Name.getText());
        preparedStatement.setInt(3, gradeToTotalRangeMap.get(3));
        preparedStatement.setInt(4, gradeToDiscountRangeMap.get(3));

        preparedStatement.execute();

        //enter grade 4 infos
        preparedStatement.setInt(1,4);
        preparedStatement.setString(2, tfGrade4Name.getText());
        preparedStatement.setInt(3, -1); // because no upper limit exists for grade 4
        preparedStatement.setInt(4, gradeToDiscountRangeMap.get(4));

        preparedStatement.execute();

    }

    private void btnSaveActionPerformed(ActionEvent actionEvent) {

        if (isAllInputsValid()) {
            boolean noExceptionOccured = true;
            gradeToDiscountRangeMap = new HashMap<Integer, Integer>();
            gradeToTotalRangeMap = new HashMap<Integer, Integer>();

            int grade1Amount = Integer.parseInt(spGrade1Amount.getValue().toString());
            int grade2Amount = Integer.parseInt(spGrade2Amount.getValue().toString());
            int grade3Amount = Integer.parseInt(spGrade3Amount.getValue().toString());

            int grade1Discount = Integer.parseInt(spGrade1Discount.getValue().toString());
            int grade2Discount = Integer.parseInt(spGrade2Discount.getValue().toString());
            int grade3Discount = Integer.parseInt(spGrade3Discount.getValue().toString());
            int grade4Discount = Integer.parseInt(spGrade4Discount.getValue().toString());

            gradeToTotalRangeMap.put(1, grade1Amount);
            gradeToTotalRangeMap.put(2, grade2Amount);
            gradeToTotalRangeMap.put(3, grade3Amount);

            gradeToDiscountRangeMap.put(1, grade1Discount);
            gradeToDiscountRangeMap.put(2, grade2Discount);
            gradeToDiscountRangeMap.put(3, grade3Discount);
            gradeToDiscountRangeMap.put(4, grade4Discount);

            Connection connection = null;
            Statement totalAmountTriggerStatement = null;
            Statement discountTriggerStatement = null;
            Statement deleteExistingTriggerStatement1 = null;
            Statement deleteExistingTriggerStatement2 = null;

            try {
                Class.forName("org.sqlite.JDBC");
                connection = DriverManager.getConnection("jdbc:sqlite:records.db");
                connection.setAutoCommit(false);

                //first delete the existing triggers
                String deleteExistingTriggersSQL1 = "DROP TRIGGER " + UPDATE_TOTAL_AMOUNT_TRIGGER + ";";

                String deleteExistingTriggerSQL2 = "DROP TRIGGER " + UPDATE_DISCOUNT_TRIGGER + ";";

                deleteExistingTriggerStatement1 = connection.createStatement();
                deleteExistingTriggerStatement2 = connection.createStatement();

                deleteExistingTriggerStatement1.execute(deleteExistingTriggersSQL1);
                deleteExistingTriggerStatement2.execute(deleteExistingTriggerSQL2);

                String updateCustomerTotalTriggerSQL =
                        "CREATE TRIGGER " +
                                UPDATE_TOTAL_AMOUNT_TRIGGER +
                                " INSERT ON SELL_INVOICE " +
                                "BEGIN " +
                                "UPDATE CUSTOMER " +
                                "SET " +
                                "TOTALAMOUNT = TOTALAMOUNT + new.TOTAL, " +

                                "GRADE = CASE " +
                                "WHEN TOTALAMOUNT< " + grade1Amount + " THEN " +
                                "1 " +
                                "WHEN TOTALAMOUNT< " + grade2Amount + " THEN " +
                                "2 " +
                                "WHEN TOTALAMOUNT< " + grade3Amount + " THEN " +
                                "3 " +
                                "ELSE " +
                                "4 " +
                                "END " +
                                "WHERE NAME = new.CUSTOMER ;" +
                                "END;";

                totalAmountTriggerStatement = connection.createStatement();

                totalAmountTriggerStatement.executeUpdate(updateCustomerTotalTriggerSQL);

                String updateCustomerDiscountSQL =
                        "CREATE TRIGGER " +
                                UPDATE_DISCOUNT_TRIGGER +
                                " UPDATE OF GRADE ON CUSTOMER " +
                                " BEGIN " +
                                "UPDATE CUSTOMER " +
                                "SET " +
                                "DISCOUNT = CASE " +
                                "WHEN new.GRADE = 1 THEN " +
                                "DISCOUNT = " + grade1Amount + " " +
                                "WHEN new.GRADE = 2 THEN " +
                                "DISCOUNT = " + grade2Discount + " " +
                                "WHEN new.GRADE = 3 THEN " +
                                "DISCOUNT = " + grade3Discount + " " +
                                "WHEN new.GRADE = 4 THEN " +
                                "DISCOUNT = " + grade4Discount + " " +
                                "END " +
                                "WHERE Id = new.ID ;" +
                                "END;";
                discountTriggerStatement = connection.createStatement();

                discountTriggerStatement.executeUpdate(updateCustomerDiscountSQL);

                recalculateExistingCustomersGradeAndDiscount(connection);

                connection.commit();

                publishToGradesTable(connection);

                connection.setAutoCommit(true);

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                noExceptionOccured = false;
            } catch (SQLException e) {
                if(connection!=null){
                    try {
                        connection.rollback();
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                }
                e.printStackTrace();
                noExceptionOccured = false;
            } finally {
                try {
                    if (totalAmountTriggerStatement != null) {
                        totalAmountTriggerStatement.close();
                    }
                    if(discountTriggerStatement != null){
                        discountTriggerStatement.close();
                    }
                    if(deleteExistingTriggerStatement1 != null){
                        deleteExistingTriggerStatement1.close();
                    }
                    if(deleteExistingTriggerStatement2!= null){
                        deleteExistingTriggerStatement2.close();
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                    System.out.println("Database closing error");
                    noExceptionOccured = false;
                }
            }
            if(noExceptionOccured){
                this.dispose();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Invalid input. Please check all the fields and insert proper value");
        }

    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        btnSave = new JButton();
        panel5 = new JPanel();
        panel4 = new JPanel();
        label10 = new JLabel();
        tfGrade4Name = new JTextField();
        label11 = new JLabel();
        spGrade4Amount = new JSpinner();
        label12 = new JLabel();
        spGrade4Discount = new JSpinner();
        label15 = new JLabel();
        panel3 = new JPanel();
        label4 = new JLabel();
        tfGrade2Name = new JTextField();
        label5 = new JLabel();
        spGrade2Amount = new JSpinner();
        label6 = new JLabel();
        spGrade2Discount = new JSpinner();
        panel2 = new JPanel();
        label7 = new JLabel();
        tfGrade3Name = new JTextField();
        label8 = new JLabel();
        spGrade3Amount = new JSpinner();
        label9 = new JLabel();
        spGrade3Discount = new JSpinner();
        panel1 = new JPanel();
        label1 = new JLabel();
        tfGrade1Name = new JTextField();
        label2 = new JLabel();
        spGrade1Amount = new JSpinner();
        label3 = new JLabel();
        spGrade1Discount = new JSpinner();
        panel6 = new JPanel();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();
        panel7 = new JPanel();
        label14 = new JLabel();
        label16 = new JLabel();
        label17 = new JLabel();

        //======== this ========
        Container contentPane = getContentPane();

        //---- btnSave ----
        btnSave.setText("Save");
        btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnSaveActionPerformed(e);
            }
        });

        //======== panel5 ========
        {


            //======== panel4 ========
            {
                panel4.setBorder(new TitledBorder("Grade 4"));

                //---- label10 ----
                label10.setText("Grade Name");

                //---- label11 ----
                label11.setText("Grade Amount");

                //---- spGrade4Amount ----
                spGrade4Amount.setEnabled(false);

                //---- label12 ----
                label12.setText("Grade Discount");

                //---- label15 ----
                label15.setText("* Not needed. ");

                GroupLayout panel4Layout = new GroupLayout(panel4);
                panel4.setLayout(panel4Layout);
                panel4Layout.setHorizontalGroup(
                    panel4Layout.createParallelGroup()
                        .addGroup(panel4Layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(panel4Layout.createParallelGroup()
                                .addGroup(panel4Layout.createSequentialGroup()
                                    .addComponent(label10)
                                    .addGap(18, 18, 18)
                                    .addComponent(tfGrade4Name, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE))
                                .addGroup(panel4Layout.createSequentialGroup()
                                    .addGroup(panel4Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(GroupLayout.Alignment.LEADING, panel4Layout.createSequentialGroup()
                                            .addComponent(label12)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(spGrade4Discount))
                                        .addGroup(GroupLayout.Alignment.LEADING, panel4Layout.createSequentialGroup()
                                            .addComponent(label11)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(spGrade4Amount, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(label15)))
                            .addContainerGap(24, Short.MAX_VALUE))
                );
                panel4Layout.setVerticalGroup(
                    panel4Layout.createParallelGroup()
                        .addGroup(panel4Layout.createSequentialGroup()
                            .addGroup(panel4Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label10)
                                .addComponent(tfGrade4Name, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(panel4Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label11)
                                .addComponent(spGrade4Amount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(label15))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(panel4Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label12)
                                .addComponent(spGrade4Discount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addContainerGap())
                );
            }

            //======== panel3 ========
            {
                panel3.setBorder(new TitledBorder("Grade 2"));

                //---- label4 ----
                label4.setText("Grade Name");

                //---- label5 ----
                label5.setText("Grade Amount");

                //---- label6 ----
                label6.setText("Grade Discount");

                GroupLayout panel3Layout = new GroupLayout(panel3);
                panel3.setLayout(panel3Layout);
                panel3Layout.setHorizontalGroup(
                    panel3Layout.createParallelGroup()
                        .addGroup(panel3Layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(panel3Layout.createParallelGroup()
                                .addComponent(label4)
                                .addComponent(label5)
                                .addComponent(label6))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(panel3Layout.createParallelGroup()
                                .addComponent(spGrade2Amount, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
                                .addComponent(spGrade2Discount, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
                                .addComponent(tfGrade2Name, GroupLayout.PREFERRED_SIZE, 319, GroupLayout.PREFERRED_SIZE))
                            .addGap(32, 32, 32))
                );
                panel3Layout.setVerticalGroup(
                    panel3Layout.createParallelGroup()
                        .addGroup(panel3Layout.createSequentialGroup()
                            .addGroup(panel3Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label4)
                                .addComponent(tfGrade2Name, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(panel3Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label5)
                                .addComponent(spGrade2Amount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addGap(10, 10, 10)
                            .addGroup(panel3Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label6)
                                .addComponent(spGrade2Discount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addGap(0, 14, Short.MAX_VALUE))
                );
            }

            //======== panel2 ========
            {
                panel2.setBorder(new TitledBorder("Grade 3"));

                //---- label7 ----
                label7.setText("Grade Name");

                //---- label8 ----
                label8.setText("Grade Amount");

                //---- label9 ----
                label9.setText("Grade Discount");

                GroupLayout panel2Layout = new GroupLayout(panel2);
                panel2.setLayout(panel2Layout);
                panel2Layout.setHorizontalGroup(
                    panel2Layout.createParallelGroup()
                        .addGroup(panel2Layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(panel2Layout.createParallelGroup()
                                .addGroup(panel2Layout.createSequentialGroup()
                                    .addComponent(label7)
                                    .addGap(18, 18, 18)
                                    .addComponent(tfGrade3Name, GroupLayout.PREFERRED_SIZE, 318, GroupLayout.PREFERRED_SIZE))
                                .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(GroupLayout.Alignment.LEADING, panel2Layout.createSequentialGroup()
                                        .addComponent(label9)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(spGrade3Discount))
                                    .addGroup(GroupLayout.Alignment.LEADING, panel2Layout.createSequentialGroup()
                                        .addComponent(label8)
                                        .addGap(11, 11, 11)
                                        .addComponent(spGrade3Amount, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE))))
                            .addContainerGap(18, Short.MAX_VALUE))
                );
                panel2Layout.setVerticalGroup(
                    panel2Layout.createParallelGroup()
                        .addGroup(panel2Layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label7)
                                .addComponent(tfGrade3Name, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label8)
                                .addComponent(spGrade3Amount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label9)
                                .addComponent(spGrade3Discount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );
            }

            //======== panel1 ========
            {
                panel1.setBorder(new TitledBorder("Grade 1"));

                //---- label1 ----
                label1.setText("Grade Name");

                //---- label2 ----
                label2.setText("Grade Amount");

                //---- label3 ----
                label3.setText("Grade Discount");

                GroupLayout panel1Layout = new GroupLayout(panel1);
                panel1.setLayout(panel1Layout);
                panel1Layout.setHorizontalGroup(
                    panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(panel1Layout.createParallelGroup()
                                .addComponent(label1)
                                .addComponent(label2)
                                .addComponent(label3))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(panel1Layout.createParallelGroup()
                                .addComponent(spGrade1Amount, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
                                .addComponent(spGrade1Discount, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
                                .addComponent(tfGrade1Name, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE))
                            .addGap(162, 162, 162))
                );
                panel1Layout.setVerticalGroup(
                    panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label1)
                                .addComponent(tfGrade1Name, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label2)
                                .addComponent(spGrade1Amount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label3)
                                .addComponent(spGrade1Discount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addContainerGap())
                );
            }

            GroupLayout panel5Layout = new GroupLayout(panel5);
            panel5.setLayout(panel5Layout);
            panel5Layout.setHorizontalGroup(
                panel5Layout.createParallelGroup()
                    .addGroup(panel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panel5Layout.createParallelGroup()
                            .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 441, GroupLayout.PREFERRED_SIZE)
                            .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel5Layout.createParallelGroup()
                            .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
                            .addComponent(panel4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
            );
            panel5Layout.setVerticalGroup(
                panel5Layout.createParallelGroup()
                    .addGroup(panel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panel5Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addComponent(panel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel5Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                            .addComponent(panel4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
            panel5Layout.linkSize(SwingConstants.VERTICAL, new Component[] {panel1, panel3});
        }

        //======== panel6 ========
        {
            panel6.setBorder(new TitledBorder("Current Customer Catagories"));

            //======== scrollPane1 ========
            {

                //---- table1 ----
                table1.setPreferredScrollableViewportSize(new Dimension(200, 200));
                scrollPane1.setViewportView(table1);
            }

            GroupLayout panel6Layout = new GroupLayout(panel6);
            panel6.setLayout(panel6Layout);
            panel6Layout.setHorizontalGroup(
                panel6Layout.createParallelGroup()
                    .addGroup(GroupLayout.Alignment.TRAILING, panel6Layout.createSequentialGroup()
                        .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 431, Short.MAX_VALUE)
                        .addContainerGap())
            );
            panel6Layout.setVerticalGroup(
                panel6Layout.createParallelGroup()
                    .addGroup(GroupLayout.Alignment.TRAILING, panel6Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
            );
        }

        //======== panel7 ========
        {
            panel7.setBorder(new TitledBorder("Note"));

            //---- label14 ----
            label14.setText("1. Grade Names example - Silver, Bronze, Gold, Platinum");

            //---- label16 ----
            label16.setText("2. Grade amount is the maximum range a customer must buy to upgrade ");

            //---- label17 ----
            label17.setText("to the next grade. ");

            GroupLayout panel7Layout = new GroupLayout(panel7);
            panel7.setLayout(panel7Layout);
            panel7Layout.setHorizontalGroup(
                panel7Layout.createParallelGroup()
                    .addGroup(panel7Layout.createSequentialGroup()
                        .addGroup(panel7Layout.createParallelGroup()
                            .addGroup(panel7Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(panel7Layout.createParallelGroup()
                                    .addComponent(label14)
                                    .addComponent(label16)))
                            .addGroup(panel7Layout.createSequentialGroup()
                                .addGap(22, 22, 22)
                                .addComponent(label17)))
                        .addContainerGap(26, Short.MAX_VALUE))
            );
            panel7Layout.setVerticalGroup(
                panel7Layout.createParallelGroup()
                    .addGroup(panel7Layout.createSequentialGroup()
                        .addComponent(label14)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(label16)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(label17)
                        .addGap(0, 24, Short.MAX_VALUE))
            );
        }

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panel6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 308, Short.MAX_VALUE)
                            .addComponent(btnSave, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
                            .addGap(32, 32, 32))
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(panel7, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addContainerGap())))
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addComponent(panel5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panel5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(panel6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addContainerGap(40, Short.MAX_VALUE))
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(panel7, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnSave)
                            .addGap(26, 26, 26))))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JButton btnSave;
    private JPanel panel5;
    private JPanel panel4;
    private JLabel label10;
    private JTextField tfGrade4Name;
    private JLabel label11;
    private JSpinner spGrade4Amount;
    private JLabel label12;
    private JSpinner spGrade4Discount;
    private JLabel label15;
    private JPanel panel3;
    private JLabel label4;
    private JTextField tfGrade2Name;
    private JLabel label5;
    private JSpinner spGrade2Amount;
    private JLabel label6;
    private JSpinner spGrade2Discount;
    private JPanel panel2;
    private JLabel label7;
    private JTextField tfGrade3Name;
    private JLabel label8;
    private JSpinner spGrade3Amount;
    private JLabel label9;
    private JSpinner spGrade3Discount;
    private JPanel panel1;
    private JLabel label1;
    private JTextField tfGrade1Name;
    private JLabel label2;
    private JSpinner spGrade1Amount;
    private JLabel label3;
    private JSpinner spGrade1Discount;
    private JPanel panel6;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JPanel panel7;
    private JLabel label14;
    private JLabel label16;
    private JLabel label17;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
