/*
 * Created by JFormDesigner on Tue May 27 16:47:44 ALMT 2014
 */

package ui.manage;

import core.debug.Debug;
import core.debug.TableModelUtil;
import ui.init.AddNewSupplier;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.sql.*;
import java.text.MessageFormat;

/**
 * @author Tareq Muntasir
 */
public class ManageSuppliers extends JDialog implements UpdateTableInterface {
    TableModel tableModel;
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JButton addNew;
    private JButton edit;
    private JButton print;
    private JPanel buttonBar;
    private JButton okButton;

    public ManageSuppliers(Frame owner) {
        super(owner);
        initComponents();
        loadData();
        setupMisc();
    }

    void setupMisc() {
        table1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    void loadData() {
        Connection connection = null;
        Statement statement = null;
        ResultSet rs;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            String sql = "SELECT NAME, ADDRESS, CONTACT FROM SUPPLIER;";
            statement = connection.createStatement();
            rs = statement.executeQuery(sql);

            tableModel = TableModelUtil.resultSetToNonEditableTableModel(rs);
            table1.setModel(tableModel);
            rs.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
//                    Debug.print("statement null", "dbmanager");
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Database closing error");
            }
        }
    }

    private void deleteSupplier(String name) {
        Connection connection = null;
        Statement statement = null;
        //     ResultSet rs;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            String sql = "DELETE FROM SUPPLIER WHERE NAME = '" + name + "';";
            statement = connection.createStatement();
            statement.executeUpdate(sql);
            //  rs.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
//                    Debug.print("statement null", "dbmanager");
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Database closing error");
            }
        }
    }

    private void editSupplier(String name) {
        EditSupplierDialog editSupplierDialog = new EditSupplierDialog(this, name);
        editSupplierDialog.setVisible(true);
    }

    private void editActionPerformed(ActionEvent e) {
        int selectedRow = table1.getSelectedRow();
        if (selectedRow == -1) {
            JOptionPane.showMessageDialog(this, "No Supplier selected", "Info", JOptionPane.OK_OPTION);
            return;
        }
        editSupplier(tableModel.getValueAt(selectedRow, 0).toString());
    }

    private void deleteActionPerformed(ActionEvent e) {
        Debug.print("delete supplier", "XX");
        int selectedRow = table1.getSelectedRow();
        if (selectedRow == -1) {
            JOptionPane.showMessageDialog(this, "No Supplier selected", "Info", JOptionPane.OK_OPTION);
            return;
        }
        int confirmStatus = JOptionPane.showConfirmDialog(this, "Are you sure to delete selected Supplier?", "Delete Supplier", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (confirmStatus != JOptionPane.OK_OPTION) {
            return;
        }
        String name;

        name = tableModel.getValueAt(selectedRow, 0).toString();
        deleteSupplier(name);
        DefaultTableModel tableModel1 = (DefaultTableModel) table1.getModel();
        //  tableModel1.fireTableDataChanged();
        tableModel1.removeRow(selectedRow);
    }

    private void addNewActionPerformed(ActionEvent e) {
        AddNewSupplier newSupplier = new AddNewSupplier(this);
        newSupplier.setVisible(true);
    }

    @Override
    public void updateTable() {
        loadData();
    }

    private void okButtonActionPerformed(ActionEvent e) {
        this.dispose();
    }

    private void cancelButtonActionPerformed(ActionEvent e) {
        this.dispose();
    }

    private void printActionPerformed(ActionEvent e) {
        MessageFormat header = new MessageFormat("Supplier List");
        MessageFormat footer = new MessageFormat("Page - {0}");

        try {
            table1.print(JTable.PrintMode.NORMAL, header, footer);
        } catch (PrinterException e1) {
            e1.printStackTrace();
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();
        addNew = new JButton();
        edit = new JButton();
        print = new JButton();
        buttonBar = new JPanel();
        okButton = new JButton();

        //======== this ========
        setTitle("Manage Suppliers");
        setModal(true);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));


            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setBorder(new TitledBorder("Supplier List"));

                //======== scrollPane1 ========
                {
                    scrollPane1.setViewportView(table1);
                }

                //---- addNew ----
                addNew.setText("Add New Supplier");
                addNew.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        addNewActionPerformed(e);
                    }
                });

                //---- edit ----
                edit.setText("Edit Selected");
                edit.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        editActionPerformed(e);
                    }
                });

                //---- print ----
                print.setText("Print Table");
                print.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        printActionPerformed(e);
                    }
                });

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                        .addComponent(print, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
                                                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                        .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                        .addGap(38, 38, 38)
                                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                                .addComponent(addNew, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(edit, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                        .addGap(25, 25, 25))))
                );
                contentPanelLayout.setVerticalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 353, GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(print)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(addNew)
                                        .addGap(18, 18, 18)
                                        .addComponent(edit)
                                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[]{0, 85, 80};
                ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[]{1.0, 0.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        okButtonActionPerformed(e);
                    }
                });
                buttonBar.add(okButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
