/*
 * Created by JFormDesigner on Thu Jun 05 12:19:28 ALMT 2014
 */

package ui.manage;

import core.debug.TableModelUtil;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

/**
 * @author Tareq Muntasir
 */
public class ManageUsers extends JDialog implements UpdateTableInterface {


    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JLabel label1;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JButton addNewBtn;
    private JButton deleteUserBtn;
    private JButton editUserBtn;
    private JButton button1;

    public ManageUsers(Frame owner) {
        super(owner);
        initComponents();
        loadData();
    }

    public ManageUsers(Dialog owner) {
        super(owner);
        initComponents();
    }

    void loadData() {
        table1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        Connection connection = null;
        Statement statement = null;
        ResultSet rs;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            String sql = "SELECT NAME FROM USERS;";
            statement = connection.createStatement();
            rs = statement.executeQuery(sql);

            TableModel tableModel = TableModelUtil.resultSetToNonEditableTableModel(rs);
            table1.setModel(tableModel);
            rs.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
//                    Debug.print("statement null", "dbmanager");
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Database closing error");
            }
        }
    }

    private void editUserBtnActionPerformed(ActionEvent e) {
        int selectedRow = table1.getSelectedRow();
        EditUerDialog edit = new EditUerDialog(this, table1.getModel().getValueAt(selectedRow, 0).toString());
        edit.setVisible(true);
    }

    private void addNewBtnActionPerformed(ActionEvent e) {
        AddNewUserDialog newUser = new AddNewUserDialog(this);
        newUser.setVisible(true);
    }

    private void deleteUserBtnActionPerformed(ActionEvent ex) {

        String selected = table1.getModel().getValueAt(table1.getSelectedRow(), 0).toString();
        if (table1.getRowCount() == 1 && table1.getSelectedRow() == 0) {
            JOptionPane.showMessageDialog(this, "Cannot delete the only user. It will prevent anyone from logging in.", "Not allowed", JOptionPane.OK_OPTION);
            return;
        }
        Connection connection = null;
        Statement statement = null;

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            String sql = "DELETE FROM USERS WHERE NAME = '" + selected + "';";
            statement = connection.createStatement();
            statement.executeUpdate(sql);

            connection.commit();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
//                    Debug.print("statement null", "dbmanager");
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Database closing error");
            }
        }
        loadData();
    }

    @Override
    public void updateTable() {
        loadData();
    }

    private void button1ActionPerformed(ActionEvent e) {
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        label1 = new JLabel();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();
        addNewBtn = new JButton();
        deleteUserBtn = new JButton();
        editUserBtn = new JButton();
        button1 = new JButton();

        //======== this ========
        setTitle("Manage Users");
        setModal(true);
        Container contentPane = getContentPane();

        //---- label1 ----
        label1.setText("Available Users");

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(table1);
        }

        //---- addNewBtn ----
        addNewBtn.setText("Add New");
        addNewBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addNewBtnActionPerformed(e);
            }
        });

        //---- deleteUserBtn ----
        deleteUserBtn.setText("Delete");
        deleteUserBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteUserBtnActionPerformed(e);
            }
        });

        //---- editUserBtn ----
        editUserBtn.setText(" Edit Selected");
        editUserBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editUserBtnActionPerformed(e);
            }
        });

        //---- button1 ----
        button1.setText("OK");
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button1ActionPerformed(e);
            }
        });

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addComponent(label1)
                                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                                                .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 151, GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                                        .addComponent(addNewBtn, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(deleteUserBtn)
                                                                        .addComponent(editUserBtn)))
                                                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                                                .addGap(0, 221, Short.MAX_VALUE)
                                                                .addComponent(button1, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)))
                                                .addGap(18, 18, 18))))
        );
        contentPaneLayout.linkSize(SwingConstants.HORIZONTAL, new Component[]{addNewBtn, deleteUserBtn, editUserBtn});
        contentPaneLayout.setVerticalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(label1)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addComponent(addNewBtn)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(deleteUserBtn)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(editUserBtn)))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                                .addComponent(button1)
                                .addGap(14, 14, 14))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
