/*
 * Created by JFormDesigner on Wed May 28 01:31:26 ALMT 2014
 */

package ui.manage;

import core.debug.TableModelUtil;
import ui.init.AddNewProduct;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;

/**
 * @author Tareq Muntasir
 */
public class ManageProducts extends JDialog implements UpdateTableInterface {


    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JButton addNew;
    private JButton edit;
    private JButton print;
    private JPanel buttonBar;
    private JButton okButton;

    public ManageProducts(Frame owner) {
        super(owner);
        initComponents();
        loadData();
        table1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    public ManageProducts(Dialog owner) {
        super(owner);
        initComponents();
    }

    private void addNewActionPerformed(ActionEvent e) {
        AddNewProduct addNewProduct = new AddNewProduct(this);
        addNewProduct.setVisible(true);
    }

    private void editActionPerformed(ActionEvent e) {
        String selectedName = table1.getValueAt(table1.getSelectedRow(), 0).toString();
        EditProductDialog editProductDialog = new EditProductDialog(this, selectedName);
        editProductDialog.setVisible(true);
    }

    private void okButtonActionPerformed(ActionEvent e) {
        dispose();
    }

    void loadData() {
        Connection connection = null;
        java.sql.Statement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");

            statement = connection.createStatement();

            String sql = "SELECT NAME, DESCRIPTION, WHOLESALE, RETAIL, PRODUCT_CODE FROM PRODUCTS;";
            ResultSet resultSet = statement.executeQuery(sql);
            table1.setModel(TableModelUtil.resultSetToNonEditableTableModel(resultSet));

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void updateTable() {
        loadData();
    }

    private void printActionPerformed(ActionEvent e) {
        MessageFormat header = new MessageFormat("Product List");
        MessageFormat footer = new MessageFormat("Page - {0}");

        try {
            table1.print(JTable.PrintMode.NORMAL, header, footer);
        } catch (PrinterException e1) {
            e1.printStackTrace();
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();
        addNew = new JButton();
        edit = new JButton();
        print = new JButton();
        buttonBar = new JPanel();
        okButton = new JButton();

        //======== this ========
        setTitle("Manage Products");
        setModal(true);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));


            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setBorder(new TitledBorder("Product List"));

                //======== scrollPane1 ========
                {
                    scrollPane1.setViewportView(table1);
                }

                //---- addNew ----
                addNew.setText("Add New");
                addNew.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        addNewActionPerformed(e);
                    }
                });

                //---- edit ----
                edit.setText("Edit Selected");
                edit.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        editActionPerformed(e);
                    }
                });

                //---- print ----
                print.setText("Print Table");
                print.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        printActionPerformed(e);
                    }
                });

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                        .addComponent(print, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
                                                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                        .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                        .addGap(38, 38, 38)
                                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                                .addComponent(addNew, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(edit, GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE))
                                                        .addGap(25, 25, 25))))
                );
                contentPanelLayout.setVerticalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 353, GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(print)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(addNew)
                                        .addGap(18, 18, 18)
                                        .addComponent(edit)
                                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[]{0, 85, 80};
                ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[]{1.0, 0.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        okButtonActionPerformed(e);
                    }
                });
                buttonBar.add(okButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
