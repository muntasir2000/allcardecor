/*
 * Created by JFormDesigner on Thu Jun 05 13:56:31 ALMT 2014
 */

package ui.manage;

import core.debug.Debug;
import ui.action.sell.RegexFormatter;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.text.NumberFormat;

/**
 * @author Tareq Muntasir
 */
public class EditProductDialog extends JDialog {
    String nameStr = null;
    UpdateTableInterface _interface;
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel label3;
    private JTextField name;
    private JLabel label4;
    private JLabel label5;
    private JFormattedTextField wholesale;
    private JLabel label6;
    private JFormattedTextField retail;
    private JScrollPane scrollPane1;
    private JTextArea description;
    private JLabel label1;
    private JFormattedTextField productCode;
    private JLabel label2;
    private JPanel buttonBar;
    private JButton okButton;
    private JButton cancelButton;

    public EditProductDialog(UpdateTableInterface owner, String name) {
        super((Dialog) owner);
        _interface = owner;
        initComponents();
        this.nameStr = name;
        loadData();
    }

    private void okButtonActionPerformed(ActionEvent e) {
        if (!evaluateFieldInputs()) {
            JOptionPane.showMessageDialog(this, "Input not valid. Check input fields", "Input Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        saveToDB();
        _interface.updateTable();
        this.dispose();
    }

    private void cancelButtonActionPerformed(ActionEvent e) {
        dispose();
    }

    private boolean evaluateFieldInputs() {
        if (name.getText().equals("")) {
            return false;
        }
        if (description.getText().equals("")) {
            return false;
        }
        if (retail.getText().equals("")) {
            return false;
        }
        if (wholesale.getText().equals("")) {
            return false;
        }
        if (productCode.getText().equals("")) {
            return false;
        }
        return true;
    }

    void loadData() {
        name.setEditable(false);
        Connection connection = null;
        Statement statement = null;
        ResultSet rs;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");

            String sql = "SELECT * FROM  PRODUCTS WHERE NAME = '" + nameStr + "';";
            statement = connection.createStatement();
            Debug.print(sql, "XXXXXXXXXXXXX");
            rs = statement.executeQuery(sql);
            if (rs.next()) {
                name.setText(rs.getString("NAME"));
                description.setText(rs.getString("DESCRIPTION"));
                retail.setValue(rs.getInt("RETAIL"));
                wholesale.setValue(rs.getInt("WHOLESALE"));
                productCode.setValue(rs.getString("PRODUCT_CODE"));
            }

            rs.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
//                    Debug.print("statement null", "dbmanager");
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Database closing error");
            }
        }
    }

    private void saveToDB() {
        Connection connection = null;
        Statement statement = null;
        ResultSet rs;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            String sql = "UPDATE PRODUCTS SET "
                    + "DESCRIPTION = '" + description.getText().trim() + "'," +
                    "RETAIL = " + retail.getValue().toString() + "," +
                    "WHOLESALE = " + wholesale.getValue().toString() + " " +
                    "WHERE NAME = '" + nameStr + "';";

            statement = connection.createStatement();
            Debug.print(sql, "XXXXXXXXXXXXXXX");
            statement.executeUpdate(sql);
            connection.commit();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
//                    Debug.print("statement null", "dbmanager");
                }
                if (connection != null) {
                    connection.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Database closing error");
            }

        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        label3 = new JLabel();
        name = new JTextField();
        label4 = new JLabel();
        label5 = new JLabel();
        wholesale = new JFormattedTextField(NumberFormat.getInstance());
        label6 = new JLabel();
        retail = new JFormattedTextField(NumberFormat.getInstance());
        scrollPane1 = new JScrollPane();
        description = new JTextArea();
        label1 = new JLabel();
        productCode = new JFormattedTextField(new RegexFormatter("[1-9][0-9][0-9]"));
        label2 = new JLabel();
        buttonBar = new JPanel();
        okButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        setTitle("Edit Product");
        setModal(true);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));


            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {

                //---- label3 ----
                label3.setText("Product Name:");

                //---- name ----
                name.setEditable(false);

                //---- label4 ----
                label4.setText("Product Description:");

                //---- label5 ----
                label5.setText("Wholesale Price:");

                //---- label6 ----
                label6.setText("Retail Price:");

                //======== scrollPane1 ========
                {
                    scrollPane1.setViewportView(description);
                }

                //---- label1 ----
                label1.setText("Product Code");

                //---- productCode ----
                productCode.setEditable(false);

                //---- label2 ----
                label2.setText("(*Must be a 3 digit number starting with 1)");

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                .addComponent(label3)
                                                .addComponent(label4)
                                                .addComponent(label5)
                                                .addComponent(label6)
                                                .addComponent(label1))
                                        .addGap(22, 22, 22)
                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                        .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                                                .addComponent(retail, GroupLayout.Alignment.LEADING)
                                                                .addComponent(wholesale, GroupLayout.Alignment.LEADING)
                                                                .addComponent(productCode, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE))
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                        .addComponent(label2)
                                                        .addGap(0, 0, Short.MAX_VALUE))
                                                .addComponent(name)
                                                .addComponent(scrollPane1))
                                        .addContainerGap())
                );
                contentPanelLayout.setVerticalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(GroupLayout.Alignment.TRAILING, contentPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                .addComponent(label3)
                                                .addComponent(name, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                .addComponent(label4)
                                                .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                                .addComponent(label5)
                                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                                        .addGap(29, 29, 29)
                                                                        .addComponent(label6)))
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                        .addComponent(label1))
                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                        .addComponent(wholesale, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(retail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                        .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                                .addComponent(productCode, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(label2))))
                                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[]{0, 85, 80};
                ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[]{1.0, 0.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        okButtonActionPerformed(e);
                    }
                });
                buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        cancelButtonActionPerformed(e);
                    }
                });
                buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
