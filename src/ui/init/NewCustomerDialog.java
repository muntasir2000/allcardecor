/*
 * Created by JFormDesigner on Fri Apr 11 12:48:15 PDT 2014
 */

package ui.init;

import core.debug.Debug;
import ui.manage.UpdateTableInterface;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * @author Tareq  Al Muntasir
 */
public class NewCustomerDialog extends JDialog {
    JComboBox jComboBox;
    UpdateTableInterface owner;
    HashMap<String, Integer> discountMap;
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel label3;
    private JLabel label4;
    private JTextField name;
    private JLabel label5;
    private JTextField contact;
    private JLabel label6;
    private JSpinner discount;
    private JLabel label7;
    private JScrollPane scrollPane1;
    private JTextArea address;
    private JPanel buttonBar;
    private JButton okButton;
    private JButton cancelButton;

    public NewCustomerDialog(UpdateTableInterface owner) {
        super((Dialog) owner);
        initComponents();
        this.owner = owner;
    }

    public NewCustomerDialog(Dialog owner, JComboBox comboBox, HashMap map) {
        super(owner);
        initComponents();
        //  saveToDB();
        jComboBox = comboBox;
        discountMap = map;
    }

    private void saveToDB() {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            Class.forName("org.sqlite.JDBC");

            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            String sql = "INSERT INTO CUSTOMER (NAME, ADDRESS, CONTACT, DISCOUNT) VALUES(?, ?, ?, ?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, name.getText());
            statement.setString(2, address.getText());
            statement.setString(3, contact.getText());
            statement.setInt(4, Integer.parseInt(discount.getValue().toString()));
            statement.executeUpdate();
            connection.commit();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    Debug.print("statement null", "dbmanager");
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Database closing error");
            }
        }

    }

    private void okButtonActionPerformed(ActionEvent e) {
        saveToDB();
        if (owner != null)
            owner.updateTable();

        if (discountMap != null) {
            discountMap.put(name.getText(), new Integer(discount.getValue().toString()));
        }

        if (jComboBox != null) {
            jComboBox.addItem(name.getText());
            jComboBox.setSelectedItem(name.getText().toString());
        }
        this.dispose();
    }

    private void cancelButtonActionPerformed(ActionEvent e) {
        this.dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        label3 = new JLabel();
        label4 = new JLabel();
        name = new JTextField();
        label5 = new JLabel();
        contact = new JTextField();
        label6 = new JLabel();
        discount = new JSpinner();
        label7 = new JLabel();
        scrollPane1 = new JScrollPane();
        address = new JTextArea();
        buttonBar = new JPanel();
        okButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        setTitle("Add New Customer");
        setModal(true);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));


            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {

                //---- label3 ----
                label3.setText("Customer Name:");

                //---- label4 ----
                label4.setText("Customer Address:");

                //---- label5 ----
                label5.setText("Contact:");

                //---- label6 ----
                label6.setText("Discount Rate:");

                //---- label7 ----
                label7.setText("%");

                //======== scrollPane1 ========
                {
                    scrollPane1.setViewportView(address);
                }

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                                .addComponent(label3)
                                                                .addComponent(label4))
                                                        .addGap(22, 22, 22)
                                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                                .addComponent(name, GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE)
                                                                .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE)))
                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                                .addComponent(label6)
                                                                .addComponent(label5))
                                                        .addGap(46, 46, 46)
                                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                                        .addComponent(discount, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(label7))
                                                                .addComponent(contact, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE))
                                                        .addGap(0, 195, Short.MAX_VALUE)))
                                        .addContainerGap())
                );
                contentPanelLayout.setVerticalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(GroupLayout.Alignment.TRAILING, contentPanelLayout.createSequentialGroup()
                                        .addGap(7, 7, 7)
                                        .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                .addComponent(label3)
                                                .addComponent(name, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                        .addComponent(label4)
                                                        .addGap(0, 24, Short.MAX_VALUE))
                                                .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE))
                                        .addGap(12, 12, 12)
                                        .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                .addComponent(contact, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                .addComponent(label5))
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                .addComponent(label6)
                                                .addComponent(label7)
                                                .addComponent(discount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                        .addContainerGap())
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[]{0, 85, 80};
                ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[]{1.0, 0.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        okButtonActionPerformed(e);
                    }
                });
                buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        cancelButtonActionPerformed(e);
                    }
                });
                buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
