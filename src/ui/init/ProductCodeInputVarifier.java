package ui.init;

import javax.swing.*;
import java.awt.*;
import java.text.ParseException;

/**
 * Created by Tareq on 6/6/2014.
 */
public class ProductCodeInputVarifier extends InputVerifier {
    @Override
    public boolean verify(JComponent input) {
        if (input instanceof JFormattedTextField) {
            JFormattedTextField ftf = (JFormattedTextField) input;
            JFormattedTextField.AbstractFormatter formatter = ftf.getFormatter();
            if (formatter != null) {
                String text = ftf.getText();
                try {
                    String str = (String) formatter.stringToValue(text);
                    if (str.matches("[1-9][0-9][0-9]")) {
                        input.setBackground(UIManager.getColor("TextField.background"));
                        return true;
                    } else {
                        input.setBackground(Color.RED);
                        return false;
                    }
                } catch (ParseException pe) {
                    input.setBackground(Color.red);
                    return false;
                }
            }
        }
        return false;
    }

}
