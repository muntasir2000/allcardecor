/*
 * Created by JFormDesigner on Wed Apr 23 15:20:54 ALMT 2014
 */

package ui.init;

import ui.manage.UpdateTableInterface;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

/**
 * @author Tareq  Al Muntasir
 */
public class AddNewSupplier extends JDialog {
    JComboBox comboBox;
    UpdateTableInterface interface_;
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel label1;
    private JTextField name;
    private JLabel label2;
    private JLabel label3;
    private JTextField contact;
    private JScrollPane scrollPane1;
    private JTextArea address;
    private JPanel buttonBar;
    private JButton okButton;
    private JButton cancelButton;

    public AddNewSupplier(Frame owner) {
        super(owner);
        initComponents();
    }

    public AddNewSupplier(UpdateTableInterface owner) {
        super((Dialog) owner);
        interface_ = owner;
        initComponents();

    }

    public AddNewSupplier(Dialog owner, JComboBox toBeRefreshed) {
        super(owner);
        comboBox = toBeRefreshed;
        initComponents();
    }

    private void okButtonActionPerformed(ActionEvent e) {
        if (!evaluateFieldInputs()) {
            JOptionPane.showMessageDialog(this, "Input not valid. Check input fields", "Input Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        saveToDB();
        if (comboBox != null) {
            comboBox.addItem(name.getText());
            comboBox.setSelectedItem(name.getText().toString());
        }
        if (interface_ != null) {
            interface_.updateTable();
        }

        this.dispose();
    }

    private boolean evaluateFieldInputs() {
        if (name.getText().equals("")) {
            return false;
        }
        if (address.getText().equals("")) {
            return false;
        }
        if (contact.getText().equals("")) {
            return false;
        }
        return true;
    }

    private void saveToDB() {
        Connection connection = null;
        Statement statement = null;
        ResultSet rs;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            String sql = "INSERT INTO SUPPLIER (NAME, ADDRESS, CONTACT) VALUES('" + name.getText() + "', '" + address.getText() + "', '" + contact.getText() + "');";
            statement = connection.createStatement();
            statement.executeUpdate(sql);
            connection.commit();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
//                    Debug.print("statement null", "dbmanager");
                }
                if (connection != null) {
                    connection.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Database closing error");
            }

        }
    }

    private void cancelButtonActionPerformed(ActionEvent e) {
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        label1 = new JLabel();
        name = new JTextField();
        label2 = new JLabel();
        label3 = new JLabel();
        contact = new JTextField();
        scrollPane1 = new JScrollPane();
        address = new JTextArea();
        buttonBar = new JPanel();
        okButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        setTitle("Add New Supplier");
        setModal(true);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));


            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {

                //---- label1 ----
                label1.setText("Supplier Name");

                //---- label2 ----
                label2.setText("Address");

                //---- label3 ----
                label3.setText("Contact");

                //======== scrollPane1 ========
                {
                    scrollPane1.setViewportView(address);
                }

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                .addComponent(label1)
                                                .addComponent(label2)
                                                .addComponent(label3, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                .addComponent(contact, GroupLayout.PREFERRED_SIZE, 254, GroupLayout.PREFERRED_SIZE)
                                                .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)
                                                        .addComponent(name, GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)))
                                        .addContainerGap(4, Short.MAX_VALUE))
                );
                contentPanelLayout.setVerticalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                .addComponent(label1)
                                                .addComponent(name, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                        .addComponent(label2)
                                                        .addGap(0, 19, Short.MAX_VALUE))
                                                .addComponent(scrollPane1, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE))
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                .addComponent(label3)
                                                .addComponent(contact, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                        .addContainerGap())
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[]{0, 85, 80};
                ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[]{1.0, 0.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        okButtonActionPerformed(e);
                    }
                });
                buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        cancelButtonActionPerformed(e);
                    }
                });
                buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
