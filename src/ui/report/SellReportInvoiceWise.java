/*
 * Created by JFormDesigner on Fri Jun 06 00:58:23 ALMT 2014
 */

package ui.report;

import com.toedter.calendar.JDateChooser;
import core.debug.Debug;
import net.sf.dynamicreports.report.exception.DRException;
import ui.report.invoice.dynamicreport.InvoiceGenerator;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PrinterException;
import java.sql.*;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.Vector;

/**
 * @author Tareq Muntasir
 */
public class SellReportInvoiceWise extends JDialog {
    NumberFormat numberFormat = NumberFormat.getNumberInstance();
    boolean isSomethingWrong = false;
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel panel1;
    private JLabel label1;
    private JComboBox cbCustomer;
    private JLabel label2;
    private JLabel label3;
    private JFormattedTextField toInvoiceID;
    private JLabel label4;
    private JFormattedTextField fromInvoiceID;
    private JLabel label5;
    private JLabel label6;
    private JDateChooser toDateChooser;
    private JLabel label7;
    private JDateChooser fromDateChooser;
    private JLabel label8;
    private JCheckBox retailCheckBox;
    private JCheckBox wholesaleCheckBox;
    private JButton btnSearch;
    private JLabel label9;
    private JCheckBox outputIDCheckbox;
    private JCheckBox outputCustomerCheckBox;
    private JCheckBox outputDateCheckbox;
    private JCheckBox outputTotalCheckBox;
    private JCheckBox outputDiscountCheckBox;
    private JCheckBox outputCountCheckBox;
    private JCheckBox outputIsRetailCheckBox;
    private JPanel panel2;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JButton button2;
    private JButton close;

    public SellReportInvoiceWise(Frame owner) {
        super(owner);
        initComponents();
        loadData();
        table1.setCellEditor(null);
        table1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    Debug.print("in table mouse listener ", "test");
                    JTable target = (JTable) e.getSource();
                    int row = target.getSelectedRow();
                    int invoiceID = (Integer) target.getValueAt(row, 0);
                    InvoiceGenerator invoiceGenerator = new InvoiceGenerator(invoiceID);
                    SellReportInvoiceWise.this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    try {
                        invoiceGenerator.build().show(false);
                    } catch (DRException e1) {
                        e1.printStackTrace();
                    }
                    SellReportInvoiceWise.this.setCursor(Cursor.getDefaultCursor());
                  /*  WaitDialog dialog = new WaitDialog(SellReportInvoiceWise.this, "Please Wait", "Generating Invoice for Invoice ID: " + invoiceID);
                    dialog.setVisible(true);
                    LoadAndShowInvoice loadAndShowInvoice = new LoadAndShowInvoice(dialog, invoiceID);
                    loadAndShowInvoice.execute();*/
                }
            }
        });
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }

    public static TableModel resultSetToTableModel(ResultSet rs) {
        try {
            ResultSetMetaData metaData = rs.getMetaData();
            int numberOfColumns = metaData.getColumnCount();
            Vector<String> columnNames = new Vector<String>();

            // Get the column names
            for (int column = 0; column < numberOfColumns; column++) {
                columnNames.addElement(metaData.getColumnLabel(column + 1));
            }

            // Get all rows.
            Vector<Vector<Object>> rows = new Vector<Vector<Object>>();

            while (rs.next()) {
                Vector<Object> newRow = new Vector<Object>();

                for (int i = 1; i <= numberOfColumns; i++) {
                    newRow.addElement(rs.getObject(i));
                }

                rows.addElement(newRow);
            }

            return new DefaultTableModel(rows, columnNames) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    void loadData() {
        Connection connection = null;
        Statement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            statement = connection.createStatement();
            String sql = "SELECT NAME FROM CUSTOMER;";
            ResultSet rs = statement.executeQuery(sql);

            cbCustomer.addItem(""); // add an empty element for if the user wants to exclude customer
            while (rs.next()) {
                cbCustomer.addItem(rs.getString("NAME"));
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private String generateSQL() {
        boolean atLeastOneColumnSelected = false;
        boolean atLeastOneCriterionSelected = false;
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");

        if (outputIDCheckbox.isSelected()) {
            sql.append("ID,");
            atLeastOneColumnSelected = true;
        }
        if (outputCustomerCheckBox.isSelected()) {
            sql.append("CUSTOMER,");
            atLeastOneColumnSelected = true;
        }
        if (outputDateCheckbox.isSelected()) {
            sql.append("DATE,");
            atLeastOneColumnSelected = true;
        }
        if (outputTotalCheckBox.isSelected()) {
            sql.append("TOTAL,");
            atLeastOneColumnSelected = true;
        }
        if (outputDiscountCheckBox.isSelected()) {
            sql.append("DISCOUNT,");
            atLeastOneColumnSelected = true;
        }
        if (outputCountCheckBox.isSelected()) {
            sql.append("COUNT,");
            atLeastOneColumnSelected = true;
        }
        if (outputIsRetailCheckBox.isSelected()) {
            sql.append("ISRETAIL AS TYPE,");
            atLeastOneColumnSelected = true;
        }

        if (atLeastOneColumnSelected) {
            sql.deleteCharAt(sql.lastIndexOf(","));
        } else {
            sql.append(" * ");
        }

        sql.append(" FROM SELL_INVOICE WHERE ");

        if (!isCustomerInclueded().equals("")) {
            sql.append(isCustomerInclueded() + " AND ");
            atLeastOneCriterionSelected = true;
        }
        if (!isInvoiceIDIncluded().equals("")) {
            sql.append(isInvoiceIDIncluded() + " AND ");
            atLeastOneCriterionSelected = true;
        }
        if (!isDateIncluded().equals("")) {
            sql.append(isDateIncluded() + " AND ");
            atLeastOneCriterionSelected = true;
        }
        if (!isTypeSelected().equals("")) {
            sql.append(isTypeSelected() + " AND ");
            atLeastOneCriterionSelected = true;
        }

        if (atLeastOneCriterionSelected) {
            int i = sql.lastIndexOf("AND");
            sql.delete(i, i + 3);
        } else {
            int i = sql.lastIndexOf("WHERE");
            sql.delete(i, i + 5);
        }
        sql.append(";");

        Debug.print(sql.toString(), "Generated SQL: ");
        return sql.toString();
    }

    private String isCustomerInclueded() {
        if (!cbCustomer.getSelectedItem().equals("")) {
            return "CUSTOMER = '" + cbCustomer.getSelectedItem().toString() + "'";
        } else {
            isSomethingWrong = false;
            return "";
        }
    }

    private String isInvoiceIDIncluded() {
        if (toInvoiceID.getText().equals("") && fromInvoiceID.getText().equals("")) {
            // isSomethingWrong = true;
            return "";
        } else if (toInvoiceID.getText().equals("") && !fromInvoiceID.getText().equals("")) {
            //isSomethingWrong = true;
            toInvoiceID.setText(fromInvoiceID.getText());
            return "ID BETWEEN " + toInvoiceID.getText() + " AND " + fromInvoiceID.getText();
        } else if (!toInvoiceID.getText().equals("") && fromInvoiceID.getText().equals("")) {
            fromInvoiceID.setText(toInvoiceID.getText());
            return "ID BETWEEN " + toInvoiceID.getText() + " AND " + fromInvoiceID.getText();
        } else if (!toInvoiceID.getText().equals("") && !fromInvoiceID.getText().equals("")) {

            //        return "ID BETWEEN " + toInvoiceID.getText() + " AND " + fromInvoiceID.getText();
            if (isRangeVaild(toInvoiceID.getText(), fromInvoiceID.getText())) {
                return "ID BETWEEN " + toInvoiceID.getText() + " AND " + fromInvoiceID.getText();
            } else {
                JOptionPane.showMessageDialog(this, "Invalid Range for Invoice ID. Range not allowed. 'To' field must be less than 'From' field.", "Invalid range", JOptionPane.ERROR_MESSAGE);
                return "";
            }

        } else {
            isSomethingWrong = false;
            return "";
        }
    }

    boolean isRangeVaild(String a, String b) {
        int i = 0, j = 0;
        try {
            i = Integer.parseInt(a);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        try {
            j = Integer.parseInt(b);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        if (i > j) {
            return false;
        } else return true;
    }

    private String isDateIncluded() {
        java.util.Date toDate = toDateChooser.getDate();
        java.util.Date fromDate = fromDateChooser.getDate();

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Dhaka"));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
        simpleDateFormat.setCalendar(cal);

        if (toDate != null && fromDate == null) {
            fromDateChooser.setDate(toDate);
            return "DATE BETWEEN '" + simpleDateFormat.format(toDateChooser.getDate()) + " 00:00:00' AND '" + simpleDateFormat.format(fromDateChooser.getDate()) + " 23:59:59'";
        } else if (toDate == null && fromDate != null) {
            toDateChooser.setDate(fromDate);
            return "DATE BETWEEN '" + simpleDateFormat.format(toDateChooser.getDate()) + " 00:00:00' AND '" + simpleDateFormat.format(fromDateChooser.getDate()) + " 23:59:59'";
        } else if (toDate != null && fromDate != null) {
            //check range validity
            if (!toDate.after(fromDate)) {
                //valid
                return "DATE BETWEEN '" + simpleDateFormat.format(toDateChooser.getDate()) + " 00:00:00' AND '" + simpleDateFormat.format(fromDateChooser.getDate()) + " 23:59:59'";
            } else {
                JOptionPane.showMessageDialog(this, "Date range invalid. 'To' date should be before 'From' date.", "Invalid Input", JOptionPane.ERROR_MESSAGE);
                return "";
            }
        }
        return "";
    }

    private String isTypeSelected() {
        if (retailCheckBox.isSelected() && wholesaleCheckBox.isSelected()) {
            return "";
        } else if (retailCheckBox.isSelected() && !wholesaleCheckBox.isSelected()) {
            return "ISRETAIL = 'true'";
        } else if (retailCheckBox.isSelected() && wholesaleCheckBox.isSelected()) {
            return "ISRETAIL = 'false'";
        }
        return "";
    }

    private void btnSearchActionPerformed(ActionEvent event) {
        Connection connection = null;
        Statement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            statement = connection.createStatement();
            String sql = generateSQL();
            ResultSet rs = statement.executeQuery(sql);

            table1.setModel(resultSetToNonEditableTableModel(rs));

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private TableModel resultSetToNonEditableTableModel(ResultSet rs) {
        try {
            ResultSetMetaData metaData = rs.getMetaData();
            int numberOfColumns = metaData.getColumnCount();
            final Vector<String> columnNames = new Vector<String>();

            // Get the column names
            for (int column = 0; column < numberOfColumns; column++) {
                columnNames.addElement(metaData.getColumnLabel(column + 1));
            }

            // Get all rows.
            Vector<Vector<Object>> rows = new Vector<Vector<Object>>();

            while (rs.next()) {
                Vector<Object> newRow = new Vector<Object>();

                for (int i = 1; i <= numberOfColumns; i++) {
                    newRow.addElement(rs.getObject(i));
                }

                rows.addElement(newRow);
            }

            return new DefaultTableModel(rows, columnNames) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }

                @Override
                public void setValueAt(Object aValue, int row, int column) {
                    if (aValue.toString().equals("true")) {
                        super.setValueAt("Retail", row, column);
                    } else if (aValue.toString().equals("false")) {
                        super.setValueAt("Wholesale", row, column);
                    } else {
                        super.setValueAt(aValue, row, column);
                    }
                }

                @Override
                public Object getValueAt(int row, int column) {
                    if (columnNames.get(column).equals("TYPE")) {
                        Vector<Object> rows = (Vector) dataVector.get(row);
                        if (rows.get(column).equals("true")) {
                            return "RETAIL";
                        } else {
                            return "WHOLESALE";
                        }
                    }

                    return super.getValueAt(row, column);
                }
            };
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    private void button2ActionPerformed(ActionEvent e) {
        MessageFormat header = new MessageFormat("Invoice Wise Sale List");
        MessageFormat footer = new MessageFormat("Page - {0}");

        try {
            table1.print(JTable.PrintMode.NORMAL, header, footer);
        } catch (PrinterException e1) {
            e1.printStackTrace();
        }
    }

    private void closeActionPerformed(ActionEvent e) {
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        panel1 = new JPanel();
        label1 = new JLabel();
        cbCustomer = new JComboBox();
        label2 = new JLabel();
        label3 = new JLabel();
        toInvoiceID = new JFormattedTextField(numberFormat);
        label4 = new JLabel();
        fromInvoiceID = new JFormattedTextField(numberFormat);
        label5 = new JLabel();
        label6 = new JLabel();
        toDateChooser = new JDateChooser();
        label7 = new JLabel();
        fromDateChooser = new JDateChooser();
        label8 = new JLabel();
        retailCheckBox = new JCheckBox();
        wholesaleCheckBox = new JCheckBox();
        btnSearch = new JButton();
        label9 = new JLabel();
        outputIDCheckbox = new JCheckBox();
        outputCustomerCheckBox = new JCheckBox();
        outputDateCheckbox = new JCheckBox();
        outputTotalCheckBox = new JCheckBox();
        outputDiscountCheckBox = new JCheckBox();
        outputCountCheckBox = new JCheckBox();
        outputIsRetailCheckBox = new JCheckBox();
        panel2 = new JPanel();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();
        button2 = new JButton();
        close = new JButton();

        //======== this ========
        setTitle("Invoice Wise Sale Reports");
        setModal(true);
        Container contentPane = getContentPane();

        //======== panel1 ========
        {
            panel1.setBorder(new TitledBorder("Search Criteria"));


            //---- label1 ----
            label1.setText("Customer");

            //---- label2 ----
            label2.setText("Invoice ID");

            //---- label3 ----
            label3.setText("From");

            //---- label4 ----
            label4.setText("To");

            //---- label5 ----
            label5.setText("Date");

            //---- label6 ----
            label6.setText("From");

            //---- label7 ----
            label7.setText("To");

            //---- label8 ----
            label8.setText("Type");

            //---- retailCheckBox ----
            retailCheckBox.setText("Retail");

            //---- wholesaleCheckBox ----
            wholesaleCheckBox.setText("Wholesale");

            //---- btnSearch ----
            btnSearch.setText("Search");
            btnSearch.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    btnSearchActionPerformed(e);
                }
            });

            //---- label9 ----
            label9.setText("Select output columns");

            //---- outputIDCheckbox ----
            outputIDCheckbox.setText("ID");
            outputIDCheckbox.setEnabled(false);
            outputIDCheckbox.setSelected(true);

            //---- outputCustomerCheckBox ----
            outputCustomerCheckBox.setText("Customer");

            //---- outputDateCheckbox ----
            outputDateCheckbox.setText("Date");

            //---- outputTotalCheckBox ----
            outputTotalCheckBox.setText("Total");

            //---- outputDiscountCheckBox ----
            outputDiscountCheckBox.setText("Discount");

            //---- outputCountCheckBox ----
            outputCountCheckBox.setText("No. of Products");

            //---- outputIsRetailCheckBox ----
            outputIsRetailCheckBox.setText("Retail or Wholesale");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
                    panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createSequentialGroup()
                                    .addGroup(panel1Layout.createParallelGroup()
                                            .addGroup(panel1Layout.createSequentialGroup()
                                                    .addContainerGap()
                                                    .addGroup(panel1Layout.createParallelGroup()
                                                            .addComponent(label1)
                                                            .addComponent(label2)
                                                            .addComponent(label8)
                                                            .addComponent(label5))
                                                    .addGap(31, 31, 31)
                                                    .addGroup(panel1Layout.createParallelGroup()
                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                    .addComponent(retailCheckBox)
                                                                    .addGap(18, 18, 18)
                                                                    .addComponent(wholesaleCheckBox))
                                                            .addComponent(cbCustomer, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE)
                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                                    .addComponent(label3)
                                                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                                    .addComponent(toInvoiceID, GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE))
                                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                                    .addComponent(label6)
                                                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                                    .addComponent(toDateChooser, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                                    .addGap(27, 27, 27)))
                                                                    .addGroup(panel1Layout.createParallelGroup()
                                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                                    .addComponent(label4)
                                                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                                    .addComponent(fromInvoiceID, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                                    .addComponent(label7)
                                                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                                    .addComponent(fromDateChooser, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE))))))
                                            .addGroup(panel1Layout.createSequentialGroup()
                                                    .addGap(137, 137, 137)
                                                    .addComponent(outputIsRetailCheckBox))
                                            .addGroup(panel1Layout.createSequentialGroup()
                                                    .addGap(274, 274, 274)
                                                    .addComponent(btnSearch, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE))
                                            .addGroup(panel1Layout.createSequentialGroup()
                                                    .addContainerGap()
                                                    .addComponent(label9)
                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(outputIDCheckbox)
                                                    .addGap(34, 34, 34)
                                                    .addComponent(outputCustomerCheckBox)
                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(outputDateCheckbox)
                                                    .addGap(18, 18, 18)
                                                    .addComponent(outputTotalCheckBox)
                                                    .addGap(30, 30, 30)
                                                    .addComponent(outputDiscountCheckBox)
                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(outputCountCheckBox)))
                                    .addContainerGap(13, Short.MAX_VALUE))
            );
            panel1Layout.setVerticalGroup(
                    panel1Layout.createParallelGroup()
                            .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(label1)
                                            .addComponent(cbCustomer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(label2)
                                            .addComponent(label3)
                                            .addComponent(label4)
                                            .addComponent(toInvoiceID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(fromInvoiceID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(panel1Layout.createParallelGroup()
                                            .addComponent(fromDateChooser, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(label6, GroupLayout.Alignment.TRAILING)
                                            .addComponent(label7, GroupLayout.Alignment.TRAILING)
                                            .addComponent(toDateChooser, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(label5, GroupLayout.Alignment.TRAILING))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(retailCheckBox)
                                            .addComponent(wholesaleCheckBox)
                                            .addComponent(label8))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(outputCustomerCheckBox)
                                            .addComponent(outputDateCheckbox)
                                            .addComponent(outputTotalCheckBox)
                                            .addComponent(outputDiscountCheckBox)
                                            .addComponent(outputCountCheckBox)
                                            .addComponent(label9)
                                            .addComponent(outputIDCheckbox))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(outputIsRetailCheckBox)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnSearch)
                                    .addContainerGap())
            );
        }

        //======== panel2 ========
        {
            panel2.setBorder(new TitledBorder("Search Result"));

            //======== scrollPane1 ========
            {
                scrollPane1.setViewportView(table1);
            }

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
                    panel2Layout.createParallelGroup()
                            .addGroup(panel2Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE)
                                    .addContainerGap())
            );
            panel2Layout.setVerticalGroup(
                    panel2Layout.createParallelGroup()
                            .addGroup(GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                                    .addContainerGap())
            );
        }

        //---- button2 ----
        button2.setText("Print");
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button2ActionPerformed(e);
            }
        });

        //---- close ----
        close.setText("Close");
        close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                closeActionPerformed(e);
            }
        });

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addComponent(button2, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 487, Short.MAX_VALUE)
                                                .addComponent(close, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                                                .addGap(12, 12, 12))
                                        .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
        );
        contentPaneLayout.setVerticalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(button2)
                                        .addComponent(close))
                                .addContainerGap())
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
