package ui.report.profit;

import core.debug.Debug;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


public class ProfitData {
    Date toDate, fromDate;
    HashMap<Date, Integer> dateStockMap;
    HashMap<Date, Integer> dateSaleMap;
    private int totalStock = 0;
    private int totalSale = 0;

    public ProfitData(Date from, Date to) {
        //WARNING THERE IS AN ISSUE REGARDING TODATE AND FROM DATE
        // I DIDN'T FOLLOW THE STANDARD CODING CONVENTION DUE TO MY LAZINESS. BE CAREFUL
        toDate = to;
        fromDate = from;
        loadData();
    }

    public int getTotalStock() {
        return totalStock;
    }

    public int getTotalSale() {
        return totalSale;
    }

    public void loadData() {
        // WARNING THERE IS AN ISSUE REGARDING TODATE AND FROMDATE
        // I DIDN'T FOLLOW THE STANDARD CODING CONVENTION DUE TO MY LAZINESS. BE CAREFUL

        dateSaleMap = new HashMap<Date, Integer>();
        dateStockMap = new HashMap<Date, Integer>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
        String toDateStr = simpleDateFormat.format(toDate);
        String fromDateStr = simpleDateFormat.format(fromDate);

        String datePattern = "YYYY-MM-dd";
        Connection connection = null;
        java.sql.Statement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            statement = connection.createStatement();
            String sql = "SELECT COST, DATE  FROM STOCK_INVENTORY WHERE DATE BETWEEN '" + fromDateStr + "' AND '" + toDateStr + "';";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                int cost = rs.getInt("COST");

                String dateStr = rs.getString("DATE");
                //     Debug.print(dateStr, "XXXXXXXXXXXXXXXXXXXX");
                dateStr = dateStr.substring(0, 10);
                DateTime dateTime = DateTime.parse(dateStr, DateTimeFormat.forPattern(datePattern));
                //     Debug.print("Date " + dateStr + " // Cost " + cost, "");
                Integer temp = dateStockMap.get(dateTime.toDate());
                if (temp == null) {
                    dateStockMap.put(dateTime.toDate(), cost);
                } else {
                    //       int a = temp + cost;
                    //     Debug.print(a + "", "putting cost for Date" + dateStr);
                    dateStockMap.put(dateTime.toDate(), temp + cost);
                }
                //   totalStock += cost;
            }

//            statement.close();
            rs.close();
            sql = "SELECT PRODUCT, DATE, COST FROM SELL_RECORDS WHERE DATE BETWEEN '" + fromDateStr + "' AND '" + toDateStr + "';";
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                int cost = rs.getInt("COST");

                String dateStr = rs.getString("DATE");
                dateStr = dateStr.substring(0, 10);
                DateTime dateTime = DateTime.parse(dateStr, DateTimeFormat.forPattern(datePattern));

                Integer temp = dateSaleMap.get(dateTime.toDate());
                if (temp == null) {
                    dateSaleMap.put(dateTime.toDate(), cost);
                } else {
                    //     Debug.print(a + "", "putting cost for Date" + dateStr);
                    dateSaleMap.put(dateTime.toDate(), temp + cost);
                }
                //    totalSale += cost;
            }

            rs.close();

            sql = "SELECT SUM(STOCK_INVENTORY.COST) AS STOCK, " +
                    "SUM(SELL_RECORDS.COST) AS SALE " +
                    "FROM STOCK_INVENTORY, SELL_RECORDS " +
                    "WHERE SELL_RECORDS.DATE BETWEEN '" + fromDateStr + "' AND '" + toDateStr + "' " +
                    "AND STOCK_INVENTORY.SERIAL = SELL_RECORDS.SERIAL " +
                    "AND SELL_RECORDS.ISRETURNED='false';";
            rs = statement.executeQuery(sql);
            Debug.print(sql, "SQL from profit report generator");
            if (rs.next()) {
                totalSale = rs.getInt("SALE");
                Debug.print("TotalSale before considering discount = " + totalSale, "ProfitData");
                totalStock = rs.getInt("STOCK");
            }

            //ADJUST PROFIT FOR GIVEN DISCOUNTS.
            sql = "SELECT SUM(DISCOUNT) AS TOTAL_DISCOUNT " +
                    "FROM SELL_INVOICE WHERE" +
                    " DATE BETWEEN '" + fromDateStr + "' AND '" + toDateStr + "';";
            rs = statement.executeQuery(sql);
            if (rs.next()) {
                totalSale = totalSale - rs.getInt("TOTAL_DISCOUNT");
                Debug.print("totalSale = " + totalSale, "ProfitData");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public JRDataSource createDatasourceForStockChart() {
        ArrayList<DateSum> list = new ArrayList<DateSum>();

        for (Date date : dateStockMap.keySet()) {
            DateSum dateSum = new DateSum();
            dateSum.setDate(date);
            dateSum.setTotal(dateStockMap.get(date));
            list.add(dateSum);
            Debug.print(dateSum.getDate().toString(), "XXXXXXX Date");
            Debug.print(dateSum.getTotal().toString(), "XXXXXX Total");
        }
        return new JRBeanCollectionDataSource(list);
    }

    public JRDataSource createDatasourceForSaleChart() {
        ArrayList<DateSum> list = new ArrayList<DateSum>();

        for (Date date : dateSaleMap.keySet()) {
            DateSum dateSum = new DateSum();
            dateSum.setDate(date);
            dateSum.setTotal(dateSaleMap.get(date));
            list.add(dateSum);
        }
        return new JRBeanCollectionDataSource(list);
    }
}
