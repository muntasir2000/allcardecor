package ui.report.profit;

import java.util.Date;

/**
 * Created by Tareq on 6/5/2014.
 */
public class DateSum {
    private Date date;
    private Integer total;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
