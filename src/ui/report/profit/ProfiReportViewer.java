package ui.report.profit;

/**
 * Created by Tareq on 6/5/2014.
 */

import net.sf.dynamicreports.examples.Templates;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.builder.component.SubreportBuilder;
import net.sf.dynamicreports.report.builder.component.VerticalListBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.exception.DRException;
import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

public class ProfiReportViewer {

    private final String toDateStr;
    private final String fromDateStr;
    ProfitData data;
    DateTime from, to;
    DateTimeFormatter formatter;

    public ProfiReportViewer(DateTime from, DateTime to) {
        this.from = from;
        this.to = to;
        data = new ProfitData(from.toDate(), to.toDate());
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
       /* toDateStr = formatter.format(to.minusDays(1).toDate());
        fromDateStr = formatter.format(from.plusDays(1).toDate());*/
        toDateStr = formatter.format(to.toDate());
        fromDateStr = formatter.format(from.toDate());
        showReport();
    }

    public void showReport() {
        SubreportBuilder saleChart = cmp.subreport(stockTimeChartSubReport());
        SubreportBuilder stockChart = cmp.subreport(saleTimeChartSubReport());


        try {
            report().title(createTitleComponent().setStyle(Templates.columnTitleStyle))
                    .summary(
                            createHeaderComponent(),
                            profitBand(),
                            saleChart,
                            stockChart
                    ).setPageMargin(margin(40)).show(false);
        } catch (DRException e) {
            e.printStackTrace();
        }
    }

    private ComponentBuilder<?, ?> createHeaderComponent() {
        VerticalListBuilder list = cmp.verticalList();
        list.add(cmp.text("Profit Report").setStyle(stl.style().bold().setFontSize(18).setHorizontalAlignment(HorizontalAlignment.CENTER)));
        list.add(cmp.text("From " + fromDateStr + " to " + toDateStr).setStyle(stl.style().bold().setFontSize(16).setHorizontalAlignment(HorizontalAlignment.CENTER)));
        list.add(cmp.line());
        list.add(cmp.verticalGap(10));
        return list;

    }


    private ComponentBuilder<?, ?> createTitleComponent() {
        VerticalListBuilder list = cmp.verticalList();
        list.add(
                cmp.image("res\\logo.png").setFixedDimension(150, 60),
                cmp.text("All Car Decor").setStyle(stl.style().bold().setFontSize(18)).setHorizontalAlignment(HorizontalAlignment.LEFT),
                cmp.text("145, South Shahjahanpur "),
                cmp.text("Dhaka, Bangladesh"),
                cmp.text("+880 1713115944, +880 1613115944"));
        return list;
    }

    private ComponentBuilder<?, ?> profitBand() {
        int profit = data.getTotalSale() - data.getTotalStock();
        StyleBuilder profitBandStyle = stl.style().setHorizontalAlignment(HorizontalAlignment.LEFT).setBold(true);
        VerticalListBuilder list = cmp.verticalList();
        list.add(
                cmp.text("Total Stock worth  Tk. " + data.getTotalStock()),
                cmp.text("Total Sale worth Tk. " + data.getTotalSale()),
                cmp.text("Gross profit = Tk. " + profit).setStyle(profitBandStyle)
                // cmp.text()
        );
        return list;
    }

    private JasperReportBuilder stockTimeChartSubReport() {
        TextColumnBuilder<Date> dateColumn = col.column("Date", "date", type.dateType());
        TextColumnBuilder<Integer> priceColumn = col.column("Stock Price", "total", type.integerType());
        return report()
                .title(cmp.verticalGap(10), cmp.text("Stock by Date").setStyle(Templates.bold18CenteredStyle))
                        //      .columns(dateColumn, priceColumn)
                        //    .setColumnStyle(Templates.columnStyle)
                .summary(
                        cht.timeSeriesChart()
                                .setTimePeriod(dateColumn)
                                .addSerie(
                                        cht.serie(priceColumn)
                                )
                ).setDataSource(data.createDatasourceForStockChart());

    }

    private JasperReportBuilder saleTimeChartSubReport() {
        TextColumnBuilder<Date> dateColumn = col.column("Date", "date", type.dateType());
        TextColumnBuilder<Integer> priceColumn = col.column("Sale", "total", type.integerType());
        return report()
                .title(cmp.verticalGap(10), cmp.text("Sale By Date").setStyle(Templates.bold18CenteredStyle))
                        //      .columns(dateColumn, priceColumn)
                        //    .setColumnStyle(Templates.columnStyle)
                .summary(
                        cht.timeSeriesChart()
                                .setTimePeriod(dateColumn)
                                .addSerie(
                                        cht.serie(priceColumn)
                                )
                ).setDataSource(data.createDatasourceForSaleChart());

    }
}



