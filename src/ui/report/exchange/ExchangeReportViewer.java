package ui.report.exchange;

import net.sf.dynamicreports.examples.Templates;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.builder.component.VerticalListBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.exception.DRException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

/**
 * Created by Tareq on 6/6/2014.
 */
public class ExchangeReportViewer {
    private final String toDateStr;
    private final String fromDateStr;
    private Date to, from;

    public ExchangeReportViewer(Date from, Date to) {
        this.to = to;
        this.from = from;
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
        toDateStr = formatter.format(to);
        fromDateStr = formatter.format(from);
        build();
    }

    public void build() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
        String sqlToDateStr = dateFormat.format(to).concat(" 00:00:00");
        String sqlFromDateStr = dateFormat.format(from).concat(" 00:00:00");

        String sql = "SELECT * FROM EXCHANGED_ITEM WHERE DATE BETWEEN '" + sqlFromDateStr + " ' AND '" + sqlToDateStr + "';";

        StyleBuilder columnStyle = stl.style(Templates.columnStyle)
                .setBorder(stl.pen2Point()).setHorizontalAlignment(HorizontalAlignment.CENTER);


        TextColumnBuilder<Integer> rowIDColumn = col.columnRowNumberColumn(" # ")
                .setFixedColumns(2)
                .setHorizontalAlignment(HorizontalAlignment.CENTER);
        TextColumnBuilder<String> productName = col.column("New Item", "PRODUCT", type.stringType());
        TextColumnBuilder<Integer> serialColumn = col.column("New Serial", "SERIAL", type.integerType());

        TextColumnBuilder<String> oldProductName = col.column("Old Item", "EXCHANGED_PRODUCT", type.stringType());
        TextColumnBuilder<Integer> oldSerial = col.column("Old Serial", "EXCHANGED_SERIAL", type.integerType());
        TextColumnBuilder<Integer> invoiceID = col.column("Invoice ID", "INVOICE_ID", type.integerType());


        try {
            report().title(createTitleComponent())
                    .setTemplate(Templates.reportTemplate)
                    .columns(rowIDColumn, productName, serialColumn, oldProductName, oldSerial, invoiceID)
                    .setColumnStyle(columnStyle)
                    .pageFooter(cmp.pageXofY()).setPageMargin(margin(40))
                    .setDataSource(sql, connection).show(false);
        } catch (DRException e) {
            e.printStackTrace();
        }
    }

    private ComponentBuilder<?, ?> createTitleComponent() {
        VerticalListBuilder list = cmp.verticalList();
        list.add(
                cmp.image("res\\logo.png").setFixedDimension(150, 60),
                cmp.text("All Car Decor").setStyle(stl.style().bold().setFontSize(18)).setHorizontalAlignment(HorizontalAlignment.LEFT),
                cmp.text("145, South Shahjahanpur "),
                cmp.text("Dhaka, Bangladesh"),
                cmp.text("+880 1713115944, +880 1613115944"),
                cmp.line(),
                cmp.verticalGap(10),
                cmp.text("Item Exchange Report").setStyle(stl.style().bold().setFontSize(20)).setHorizontalAlignment(HorizontalAlignment.CENTER),
                cmp.text(fromDateStr + " - " + toDateStr).setStyle(stl.style().bold().setFontSize(15)).setHorizontalAlignment(HorizontalAlignment.CENTER),
                cmp.verticalGap(10)
        );
        return list;
    }

}
