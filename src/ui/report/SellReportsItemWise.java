/*
 * Created by JFormDesigner on Tue May 20 01:05:29 ALMT 2014
 */

package ui.report;

import com.toedter.calendar.JDateChooser;
import core.debug.Debug;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.sql.*;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.Vector;

/**
 * @author Tareq Muntasir
 */
public class SellReportsItemWise extends JDialog {
    boolean isSomethingWrong = false;
    NumberFormat numberFormatForTextFields = NumberFormat.getNumberInstance();
    TableModel model;
    private ArrayList<String> columnNames;
    private ArrayList<String> customerList;
    private ArrayList<String> productList;
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JPanel panel1;
    private JLabel label13;
    private JComboBox cbCustomer;
    private JLabel label14;
    private JComboBox cbProduct;
    private JSeparator separator1;
    private JLabel label21;
    private JLabel label22;
    private JFormattedTextField toSerial;
    private JLabel label23;
    private JFormattedTextField fromSerial;
    private JLabel label24;
    private JLabel label25;
    private JFormattedTextField toInvoiceNo;
    private JLabel label26;
    private JFormattedTextField fromInvoiceNo;
    private JFormattedTextField toPrice;
    private JLabel label27;
    private JFormattedTextField fromPrice;
    private JLabel label28;
    private JLabel label29;
    private JLabel label30;
    private JLabel label31;
    private JDateChooser toDateChooser;
    private JLabel label32;
    private JDateChooser fromDateChooser;
    private JCheckBox checkBoxRetail;
    private JCheckBox checkBoxWholesale;
    private JLabel label33;
    private JCheckBox chkBSerialColumn;
    private JCheckBox chkBProductColumn;
    private JCheckBox chkBCustomerColumn;
    private JCheckBox chkBPriceColumn;
    private JCheckBox chkBDate;
    private JCheckBox chkBWarrantyColumn;
    private JCheckBox chkBTypeColumn;
    private JButton btnSearch;
    private JCheckBox chkBInvoiceID;
    private JPanel panel2;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JButton btnPrint;
    private JPanel buttonBar;
    private JButton okButton;

    public SellReportsItemWise(Frame owner) {
        super(owner);
        initComponents();
        setupMisc();
        loadData();
    }

    void setupMisc() {

    }

    public String enableCustomer() {
        return "CUSTOMER = '" + cbCustomer.getSelectedItem().toString() + "'";
    }

    private String enableProduct() {
        return "PRODUCT = '" + cbProduct.getSelectedItem().toString() + "'";
    }

    private String enableSerial() {
        int to = 0, from = 0;
        boolean isInvalidRange = false;
        to = Integer.parseInt(toSerial.getText().replaceAll(",", ""));
        String str = "SERIAL BETWEEN " + to + " AND ";


        if (!fromSerial.getText().equals("")) {
            from = Integer.parseInt(fromSerial.getText().replaceAll(",", ""));
        }

        if (from != 0 && !(to <= from)) {
            JOptionPane.showMessageDialog(this, "Serial range invalid. To field should be less than From field");
            isSomethingWrong = true;
            isInvalidRange = true;
        }

        if (isInvalidRange) {
            return " ";
        }

        if (from != 0 && !isInvalidRange) {
            str += from;
        } else {
            str += to;
        }
        str += " ";
        return str;
    }

    private String enableInvoice() {
        boolean isInvalidRange = false;
        int to = 0, from = 0;
        to = Integer.parseInt(toInvoiceNo.getText().replaceAll(",", ""));
        if (!fromInvoiceNo.getText().equals("")) {
            from = Integer.parseInt(fromInvoiceNo.getText().replaceAll(",", ""));
        }

        String str = "INVOICE_ID BETWEEN " + to + " AND ";

        if (from != 0 && !(to <= from)) {
            JOptionPane.showMessageDialog(this, "Invoice ID range invalid. 'To' field should be less than 'From' field");
            isSomethingWrong = true;
            isInvalidRange = true;
        }

        if (isInvalidRange) {
            return " ";
        }

        if (from != 0 && !isInvalidRange) {
            str += from;
        } else {
            str += to;
        }
        str += " ";
        return str;
    }

    private String enablePrice() {
        boolean isInvalidRange = false;
        int to = 0, from = 0;
        to = Integer.parseInt(toPrice.getText().replaceAll(",", ""));

        String str = "COST BETWEEN " + to + " AND ";
        if (!fromPrice.getText().equals("")) {
            from = Integer.parseInt(fromPrice.getText().replaceAll(",", ""));
        }

        if (from != 0 && !(to <= from)) {
            JOptionPane.showMessageDialog(this, "Price range range invalid. 'To' field should be less than 'From' field");
            isSomethingWrong = true;
            isInvalidRange = true;
        }

        if (isInvalidRange) {
            return " ";
        }
        if (from != 0 && !isInvalidRange) {
            str += from;
        } else {
            str += to;
        }
        str += " ";
        return str;
    }

    private String isDateIncluded() {
        java.util.Date toDate = toDateChooser.getDate();
        java.util.Date fromDate = fromDateChooser.getDate();

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Dhaka"));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
        simpleDateFormat.setCalendar(cal);

        if (toDate != null && fromDate == null) {
            fromDateChooser.setDate(toDate);
            return "DATE BETWEEN '" + simpleDateFormat.format(toDateChooser.getDate()) + " 00:00:00' AND '" + simpleDateFormat.format(fromDateChooser.getDate()) + " 23:59:59'";
        } else if (toDate == null && fromDate != null) {
            toDateChooser.setDate(fromDate);
            return "DATE BETWEEN '" + simpleDateFormat.format(toDateChooser.getDate()) + " 00:00:00' AND '" + simpleDateFormat.format(fromDateChooser.getDate()) + " 23:59:59'";
        } else if (toDate != null && fromDate != null) {
            //check range validity
            if (!toDate.after(fromDate)) {
                //valid
                return "DATE BETWEEN '" + simpleDateFormat.format(toDateChooser.getDate()) + " 00:00:00' AND '" + simpleDateFormat.format(fromDateChooser.getDate()) + " 23:59:59'";
            } else {
                JOptionPane.showMessageDialog(this, "Date range invalid. 'To' date should be before 'From' date.", "Invalid Input", JOptionPane.ERROR_MESSAGE);
                return "";
            }
        }
        return "";
    }

    private String enableRetailWholesale() {
        String str = "ISRETAIL = '";
        if (checkBoxRetail.isSelected() && !checkBoxWholesale.isSelected()) {
            str += "true";
        } else if (!checkBoxRetail.isSelected() && checkBoxWholesale.isSelected()) {
            str += "false";
        } else if (checkBoxRetail.isSelected() && checkBoxWholesale.isSelected()) {
            str += "true' OR ISRETAIL = 'false";
        }
        str += "' ";
        return str;
    }

    void validateRangeFields() {
        if (!fromSerial.getText().equals("") && toSerial.getText().equals("")) {
            isSomethingWrong = true;
            JOptionPane.showMessageDialog(this, "Empty 'To Serial' field", "Input error", JOptionPane.ERROR_MESSAGE);
        }
        if (!fromInvoiceNo.getText().equals("") && toInvoiceNo.getText().equals("")) {
            isSomethingWrong = true;
            JOptionPane.showMessageDialog(this, "Empty 'To Invoice ID' field", "Input error", JOptionPane.ERROR_MESSAGE);
        }
        if (!fromPrice.getText().equals("") && toPrice.getText().equals("")) {
            isSomethingWrong = false;
            JOptionPane.showMessageDialog(this, "Empty 'To Price' field", "Input error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private String generateSQl() {
        boolean atLeastOneCriteriaSelected = false;
        boolean atLeastOneColumnSelected = false;
        getPreferredColumnNames();

        StringBuilder base = new StringBuilder("SELECT ");

        for (String column : columnNames) {
            base.append(column + ", ");
            atLeastOneColumnSelected = true;
        }
        if (atLeastOneColumnSelected) {
            base.deleteCharAt(base.lastIndexOf(","));
        } else {
            base.append(" SERIAL, PRODUCT, CUSTOMER, INVOICE_ID , COST AS PRICE, DATE, WARRANTY, ISRETAIL AS TYPE ");
        }

        //    base.deleteCharAt(base.length()-1);

        base.append(" FROM SELL_RECORDS WHERE ");

        if (!(cbCustomer.getSelectedItem().equals(""))) {
            base.append(enableCustomer() + "AND ");
            atLeastOneCriteriaSelected = true;
        }
        if (!(cbProduct.getSelectedItem().equals(""))) {
            base.append(enableProduct() + " AND ");
            atLeastOneCriteriaSelected = true;
        }
        if (!toSerial.getText().equals("") || !fromSerial.getText().equals("")) {
            base.append(enableSerial() + " AND ");
            atLeastOneCriteriaSelected = true;
        }
        if (!toInvoiceNo.getText().equals("") || !fromInvoiceNo.getText().equals("")) {
            base.append(enableInvoice() + " AND ");
            atLeastOneCriteriaSelected = true;
        }
        if (!toPrice.getText().equals("") || !fromPrice.getText().equals("")) {
            base.append(enablePrice() + " AND ");
            atLeastOneCriteriaSelected = true;
        }
        if (!(toDateChooser.getDate() == null) && !(fromDateChooser.getDate() == null)) {
            base.append(isDateIncluded() + " AND ");
            atLeastOneCriteriaSelected = true;
        }
        if (!((!checkBoxWholesale.isSelected()) && (!checkBoxRetail.isSelected()))) {
            base.append(enableRetailWholesale() + " AND ");
            atLeastOneCriteriaSelected = true;
        }

        if (atLeastOneCriteriaSelected) {
            int a = base.lastIndexOf("AND");
            base.delete(a, a + 3);
        } else {
            int a = base.indexOf("WHERE");
            base.delete(a, a + 5);
        }
        base.append(";");


        Debug.print("Query SQL " + base.toString(), "sellreportsitem");

        return base.toString();
    }

    private String[] getPreferredColumnNames() {
        ArrayList<String> list = new ArrayList<String>();
        columnNames = new ArrayList<String>();
        if (chkBSerialColumn.isSelected()) {
            list.add("Serial");
            columnNames.add("SERIAL");
        }
        if (chkBProductColumn.isSelected()) {
            list.add("Product");
            columnNames.add("PRODUCT");
        }
        if (chkBCustomerColumn.isSelected()) {
            list.add("Customer");
            columnNames.add("CUSTOMER");
        }
        if (chkBInvoiceID.isSelected()) {
            list.add("Invoice ID");
            columnNames.add("INVOICE_ID");
        }
        if (chkBPriceColumn.isSelected()) {
            list.add("Price");
            columnNames.add("COST AS PRICE");
        }
        if (chkBDate.isSelected()) {
            list.add("Date");
            columnNames.add("DATE");
        }
        if (chkBWarrantyColumn.isSelected()) {
            list.add("Warranty");
            columnNames.add("WARRANTY");
        }
        if (chkBTypeColumn.isSelected()) {
            list.add("Type");
            columnNames.add("ISRETAIL AS TYPE");
        }


        return (String[]) list.toArray(new String[list.size()]);

    }

    private void btnSearchActionPerformed(ActionEvent eb) {
        validateRangeFields();
        if (!isSomethingWrong) {
            //  generateSQl();

            Connection connection = null;
            java.sql.Statement statement = null;

            try {
                connection = DriverManager.getConnection("jdbc:sqlite:records.db");
                statement = connection.createStatement();

                String sql = generateSQl();
                Debug.print(sql, "XXXXXXX");
                ResultSet resultSet = statement.executeQuery(sql);
                /*SellReportTableModel model = ResultSet2TableModel(resultSet);*/
                model = resultSetToNonEditableTableModel(resultSet);
                table1.setModel(model);

                RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(model);
                table1.setRowSorter(sorter);

                resultSet.close();

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private TableModel resultSetToNonEditableTableModel(ResultSet rs) {
        try {
            ResultSetMetaData metaData = rs.getMetaData();
            int numberOfColumns = metaData.getColumnCount();
            final Vector<String> columnNames = new Vector<String>();

            // Get the column names
            for (int column = 0; column < numberOfColumns; column++) {
                columnNames.addElement(metaData.getColumnLabel(column + 1));
            }

            // Get all rows.
            Vector<Vector<Object>> rows = new Vector<Vector<Object>>();

            while (rs.next()) {
                Vector<Object> newRow = new Vector<Object>();

                for (int i = 1; i <= numberOfColumns; i++) {
                    newRow.addElement(rs.getObject(i));
                }

                rows.addElement(newRow);
            }

            return new DefaultTableModel(rows, columnNames) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }

                @Override
                public void setValueAt(Object aValue, int row, int column) {
                    if (aValue.toString().equals("true")) {
                        super.setValueAt("Retail", row, column);
                    } else if (aValue.toString().equals("false")) {
                        super.setValueAt("Wholesale", row, column);
                    } else {
                        super.setValueAt(aValue, row, column);
                    }
                }

                @Override
                public Object getValueAt(int row, int column) {
                    if (columnNames.get(column).equals("TYPE")) {
                        Vector<Object> rows = (Vector) dataVector.get(row);
                        if (rows.get(column).equals("true")) {
                            return "RETAIL";
                        } else {
                            return "WHOLESALE";
                        }
                    }

                    return super.getValueAt(row, column);
                }
            };
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    void loadData() {
        customerList = new ArrayList<String>();
        productList = new ArrayList<String>();

        cbCustomer.addItem("");
        cbProduct.addItem("");

        Connection connection = null;
        java.sql.Statement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            statement = connection.createStatement();

            String sql = "SELECT * FROM CUSTOMER;";
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                String temp = resultSet.getString("NAME");
                customerList.add(temp);
            }
            resultSet.close();
            sql = "SELECT * FROM PRODUCTS;";
            ResultSet rs2 = statement.executeQuery(sql);
            while (resultSet.next()) {
                productList.add(resultSet.getString("NAME"));
            }

            //ADD ARRAYLIST ELEMENTS TO RESPECTIVE CONTAINERS
            for (String s : customerList) {
                cbCustomer.addItem(s);
            }
            for (String ss : productList) {
                cbProduct.addItem(ss);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    //        connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void btnPrintActionPerformed(ActionEvent e) {
        //  SellReportItemWisePrinter sellReportItemWisePrinter = new SellReportItemWisePrinter(model);
//        sellReportItemWisePrinter.
        // sellReportItemWisePrinter.display();
        try {
            table1.print(JTable.PrintMode.FIT_WIDTH, new MessageFormat("Item wise Sale Report"), new MessageFormat("Page - {0}"));
        } catch (PrinterException e1) {
            e1.printStackTrace();
        }
    }

    private void okButtonActionPerformed(ActionEvent e) {
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        panel1 = new JPanel();
        label13 = new JLabel();
        cbCustomer = new JComboBox();
        label14 = new JLabel();
        cbProduct = new JComboBox();
        separator1 = new JSeparator();
        label21 = new JLabel();
        label22 = new JLabel();
        toSerial = new JFormattedTextField(numberFormatForTextFields);
        label23 = new JLabel();
        fromSerial = new JFormattedTextField(numberFormatForTextFields);
        label24 = new JLabel();
        label25 = new JLabel();
        toInvoiceNo = new JFormattedTextField(numberFormatForTextFields);
        label26 = new JLabel();
        fromInvoiceNo = new JFormattedTextField(numberFormatForTextFields);
        toPrice = new JFormattedTextField(numberFormatForTextFields);
        label27 = new JLabel();
        fromPrice = new JFormattedTextField(numberFormatForTextFields);
        label28 = new JLabel();
        label29 = new JLabel();
        label30 = new JLabel();
        label31 = new JLabel();
        toDateChooser = new JDateChooser();
        label32 = new JLabel();
        fromDateChooser = new JDateChooser();
        checkBoxRetail = new JCheckBox();
        checkBoxWholesale = new JCheckBox();
        label33 = new JLabel();
        chkBSerialColumn = new JCheckBox();
        chkBProductColumn = new JCheckBox();
        chkBCustomerColumn = new JCheckBox();
        chkBPriceColumn = new JCheckBox();
        chkBDate = new JCheckBox();
        chkBWarrantyColumn = new JCheckBox();
        chkBTypeColumn = new JCheckBox();
        btnSearch = new JButton();
        chkBInvoiceID = new JCheckBox();
        panel2 = new JPanel();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();
        btnPrint = new JButton();
        buttonBar = new JPanel();
        okButton = new JButton();

        //======== this ========
        setTitle("Sell Reports (Item Wise)");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setModal(true);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(8, 8, 8, 8));


            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {

                //======== panel1 ========
                {
                    panel1.setBorder(new TitledBorder("Search Criteria"));

                    //---- label13 ----
                    label13.setText("Customer");

                    //---- label14 ----
                    label14.setText("Product");

                    //---- separator1 ----
                    separator1.setOrientation(SwingConstants.VERTICAL);

                    //---- label21 ----
                    label21.setText("Serial");

                    //---- label22 ----
                    label22.setText("To");

                    //---- label23 ----
                    label23.setText("From");

                    //---- label24 ----
                    label24.setText("Invoice ID");

                    //---- label25 ----
                    label25.setText("To");

                    //---- label26 ----
                    label26.setText("From");

                    //---- label27 ----
                    label27.setText("To");

                    //---- label28 ----
                    label28.setText("From");

                    //---- label29 ----
                    label29.setText("Price");

                    //---- label30 ----
                    label30.setText("Date");

                    //---- label31 ----
                    label31.setText("From");

                    //---- label32 ----
                    label32.setText("To");

                    //---- checkBoxRetail ----
                    checkBoxRetail.setText("Retail");

                    //---- checkBoxWholesale ----
                    checkBoxWholesale.setText("Wholesale");

                    //---- label33 ----
                    label33.setText("Select output columns");

                    //---- chkBSerialColumn ----
                    chkBSerialColumn.setText("Serial");

                    //---- chkBProductColumn ----
                    chkBProductColumn.setText("Product");

                    //---- chkBCustomerColumn ----
                    chkBCustomerColumn.setText("Customer");

                    //---- chkBPriceColumn ----
                    chkBPriceColumn.setText("Price");

                    //---- chkBDate ----
                    chkBDate.setText("Date");

                    //---- chkBWarrantyColumn ----
                    chkBWarrantyColumn.setText("Warranty");

                    //---- chkBTypeColumn ----
                    chkBTypeColumn.setText("Type");

                    //---- btnSearch ----
                    btnSearch.setText("Search");
                    btnSearch.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnSearchActionPerformed(e);
                        }
                    });

                    //---- chkBInvoiceID ----
                    chkBInvoiceID.setText("Invoice ID");

                    GroupLayout panel1Layout = new GroupLayout(panel1);
                    panel1.setLayout(panel1Layout);
                    panel1Layout.setHorizontalGroup(
                            panel1Layout.createParallelGroup()
                                    .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                            .addGroup(panel1Layout.createParallelGroup()
                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                            .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                            .addComponent(label13)
                                                            .addGap(18, 18, 18))
                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                            .addContainerGap()
                                                            .addGroup(panel1Layout.createParallelGroup()
                                                                    .addComponent(label21)
                                                                    .addComponent(label29))
                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                    .addComponent(cbCustomer, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE)
                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                            .addComponent(label22)
                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                            .addComponent(toSerial, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                            .addComponent(label23)
                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                            .addComponent(fromSerial, GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE))
                                                    .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                                            .addGroup(panel1Layout.createParallelGroup()
                                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                                            .addComponent(label27)
                                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                            .addComponent(toPrice, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
                                                                    .addComponent(checkBoxRetail))
                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                            .addGroup(panel1Layout.createParallelGroup()
                                                                    .addComponent(checkBoxWholesale)
                                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                                            .addComponent(label28)
                                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                            .addComponent(fromPrice, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)))))
                                            .addGap(18, 18, 18)
                                            .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 2, GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(panel1Layout.createParallelGroup()
                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                            .addComponent(label14)
                                                            .addGap(18, 18, 18)
                                                            .addComponent(cbProduct, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE))
                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                            .addGroup(panel1Layout.createParallelGroup()
                                                                    .addComponent(label24)
                                                                    .addComponent(label30))
                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                            .addGroup(panel1Layout.createParallelGroup()
                                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                                            .addComponent(label25)
                                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                            .addComponent(toInvoiceNo, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
                                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                                            .addComponent(label31)
                                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                            .addComponent(toDateChooser, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)))
                                                            .addGap(18, 18, 18)
                                                            .addGroup(panel1Layout.createParallelGroup()
                                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                                            .addComponent(label32)
                                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                            .addComponent(fromDateChooser, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE))
                                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                                            .addComponent(label26)
                                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                            .addComponent(fromInvoiceNo)))))
                                            .addContainerGap())
                                    .addGroup(panel1Layout.createSequentialGroup()
                                            .addContainerGap()
                                            .addComponent(label33)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(chkBSerialColumn)
                                            .addGap(18, 18, 18)
                                            .addComponent(chkBProductColumn)
                                            .addGap(18, 18, 18)
                                            .addComponent(chkBCustomerColumn)
                                            .addGap(18, 18, 18)
                                            .addComponent(chkBPriceColumn)
                                            .addGap(18, 18, 18)
                                            .addComponent(chkBDate)
                                            .addGap(18, 18, 18)
                                            .addComponent(chkBWarrantyColumn)
                                            .addGap(18, 18, 18)
                                            .addComponent(chkBTypeColumn)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(chkBInvoiceID)
                                            .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                            .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(btnSearch, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                                            .addGap(340, 340, 340))
                    );
                    panel1Layout.linkSize(SwingConstants.HORIZONTAL, new Component[]{cbCustomer, cbProduct});
                    panel1Layout.linkSize(SwingConstants.HORIZONTAL, new Component[]{fromSerial, toInvoiceNo, toSerial});
                    panel1Layout.setVerticalGroup(
                            panel1Layout.createParallelGroup()
                                    .addGroup(panel1Layout.createSequentialGroup()
                                            .addGap(0, 0, 0)
                                            .addGroup(panel1Layout.createParallelGroup()
                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                                    .addComponent(label13)
                                                                    .addComponent(cbCustomer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                                    .addComponent(label21)
                                                                    .addComponent(label22)
                                                                    .addComponent(toSerial, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                                    .addComponent(label23)
                                                                    .addComponent(fromSerial))
                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                                    .addComponent(label27)
                                                                    .addComponent(label28)
                                                                    .addComponent(fromPrice, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                                    .addComponent(label29)
                                                                    .addComponent(toPrice, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                                    .addComponent(checkBoxRetail)
                                                                    .addComponent(checkBoxWholesale)))
                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                                    .addComponent(label14)
                                                                    .addComponent(cbProduct, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                                    .addComponent(label25)
                                                                    .addComponent(toInvoiceNo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                                    .addComponent(label26)
                                                                    .addComponent(fromInvoiceNo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                                    .addComponent(label24))
                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                                    .addComponent(label30)
                                                                    .addComponent(label31)
                                                                    .addComponent(toDateChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                                    .addComponent(label32)
                                                                    .addComponent(fromDateChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                            .addGap(0, 0, Short.MAX_VALUE))
                                                    .addComponent(separator1, GroupLayout.Alignment.TRAILING))
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                    .addComponent(label33)
                                                    .addComponent(chkBSerialColumn)
                                                    .addComponent(chkBProductColumn)
                                                    .addComponent(chkBCustomerColumn)
                                                    .addComponent(chkBPriceColumn)
                                                    .addComponent(chkBDate)
                                                    .addComponent(chkBWarrantyColumn)
                                                    .addComponent(chkBTypeColumn)
                                                    .addComponent(chkBInvoiceID))
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(btnSearch)
                                            .addContainerGap())
                    );
                }

                //======== panel2 ========
                {
                    panel2.setBorder(new TitledBorder("Search Results"));

                    //======== scrollPane1 ========
                    {
                        scrollPane1.setViewportView(table1);
                    }

                    //---- btnPrint ----
                    btnPrint.setText("Print");
                    btnPrint.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnPrintActionPerformed(e);
                        }
                    });

                    GroupLayout panel2Layout = new GroupLayout(panel2);
                    panel2.setLayout(panel2Layout);
                    panel2Layout.setHorizontalGroup(
                            panel2Layout.createParallelGroup()
                                    .addGroup(panel2Layout.createSequentialGroup()
                                            .addContainerGap()
                                            .addGroup(panel2Layout.createParallelGroup()
                                                    .addComponent(scrollPane1)
                                                    .addGroup(panel2Layout.createSequentialGroup()
                                                            .addComponent(btnPrint, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
                                                            .addGap(0, 0, Short.MAX_VALUE)))
                                            .addContainerGap())
                    );
                    panel2Layout.setVerticalGroup(
                            panel2Layout.createParallelGroup()
                                    .addGroup(panel2Layout.createSequentialGroup()
                                            .addContainerGap()
                                            .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                                            .addComponent(btnPrint)
                                            .addContainerGap())
                    );
                }

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(GroupLayout.Alignment.TRAILING, contentPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addContainerGap())
                );
                contentPanelLayout.setVerticalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[]{0, 80};
                ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[]{1.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        okButtonActionPerformed(e);
                    }
                });
                buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    } // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}

