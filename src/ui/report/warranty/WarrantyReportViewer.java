package ui.report.warranty;

import net.sf.dynamicreports.examples.Templates;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.builder.component.VerticalListBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.exception.DRException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

/**
 * Created by Tareq on 6/5/2014.
 */
public class WarrantyReportViewer {
    Date to, from;
    String toDateStr, fromDateStr;

    public WarrantyReportViewer(Date to, Date from) {
        this.to = to;
        this.from = from;
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
        toDateStr = formatter.format(to);
        fromDateStr = formatter.format(from);
        build();
    }

    private void build() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
        String sqlToDateStr = dateFormat.format(to).concat(" 00:00:00");
        String sqlFromDateStr = dateFormat.format(from).concat(" 00:00:00");

        String sql = "SELECT CUSTOMER, PRODUCT, SERIAL, DATE, STATEMENT, DELIVERY_DATE, MAN_ID" +
                " FROM WARRANTY_PROCESSING WHERE" +
                " IS_RECEIVED_REPLACEMENT = 'true' AND" +
                " IS_DELIVERED_TO_CUSTOMER = 'true' AND " +
                " IS_SENT_TO_MANU = 'true' AND " +
                " DATE BETWEEN '" + sqlFromDateStr + "' AND '" + sqlToDateStr + "';";


        StyleBuilder columnStyle = stl.style(Templates.columnStyle)
                .setBorder(stl.pen2Point()).setHorizontalAlignment(HorizontalAlignment.CENTER);


        TextColumnBuilder<Integer> rowIDColumn = col.columnRowNumberColumn(" # ")
                .setFixedColumns(2)
                .setHorizontalAlignment(HorizontalAlignment.CENTER);

        TextColumnBuilder<String> customerColumn = col.column("Customer", "CUSTOMER", type.stringType());
        TextColumnBuilder<String> productColumn = col.column("Product", "PRODUCT", type.stringType());
        TextColumnBuilder<Integer> serialColumn = col.column("Serial", "SERIAL", type.integerType());
        TextColumnBuilder<String> manIDColumn = col.column("Man.ID", "MAN_ID", type.stringType());
        TextColumnBuilder<String> statementColumn = col.column("Statement", "STATEMENT", type.stringType());
        //    TextColumnBuilder<Date> deliveryDateColumn = col.column("Delivery Date", "DELIVERY_DATE", type.dateYearToMonthType());


        try {
            report().title(createTitleComponent())
                    .setTemplate(Templates.reportTemplate)
                    .columns(rowIDColumn, customerColumn, productColumn, serialColumn, manIDColumn, statementColumn)
                    .setColumnStyle(columnStyle)
                    .pageFooter(cmp.pageXofY())
                    .setPageMargin(margin(40))
                    .setDataSource(sql, connection).show(false);
        } catch (DRException e) {
            e.printStackTrace();
        }

    }


    private ComponentBuilder<?, ?> createTitleComponent() {
        VerticalListBuilder list = cmp.verticalList();
        list.add(
                cmp.image("res\\logo.png").setFixedDimension(150, 60),
                cmp.text("All Car Decor").setStyle(stl.style().bold().setFontSize(18)).setHorizontalAlignment(HorizontalAlignment.LEFT),
                cmp.text("145, South Shahjahanpur "),
                cmp.text("Dhaka, Bangladesh"),
                cmp.text("+880 1713115944, +880 1613115944"),
                cmp.line(),
                cmp.verticalGap(10),
                cmp.text("Warranty Items Report").setStyle(stl.style().bold().setFontSize(20)).setHorizontalAlignment(HorizontalAlignment.CENTER),
                cmp.text(fromDateStr + " - " + toDateStr).setStyle(stl.style().bold().setFontSize(15)).setHorizontalAlignment(HorizontalAlignment.CENTER),
                cmp.verticalGap(10)
        );
        return list;
    }


}

