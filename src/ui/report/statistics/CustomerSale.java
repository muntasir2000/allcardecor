package ui.report.statistics;

/**
 * Created by Tareq on 6/6/2014.
 */
public class CustomerSale {
    private String name;
    private Integer count;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
