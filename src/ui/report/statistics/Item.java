package ui.report.statistics;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Tareq on 6/5/2014.
 */
public class Item {
    private String customer;
    private String product;
    private BigDecimal price;
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

}
