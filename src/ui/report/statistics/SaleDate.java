package ui.report.statistics;

import java.util.Date;

/**
 * Created by Tareq on 6/6/2014.
 */
public class SaleDate {
    private Date date;
    private Integer sale;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getSale() {
        return sale;
    }

    public void setSale(Integer sale) {
        this.sale = sale;
    }
}
