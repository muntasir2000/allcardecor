package ui.report.statistics;

import core.debug.Debug;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Tareq on 6/5/2014.
 */
public class StatData {
    Date toDate, fromDate;
    ArrayList<Item> listItems;
    HashMap<Date, Integer> saleByDateMap;
    HashMap<String, Integer> saleByProductMap;
    HashMap<String, Integer> saleByCustomerMap;

    public StatData(Date from, Date to) {
        toDate = to;
        fromDate = from;
    }

    public void loadData() {
        listItems = new ArrayList<Item>();
        saleByDateMap = new HashMap<Date, Integer>();
        saleByProductMap = new HashMap<String, Integer>();
        saleByCustomerMap = new HashMap<String, Integer>();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
        String toDateStr = simpleDateFormat.format(toDate);
        String fromDateStr = simpleDateFormat.format(fromDate);

        String datePattern = "YYYY-MM-dd";
        Connection connection = null;
        java.sql.Statement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            statement = connection.createStatement();
            String sql = "SELECT PRODUCT, CUSTOMER, COST, DATE, ISRETAIL FROM SELL_RECORDS WHERE DATE BETWEEN '" + fromDateStr + "' AND '" + toDateStr + "';";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Item item = new Item();
                String productName = rs.getString("PRODUCT");
                item.setProduct(productName);
                String customerName = rs.getString("CUSTOMER");

                item.setCustomer(customerName);
                int cost = rs.getInt("COST");
                item.setPrice(new BigDecimal(cost));
                String dateStr = rs.getString("DATE");
                dateStr = dateStr.substring(0, 10);
                DateTime dateTime = DateTime.parse(dateStr, DateTimeFormat.forPattern(datePattern));

                Date date = dateTime.toDate();

                item.setDate(date);
                Integer temp = saleByDateMap.get(date);
                if (temp != null) {
                    saleByDateMap.put(dateTime.toDate(), temp + cost);
                } else {
                    saleByDateMap.put(dateTime.toDate(), cost);
                }

                temp = saleByProductMap.get(productName);
                if (temp != null) {
                    saleByProductMap.put(productName, temp + 1);
                } else {
                    saleByProductMap.put(productName, 1);
                }

                temp = saleByCustomerMap.get(customerName);
                if (temp != null) {
                    saleByCustomerMap.put(customerName, temp + 1);
                } else {
                    saleByCustomerMap.put(customerName, 1);
                }
                listItems.add(item);
            }

            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public JRDataSource generateDataSourceForTimeChart() {
        ArrayList<SaleDate> list = new ArrayList<SaleDate>();

        for (Date d : saleByDateMap.keySet()) {
            SaleDate saleDate = new SaleDate();
            Debug.print(d.toString(), "XXXXXXX ");
            saleDate.setDate(d);
            saleDate.setSale(saleByDateMap.get(d));
            list.add(saleDate);
        }
        return new JRBeanCollectionDataSource(list);
    }

    public JRDataSource generateDataSourceForProductBarChart() {
        ArrayList<ProductSale> list = new ArrayList<ProductSale>();

        for (String s : saleByProductMap.keySet()) {
            ProductSale productSale = new ProductSale();
            productSale.setProduct(s);
            productSale.setCount(saleByProductMap.get(s));
            list.add(productSale);
        }
        return new JRBeanCollectionDataSource(list);
    }

    public JRDataSource generateDataSourceForCustomerBarChart() {
        ArrayList<CustomerSale> list = new ArrayList<CustomerSale>();

        for (String s : saleByCustomerMap.keySet()) {
            CustomerSale customerSale = new CustomerSale();

            customerSale.setName(s);
            customerSale.setCount(saleByCustomerMap.get(s));
            list.add(customerSale);
        }
        return new JRBeanCollectionDataSource(list);

    }


    public JRDataSource createDataSource() {
        return new JRBeanCollectionDataSource(listItems);
    }
}
