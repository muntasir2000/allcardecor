package ui.report.statistics;

import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.builder.component.Components;
import net.sf.dynamicreports.report.builder.component.SubreportBuilder;
import net.sf.dynamicreports.report.builder.component.VerticalListBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.constant.Orientation;
import net.sf.dynamicreports.report.exception.DRException;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

public class StatisticsReportGenerator {
    StatData data;
    StyleBuilder chartTitleStyle = stl.style().setFontSize(15).setHorizontalAlignment(HorizontalAlignment.CENTER);
    private String toDateStr;
    private String fromDateStr;

    public StatisticsReportGenerator(Date from, Date to) {
        data = new StatData(from, to);

        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
        toDateStr = formatter.format(new DateTime(to).minusDays(1).toDate());
        fromDateStr = formatter.format(new DateTime(from).plusDays(1).toDate());
        data.loadData();
        showReport();
    }

    void showReport() {
        StyleBuilder titleStyle = stl.style().setHorizontalAlignment(HorizontalAlignment.CENTER);
        StyleBuilder titleTextStyle = titleStyle.setFontSize(20);

        SubreportBuilder subReport = cmp.subreport(timeSeriesChart());
        SubreportBuilder productSaleBarChart = cmp.subreport(productBarChart());
        SubreportBuilder customerSaleBarChart = cmp.subreport(customerBarChart());

        TextColumnBuilder<Date> sellingDate = col.column("Date", "date", type.dateType());
        TextColumnBuilder<String> productName = col.column("Product", "product", type.stringType());
        TextColumnBuilder<BigDecimal> priceColumn = col.column("Price", "price", type.bigDecimalType());
        TextColumnBuilder<String> customerColumn = col.column("Customer", "customer", type.stringType());
        JasperReportBuilder report = DynamicReports.report();//a new report
        report
                .title(//title of the report
                        createTitleComponent().setStyle(titleStyle),
                        cmp.line(),

                        cmp.text("Sales Statistics").setStyle(titleTextStyle),
                        cmp.text(fromDateStr + "  -  " + toDateStr).setStyle(titleTextStyle),
                        cmp.line()
                )
                .summary(
                        subReport,

                        productSaleBarChart,

                        customerSaleBarChart

                )
                .pageFooter(Components.pageXofY())
                        //show page number on the page footer
                .setDataSource(data.createDataSource());

        try {
            //show the report
            report.setPageMargin(margin(40));
            report.show(false);


            //export the report to a pdf file
            //   report.toPdf(new FileOutputStream("c:/report.pdf"));
        } catch (DRException e) {
            e.printStackTrace();
        }
    }

    private ComponentBuilder<?, ?> createTitleComponent() {
        VerticalListBuilder list = cmp.verticalList();
        list.add(
                cmp.image("res\\logo.png").setFixedDimension(150, 60),
                cmp.text("All Car Decor").setStyle(stl.style().bold().setFontSize(18)).setHorizontalAlignment(HorizontalAlignment.LEFT),
                cmp.text("145, South Shahjahanpur "),
                cmp.text("Dhaka, Bangladesh"),
                cmp.text("+880 1713115944, +880 1613115944"));
        return list;
    }

    private JasperReportBuilder timeSeriesChart() {
        TextColumnBuilder<Date> sellingDateColumn = col.column("Date", "date", type.dateDayType());
        TextColumnBuilder<Integer> totalSaleColumn = col.column("Total Sale", "sale", type.integerType());

        //    sellingDateColumn.
        return report()
                .title(cmp.text("Sale By Date").setStyle(chartTitleStyle))
                .summary(
                        cht.timeSeriesChart()
                                .setTimePeriod(sellingDateColumn)
                                .addSerie(
                                        cht.serie(totalSaleColumn)
                                )
                                .setOrientation(Orientation.HORIZONTAL)
                ).setDataSource(data.generateDataSourceForTimeChart());
    }

    private JasperReportBuilder productBarChart() {
        TextColumnBuilder<String> productColumn = col.column("Product", "product", type.stringType());
        TextColumnBuilder<Integer> countColumn = col.column("Count", "count", type.integerType());

        return report()
                .title(cmp.text("Sale by Product").setStyle(chartTitleStyle))
                .summary(
                        cht.bar3DChart()
                                .setCategory(productColumn)
                                .addSerie(
                                        cht.serie(countColumn)
                                )
                                .setOrientation(Orientation.HORIZONTAL)
                )
                .setDataSource(data.generateDataSourceForProductBarChart());
    }


    private JasperReportBuilder customerBarChart() {
        TextColumnBuilder<String> customerColumn = col.column("Customer", "name", type.stringType());
        TextColumnBuilder<Integer> countColumn = col.column("Count", "count", type.integerType());

        return report()
                .title(cmp.text("Sale by Customer").setStyle(chartTitleStyle))
                .summary(
                        cht.bar3DChart()
                                .setCategory(customerColumn)
                                .addSerie(
                                        cht.serie(countColumn)
                                )
                                .setOrientation(Orientation.HORIZONTAL)
                )
                .setDataSource(data.generateDataSourceForCustomerBarChart());
    }
}
