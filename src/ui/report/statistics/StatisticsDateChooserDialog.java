/*
 * Created by JFormDesigner on Sat Jun 07 02:21:27 ALMT 2014
 */

package ui.report.statistics;

import com.toedter.calendar.JDateChooser;
import org.joda.time.DateTime;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Tareq Muntasir
 */
public class StatisticsDateChooserDialog extends JDialog {


    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel label1;
    private JDateChooser fromDateChooser;
    private JLabel label2;
    private JDateChooser toDateChooser;
    private JLabel label3;
    private JPanel buttonBar;
    private JButton okButton;
    private JButton cancelButton;

    public StatisticsDateChooserDialog(Frame owner) {
        super(owner);
        initComponents();
        // first midnight in this month
        DateTime firstDayOfMonth = new DateTime().dayOfMonth().withMinimumValue();
        DateTime lastDayOfMonth = new DateTime().dayOfMonth().withMaximumValue();
        toDateChooser.setDate(lastDayOfMonth.toDate());
        fromDateChooser.setDate(firstDayOfMonth.toDate());
    }

    private void okButtonActionPerformed(ActionEvent e) {
        DateTime to, from;
        to = new DateTime(toDateChooser.getDate());
        from = new DateTime(fromDateChooser.getDate());

        //minus one day because sql query will be called with between. That means less than, not less than or equal to

        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
     /*   if (from.isAfter(to)) {
            StatisticsReportGenerator stat = new StatisticsReportGenerator(to.minusDays(1).toDate(), from.plusDays(1).toDate());
        } else {
            StatisticsReportGenerator stat = new StatisticsReportGenerator(from.minusDays(1).toDate(), to.plusDays(1).toDate());
        }*/

        if (from.isBefore(to)) {
            StatisticsReportGenerator stat = new StatisticsReportGenerator(from.minusDays(1).toDate(), to.plusDays(1).toDate());
        } else {
            StatisticsReportGenerator stat = new StatisticsReportGenerator(to.minusDays(1).toDate(), from.plusDays(1).toDate());
        }
        this.setCursor(Cursor.getDefaultCursor());
    }

    private void cancelButtonActionPerformed(ActionEvent e) {
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        label1 = new JLabel();
        fromDateChooser = new JDateChooser();
        label2 = new JLabel();
        toDateChooser = new JDateChooser();
        label3 = new JLabel();
        buttonBar = new JPanel();
        okButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        setTitle("Statistics Viewer");
        setModal(true);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));


            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {

                //---- label1 ----
                label1.setText("From ");

                //---- label2 ----
                label2.setText("To");

                //---- label3 ----
                label3.setText("Select Date Range");

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                                .addComponent(label2)
                                                                .addComponent(label1))
                                                        .addGap(18, 18, 18)
                                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                                .addComponent(fromDateChooser, GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE)
                                                                .addComponent(toDateChooser, GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE)))
                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                        .addComponent(label3)
                                                        .addGap(0, 164, Short.MAX_VALUE)))
                                        .addContainerGap())
                );
                contentPanelLayout.setVerticalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(GroupLayout.Alignment.TRAILING, contentPanelLayout.createSequentialGroup()
                                        .addComponent(label3)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                .addComponent(label1, GroupLayout.Alignment.TRAILING)
                                                .addComponent(fromDateChooser, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                .addComponent(label2)
                                                .addComponent(toDateChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                        .addGap(12, 12, 12))
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[]{0, 85, 80};
                ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[]{1.0, 0.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        okButtonActionPerformed(e);
                    }
                });
                buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        cancelButtonActionPerformed(e);
                    }
                });
                buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
