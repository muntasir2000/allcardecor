/*
 * Created by JFormDesigner on Thu Jun 05 16:12:45 ALMT 2014
 */

package ui.report;

import com.toedter.calendar.JDateChooser;
import core.debug.Debug;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.sql.*;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * @author Tareq Muntasir
 */
public class StockInventoryReport extends JDialog {
    private boolean isSomethingWrong = false;
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel panel1;
    private JLabel label1;
    private JComboBox cbSupplier;
    private JLabel label2;
    private JComboBox cbProduct;
    private JLabel label3;
    private JLabel label4;
    private JFormattedTextField tfToSerial;
    private JLabel label5;
    private JFormattedTextField tfFromSerial;
    private JLabel label6;
    private JLabel label7;
    private JFormattedTextField tfToInvoice;
    private JLabel label8;
    private JFormattedTextField tfFromInvoice;
    private JLabel label11;
    private JLabel label12;
    private JFormattedTextField tfToCost;
    private JLabel label13;
    private JFormattedTextField tfFromCost;
    private JLabel label14;
    private JLabel label15;
    private JLabel label16;
    private JDateChooser toDateChooser;
    private JDateChooser fromDateChooser;
    private JCheckBox includeSoldItemCheckbox;
    private JButton searchBtn;
    private JLabel label17;
    private JCheckBox outputSerialCheckBox;
    private JCheckBox outputProductCheckBox;
    private JCheckBox outputInvoiceCheckBox;
    private JCheckBox outputCostChecBox;
    private JCheckBox outputDateCheckBox;
    private JCheckBox outputWarrantyCheckBox;
    private JCheckBox outputSupplierCheckBox;
    private JPanel panel2;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JButton printBtn;
    private JButton button3;

    public StockInventoryReport(Frame owner) {
        super(owner);
        initComponents();
        loadData();
    }

    void loadData() {
        Connection connection = null;
        Statement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            statement = connection.createStatement();
            String sql = "SELECT NAME FROM SUPPLIER;";
            ResultSet rs = statement.executeQuery(sql);

            cbSupplier.addItem(""); // add an empty element for if the user wants to exclude customer
            while (rs.next()) {
                cbSupplier.addItem(rs.getString("NAME"));
            }
            rs.close();

            sql = "SELECT NAME FROM PRODUCTS;";
            rs = statement.executeQuery(sql);
            cbProduct.addItem("");
            while (rs.next()) {
                cbProduct.addItem(rs.getString("NAME"));
            }
            rs.close();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private String generateSQL() {
        boolean atLeastOneColumnSelected = false;
        boolean atLeastOneCriterionSelected = false;
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");

        if (outputSerialCheckBox.isSelected()) {
            sql.append("STOCK_INVENTORY.SERIAL,");
            atLeastOneColumnSelected = true;
        }
        if (outputProductCheckBox.isSelected()) {
            sql.append("STOCK_INVENTORY.PRODUCT,");
            atLeastOneColumnSelected = true;
        }
        if (outputDateCheckBox.isSelected()) {
            sql.append("STOCK_INVENTORY.DATE,");
            atLeastOneColumnSelected = true;
        }
        if (outputCostChecBox.isSelected()) {
            sql.append("STOCK_INVENTORY.COST,");
            atLeastOneColumnSelected = true;
        }
        if (outputSupplierCheckBox.isSelected()) {
            sql.append("STOCK_INVOICE.SUPPLIER,");
            atLeastOneColumnSelected = true;
        }
        if (outputInvoiceCheckBox.isSelected()) {
            sql.append("STOCK_INVENTORY.INVOICE_ID,");
            atLeastOneColumnSelected = true;
        }
        if (outputWarrantyCheckBox.isSelected()) {
            sql.append("STOCK_INVENTORY.WARRANTY,");
            atLeastOneColumnSelected = true;
        }


        if (atLeastOneColumnSelected) {
            sql.deleteCharAt(sql.lastIndexOf(","));
        } else {
            // sql.append(" * ");
            sql.append(" STOCK_INVENTORY.SERIAL, STOCK_INVENTORY.PRODUCT, STOCK_INVENTORY.INVOICE_ID, STOCK_INVENTORY.COST, STOCK_INVENTORY.DATE, STOCK_INVENTORY.WARRANTY, STOCK_INVOICE.SUPPLIER ");
        }

        sql.append(" FROM STOCK_INVENTORY, STOCK_INVOICE WHERE STOCK_INVENTORY.INVOICE_ID = STOCK_INVOICE.ID AND ");

        if (!isSupplierInclueded().equals("")) {
            sql.append(isSupplierInclueded() + " AND ");
            atLeastOneCriterionSelected = true;
        }
        if (!isProductInclueded().equals("")) {
            sql.append(isProductInclueded() + " AND ");
            atLeastOneCriterionSelected = true;
        }
        if (!isSerialIncluded().equals("")) {
            sql.append(isSerialIncluded() + " AND ");
            atLeastOneCriterionSelected = true;
        }
        if (!isCostIncluded().equals("")) {
            sql.append(isCostIncluded() + " AND ");
            atLeastOneCriterionSelected = true;
        }

        if (!isInvoiceIDIncluded().equals("")) {
            sql.append(isInvoiceIDIncluded() + " AND ");
            atLeastOneCriterionSelected = true;
        }
        if (!isDateIncluded().equals("")) {
            sql.append(isDateIncluded() + " AND ");
            atLeastOneCriterionSelected = true;
        }
        if (!isSoldItemIncluded().equals("")) {
            Debug.print("Found the culprit", "SS");
            sql.append(isSoldItemIncluded() + "  AND ");
            atLeastOneCriterionSelected = true;


        }

        if (atLeastOneCriterionSelected) {
            int i = sql.lastIndexOf("AND");
            sql.delete(i, i + 3);
        } else {
            Debug.print("XXXXXXXXXXXXXXXXX", "kddfdf");
            int i = sql.lastIndexOf("WHERE");
            sql.delete(i, i + 5);
        }
        sql.append(";");

        Debug.print(sql.toString(), "Generated SQL: ");
        return sql.toString();
    }

    private String isSupplierInclueded() {
        if (!cbSupplier.getSelectedItem().equals("")) {
            return "STOCK_INVOICE.SUPPLIER = '" + cbSupplier.getSelectedItem().toString() + "'";
        } else {
            isSomethingWrong = false;
            return "";
        }
    }

    private String isProductInclueded() {
        if (!cbProduct.getSelectedItem().equals("")) {
            return "STOCK_INVENTORY.PRODUCT = '" + cbProduct.getSelectedItem().toString() + "'";
        } else {
            isSomethingWrong = false;
            return "";
        }
    }

    private String isInvoiceIDIncluded() {
        if (tfToInvoice.getText().equals("") && tfFromInvoice.getText().equals("")) {
            // isSomethingWrong = true;
            return "";
        } else if (tfToInvoice.getText().equals("") && !tfFromInvoice.getText().equals("")) {
            //isSomethingWrong = true;
            tfToInvoice.setText(tfFromInvoice.getText());
            return "STOCK_INVENTORY.INVOICE_ID BETWEEN " + tfToInvoice.getText() + " AND " + tfFromInvoice.getText();
        } else if (!tfToInvoice.getText().equals("") && tfFromInvoice.getText().equals("")) {
            tfFromInvoice.setText(tfToInvoice.getText());
            return "STOCK_INVENTORY.INVOICE_ID BETWEEN " + tfToInvoice.getText() + " AND " + tfFromInvoice.getText();
        } else if (!tfToInvoice.getText().equals("") && !tfFromInvoice.getText().equals("")) {

            //        return "ID BETWEEN " + toInvoiceID.getText() + " AND " + fromInvoiceID.getText();
            if (isRangeVaild(tfToInvoice.getText(), tfFromInvoice.getText())) {
                return "STOCK_INVENTORY.INVOICE_ID BETWEEN " + tfToInvoice.getText() + " AND " + tfFromInvoice.getText();
            } else {
                JOptionPane.showMessageDialog(this, "Invalid Range for Invoice ID. Range not allowed. 'To' field must be less than 'From' field.", "Invalid range", JOptionPane.ERROR_MESSAGE);
                return "";
            }

        } else {
            isSomethingWrong = false;
            return "";
        }
    }

    private String isSerialIncluded() {
        if (tfToSerial.getText().equals("") && tfFromSerial.getText().equals("")) {
            // isSomethingWrong = true;
            return "";
        } else if (tfToSerial.getText().equals("") && !tfFromSerial.getText().equals("")) {
            //isSomethingWrong = true;
            tfToSerial.setText(tfFromSerial.getText());
            return "STOCK_INVENTORY.SERIAL BETWEEN " + tfToInvoice.getText() + " AND " + tfFromInvoice.getText();
        } else if (!tfToSerial.getText().equals("") && tfFromSerial.getText().equals("")) {
            tfFromSerial.setText(tfToSerial.getText());
            return "STOCK_INVENTORY.SERIAL BETWEEN " + tfToSerial.getText() + " AND " + tfFromSerial.getText();
        } else if (!tfToSerial.getText().equals("") && !tfFromSerial.getText().equals("")) {

            //        return "ID BETWEEN " + toInvoiceID.getText() + " AND " + fromInvoiceID.getText();
            if (isRangeVaild(tfToSerial.getText(), tfFromSerial.getText())) {
                return "STOCK_INVENTORY.SERIAL BETWEEN " + tfToSerial.getText() + " AND " + tfFromSerial.getText();
            } else {
                JOptionPane.showMessageDialog(this, "Invalid Range for Invoice ID. Range not allowed. 'To' field must be less than 'From' field.", "Invalid range", JOptionPane.ERROR_MESSAGE);
                return "";
            }

        } else {
            isSomethingWrong = false;
            return "";
        }
    }

    boolean isRangeVaild(String a, String b) {
        int i = 0, j = 0;
        try {
            i = Integer.parseInt(a);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        try {
            j = Integer.parseInt(b);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        if (i > j) {
            return false;
        } else return true;
    }

    private String isDateIncluded() {
        java.util.Date toDate = toDateChooser.getDate();
        java.util.Date fromDate = fromDateChooser.getDate();

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Dhaka"));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
        simpleDateFormat.setCalendar(cal);

        if (toDate != null && fromDate == null) {
            fromDateChooser.setDate(toDate);
            return "STOCK_INVENTORY.DATE BETWEEN '" + simpleDateFormat.format(toDateChooser.getDate()) + " 00:00:00' AND '" + simpleDateFormat.format(fromDateChooser.getDate()) + " 23:59:59'";
        } else if (toDate == null && fromDate != null) {
            toDateChooser.setDate(fromDate);
            return "STOCK_INVENTORY.DATE BETWEEN '" + simpleDateFormat.format(toDateChooser.getDate()) + " 00:00:00' AND '" + simpleDateFormat.format(fromDateChooser.getDate()) + " 23:59:59'";
        } else if (toDate != null && fromDate != null) {
            //check range validity
            if (!toDate.after(fromDate)) {
                //valid
                return "STOCK_INVENTORY.DATE BETWEEN '" + simpleDateFormat.format(toDateChooser.getDate()) + " 00:00:00' AND '" + simpleDateFormat.format(fromDateChooser.getDate()) + " 23:59:59'";
            } else {
                JOptionPane.showMessageDialog(this, "Date range invalid. 'To' date should be before 'From' date.", "Invalid Input", JOptionPane.ERROR_MESSAGE);
                return "";
            }
        }
        return "";
    }

    private String isCostIncluded() {
        if (tfToCost.getText().equals("") && tfFromCost.getText().equals("")) {
            // isSomethingWrong = true;
            return "";
        } else if (tfToCost.getText().equals("") && !tfFromCost.getText().equals("")) {
            //isSomethingWrong = true;
            tfToCost.setText(tfFromCost.getText());
            return "STOCK_INVENTORY.COST BETWEEN " + tfToCost.getText() + " AND " + tfFromCost.getText();
        } else if (!tfToCost.getText().equals("") && tfFromCost.getText().equals("")) {
            tfFromCost.setText(tfToCost.getText());
            return "STOCK_INVENTORY.COST BETWEEN " + tfToCost.getText() + " AND " + tfFromCost.getText();
        } else if (!tfToCost.getText().equals("") && !tfFromCost.getText().equals("")) {

            //        return "ID BETWEEN " + toInvoiceID.getText() + " AND " + fromInvoiceID.getText();
            if (isRangeVaild(tfToCost.getText(), tfFromCost.getText())) {
                return "STOCK_INVENTORY.COST BETWEEN " + tfToCost.getText() + " AND " + tfFromCost.getText();
            } else {
                JOptionPane.showMessageDialog(this, "Invalid Range for Invoice ID. Range not allowed. 'To' field must be less than 'From' field.", "Invalid range", JOptionPane.ERROR_MESSAGE);
                return "";
            }

        } else {
            isSomethingWrong = false;
            return "";
        }
    }

    private String isSoldItemIncluded() {
        if (includeSoldItemCheckbox.isSelected()) {
            return "STOCK_INVENTORY.SOLD = 'true'";
        } else {
            return "STOCK_INVENTORY.SOLD = 'false';";
        }
    }

    private void button1ActionPerformed(ActionEvent ex) {
        Connection connection = null;
        Statement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");

            statement = connection.createStatement();
            String sql = generateSQL();
            ResultSet rs = statement.executeQuery(sql);

            table1.setModel(SellReportInvoiceWise.resultSetToTableModel(rs));

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void btnSearchActionPerformed(ActionEvent event) {

    }

    private void button3ActionPerformed(ActionEvent e) {
        dispose();
    }

    private void printBtnActionPerformed(ActionEvent e) {
        MessageFormat header = new MessageFormat("Stock Item List");
        MessageFormat footer = new MessageFormat("Page - {0}");

        try {
            table1.print(JTable.PrintMode.NORMAL, header, footer);
        } catch (PrinterException e1) {
            e1.printStackTrace();
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        panel1 = new JPanel();
        label1 = new JLabel();
        cbSupplier = new JComboBox();
        label2 = new JLabel();
        cbProduct = new JComboBox();
        label3 = new JLabel();
        label4 = new JLabel();
        tfToSerial = new JFormattedTextField();
        label5 = new JLabel();
        tfFromSerial = new JFormattedTextField();
        label6 = new JLabel();
        label7 = new JLabel();
        tfToInvoice = new JFormattedTextField();
        label8 = new JLabel();
        tfFromInvoice = new JFormattedTextField();
        label11 = new JLabel();
        label12 = new JLabel();
        tfToCost = new JFormattedTextField();
        label13 = new JLabel();
        tfFromCost = new JFormattedTextField();
        label14 = new JLabel();
        label15 = new JLabel();
        label16 = new JLabel();
        toDateChooser = new JDateChooser();
        fromDateChooser = new JDateChooser();
        includeSoldItemCheckbox = new JCheckBox();
        searchBtn = new JButton();
        label17 = new JLabel();
        outputSerialCheckBox = new JCheckBox();
        outputProductCheckBox = new JCheckBox();
        outputInvoiceCheckBox = new JCheckBox();
        outputCostChecBox = new JCheckBox();
        outputDateCheckBox = new JCheckBox();
        outputWarrantyCheckBox = new JCheckBox();
        outputSupplierCheckBox = new JCheckBox();
        panel2 = new JPanel();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();
        printBtn = new JButton();
        button3 = new JButton();

        //======== this ========
        setTitle("Stock Inventory Report");
        setModal(true);
        Container contentPane = getContentPane();

        //======== panel1 ========
        {
            panel1.setBorder(new TitledBorder("Search Criteria"));


            //---- label1 ----
            label1.setText("Supplier");

            //---- label2 ----
            label2.setText("Product");

            //---- label3 ----
            label3.setText("Serial");

            //---- label4 ----
            label4.setText("To");

            //---- label5 ----
            label5.setText("From");

            //---- label6 ----
            label6.setText("Invoice ID");

            //---- label7 ----
            label7.setText("To");

            //---- label8 ----
            label8.setText("From");

            //---- label11 ----
            label11.setText("Cost");

            //---- label12 ----
            label12.setText("To");

            //---- label13 ----
            label13.setText("From");

            //---- label14 ----
            label14.setText("Date");

            //---- label15 ----
            label15.setText("To");

            //---- label16 ----
            label16.setText("From");

            //---- includeSoldItemCheckbox ----
            includeSoldItemCheckbox.setText("Include Sold Items");

            //---- searchBtn ----
            searchBtn.setText("Search");
            searchBtn.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    button1ActionPerformed(e);
                }
            });

            //---- label17 ----
            label17.setText("Output Columns");

            //---- outputSerialCheckBox ----
            outputSerialCheckBox.setText("Serial");

            //---- outputProductCheckBox ----
            outputProductCheckBox.setText("Product");

            //---- outputInvoiceCheckBox ----
            outputInvoiceCheckBox.setText("Invoice ID");

            //---- outputCostChecBox ----
            outputCostChecBox.setText("Cost");

            //---- outputDateCheckBox ----
            outputDateCheckBox.setText("Date");

            //---- outputWarrantyCheckBox ----
            outputWarrantyCheckBox.setText("Warranty");

            //---- outputSupplierCheckBox ----
            outputSupplierCheckBox.setText("Supplier");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
                    panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addGroup(panel1Layout.createParallelGroup()
                                            .addGroup(panel1Layout.createSequentialGroup()
                                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                    .addGroup(panel1Layout.createParallelGroup()
                                                                            .addComponent(label1)
                                                                            .addComponent(label2)
                                                                            .addComponent(label3))
                                                                    .addGap(35, 35, 35)
                                                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                                    .addComponent(label4)
                                                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                                    .addComponent(tfToSerial, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)
                                                                                    .addGap(18, 18, 18)
                                                                                    .addComponent(label5)
                                                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                                                    .addComponent(tfFromSerial))
                                                                            .addComponent(cbSupplier)
                                                                            .addComponent(cbProduct, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE)))
                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                    .addComponent(label6)
                                                                    .addGap(26, 26, 26)
                                                                    .addComponent(label7)
                                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                    .addComponent(tfToInvoice, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)
                                                                    .addGap(18, 18, 18)
                                                                    .addComponent(label8)
                                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                                    .addComponent(tfFromInvoice)))
                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                                    .addComponent(label14)
                                                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                                    .addComponent(label15)
                                                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED))
                                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                                    .addComponent(label11)
                                                                                    .addGap(26, 26, 26)
                                                                                    .addComponent(label12)
                                                                                    .addGap(4, 4, 4)))
                                                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                                            .addComponent(tfToCost, GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
                                                                            .addComponent(toDateChooser, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                                    .addComponent(label13)
                                                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                                    .addComponent(tfFromCost, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE))
                                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                                    .addComponent(label16)
                                                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                                    .addComponent(fromDateChooser, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                    .addComponent(includeSoldItemCheckbox)
                                                                    .addGap(139, 139, 139))))
                                            .addGroup(panel1Layout.createSequentialGroup()
                                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                            .addGroup(panel1Layout.createSequentialGroup()
                                                                    .addComponent(label17)
                                                                    .addGap(18, 18, 18)
                                                                    .addComponent(outputSerialCheckBox)
                                                                    .addGap(18, 18, 18)
                                                                    .addComponent(outputProductCheckBox)
                                                                    .addGap(18, 18, 18)
                                                                    .addComponent(outputInvoiceCheckBox)
                                                                    .addGap(18, 18, 18)
                                                                    .addComponent(outputCostChecBox)
                                                                    .addGap(26, 26, 26)
                                                                    .addComponent(outputDateCheckBox))
                                                            .addComponent(searchBtn, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE))
                                                    .addGap(18, 18, 18)
                                                    .addComponent(outputWarrantyCheckBox)
                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(outputSupplierCheckBox)
                                                    .addGap(0, 0, Short.MAX_VALUE)))
                                    .addContainerGap())
            );
            panel1Layout.linkSize(SwingConstants.HORIZONTAL, new Component[]{cbProduct, cbSupplier});
            panel1Layout.linkSize(SwingConstants.HORIZONTAL, new Component[]{tfFromCost, tfToCost});
            panel1Layout.setVerticalGroup(
                    panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(cbSupplier, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(label1)
                                            .addComponent(label11)
                                            .addComponent(tfToCost, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(label13)
                                            .addComponent(tfFromCost, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(label12))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(cbProduct, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(label2)
                                            .addComponent(label14)
                                            .addComponent(label16)
                                            .addComponent(toDateChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(fromDateChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(label15))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(label4)
                                            .addComponent(tfToSerial, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(label5)
                                            .addComponent(label3)
                                            .addComponent(tfFromSerial, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(includeSoldItemCheckbox))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(label6)
                                            .addComponent(label7)
                                            .addComponent(tfToInvoice, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(label8)
                                            .addComponent(tfFromInvoice, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(label17)
                                            .addComponent(outputSerialCheckBox)
                                            .addComponent(outputProductCheckBox)
                                            .addComponent(outputInvoiceCheckBox)
                                            .addComponent(outputCostChecBox)
                                            .addComponent(outputDateCheckBox)
                                            .addComponent(outputWarrantyCheckBox)
                                            .addComponent(outputSupplierCheckBox))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(searchBtn)
                                    .addContainerGap(11, Short.MAX_VALUE))
            );
        }

        //======== panel2 ========
        {
            panel2.setBorder(new TitledBorder("Search Result"));

            //======== scrollPane1 ========
            {
                scrollPane1.setViewportView(table1);
            }

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
                    panel2Layout.createParallelGroup()
                            .addGroup(panel2Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(scrollPane1)
                                    .addContainerGap())
            );
            panel2Layout.setVerticalGroup(
                    panel2Layout.createParallelGroup()
                            .addGroup(panel2Layout.createSequentialGroup()
                                    .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                                    .addContainerGap())
            );
        }

        //---- printBtn ----
        printBtn.setText("Print");
        printBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printBtnActionPerformed(e);
            }
        });

        //---- button3 ----
        button3.setText("Cancel");
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button3ActionPerformed(e);
            }
        });

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addGap(14, 14, 14)
                                                .addComponent(printBtn, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(button3, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
                                                .addGap(20, 20, 20))
                                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                        .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addContainerGap())))
        );
        contentPaneLayout.setVerticalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addComponent(printBtn)
                                        .addComponent(button3))
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    } // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
