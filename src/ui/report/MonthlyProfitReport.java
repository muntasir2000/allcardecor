/*
 * Created by JFormDesigner on Sun Jun 01 03:26:24 ALMT 2014
 */

package ui.report;

import javax.swing.*;
import java.awt.*;

/**
 * @author Tareq Muntasir
 */
public class MonthlyProfitReport extends JDialog {
    public MonthlyProfitReport(Frame owner) {
        super(owner);
        initComponents();
    }

    public MonthlyProfitReport(Dialog owner) {
        super(owner);
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir

        //======== this ========
        setTitle("Profit Report");
        Container contentPane = getContentPane();

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGap(0, 619, Short.MAX_VALUE)
        );
        contentPaneLayout.setVerticalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGap(0, 364, Short.MAX_VALUE)
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
