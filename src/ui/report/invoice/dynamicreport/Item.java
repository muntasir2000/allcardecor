package ui.report.invoice.dynamicreport;

/**
 * Created by Tareq on 6/6/2014.
 */
public class Item {
    public Integer getInvoiceRowNumber() {
        return invoiceRowNumber;
    }

    public void setInvoiceRowNumber(Integer invoiceRowNumber) {
        this.invoiceRowNumber = invoiceRowNumber;
    }

    private Integer invoiceRowNumber;
    private Integer serial;
    private String productName;
    private String description;
    private Integer cost;
    private Integer warranty;
    private String manID;


    public Integer getSerial() {
        return serial;
    }

    public void setSerial(Integer id) {
        this.serial = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Integer getWarranty() {
        return warranty;
    }

    public void setWarranty(Integer warranty) {
        this.warranty = warranty;
    }

    public String getManID() {
        return manID;
    }

    public void setManID(String manID) {
        this.manID = manID;
    }
}
