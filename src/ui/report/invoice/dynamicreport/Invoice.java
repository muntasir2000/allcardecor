package ui.report.invoice.dynamicreport;

import core.debug.Debug;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Tareq on 6/6/2014.
 */
public class Invoice {
    private Integer id;
    private Customer customer;
    private String date;
    private BigDecimal subTotal;
    private BigDecimal discount;
    private BigDecimal total;
    private BigDecimal deliveryCharge;
    private String isRetailOrWholesale;
    private String shippingAddress;
    private String shipTo;
    private List<Item> items;

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter2 = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
    /*    SimpleDateFormat formatter2 = new SimpleDateFormat("MMM dd, yyyy");
        DateTimeFormatter jodaFormatter = DateTimeFormat.forPattern()*/
        Date a = null;
        try {
            a = formatter.parse(date);
            Debug.print(a.toString(), "Date from getDate");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatter2.format(a);
        //   return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(BigDecimal deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public String getIsRetailOrWholesale() {
        return isRetailOrWholesale;
    }

    public void setIsRetailOrWholesale(String isRetailOrWholesale) {
        this.isRetailOrWholesale = isRetailOrWholesale;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShipTo() {
        return shipTo;
    }

    public void setShipTo(String shipTo) {
        this.shipTo = shipTo;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }
}
