package ui.report.invoice.dynamicreport;

/**
 * Created by Tareq on 6/6/2014.
 */
public class Customer {
    private Integer id;
    private String name;
    private String address;
    private String contact;
    private String discount;
    private String customerId;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        String paddedCustomerId = "";
        int inputCustomerIdLength = countDigits(customerId);
        int zeroesToBeAdded = 8 - inputCustomerIdLength;
        for(int i=1; i<=zeroesToBeAdded; i++){
            paddedCustomerId = paddedCustomerId + "0";
        }
        paddedCustomerId = paddedCustomerId + customerId;

        this.customerId =  paddedCustomerId;
    }

    private int countDigits(int number){
        int counter = 0;
        while(number != 0){
            counter++;
            number = number/10;
        }
        return counter;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
