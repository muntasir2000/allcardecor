package ui.report.invoice.dynamicreport;

import core.debug.Debug;
import net.sf.dynamicreports.examples.Templates;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.base.expression.AbstractSimpleExpression;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.builder.component.HorizontalListBuilder;
import net.sf.dynamicreports.report.builder.component.VerticalListBuilder;
import net.sf.dynamicreports.report.builder.datatype.BigDecimalType;
import net.sf.dynamicreports.report.builder.datatype.IntegerType;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.builder.subtotal.AggregationSubtotalBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.constant.Position;
import net.sf.dynamicreports.report.constant.SplitType;
import net.sf.dynamicreports.report.constant.VerticalAlignment;
import net.sf.dynamicreports.report.definition.ReportParameters;
import net.sf.dynamicreports.report.exception.DRException;

import java.awt.*;
import java.util.Locale;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

/**
 * Created by Tareq on 5/31/2014.
 */
public class InvoiceGenerator {
    StyleBuilder shippingStyle;
    StyleBuilder totalPaymentStyle;
    private String typeStr;
    private InvoiceData data;
    private AggregationSubtotalBuilder<Integer> subTotal;

    public InvoiceGenerator(int InvoiceID) {
        data = new InvoiceData(InvoiceID);
//        this.typeStr = type;
        typeStr = data.getTypeStr();
    }

    private ComponentBuilder<?, ?> createTitleComponent(String label) {

        StyleBuilder invoiceNoLableStyle = stl.style().bold().setFontSize(15).setHorizontalAlignment(HorizontalAlignment.RIGHT);

        ComponentBuilder<?, ?> dynamicReportsComponent =
                cmp.verticalList(
                        cmp.image("res\\logo.png").setFixedDimension(150, 60),
                        cmp.text("All Car Decor").setStyle(stl.style().bold().setFontSize(18)).setHorizontalAlignment(HorizontalAlignment.LEFT),
                        cmp.text("145, South Shahjahanpur "),
                        cmp.text("Dhaka, Bangladesh"),
                        cmp.text("+880 1713115944, +880 1613115944"),
                        cmp.text("http://www.allcardecor.com").setStyle(Templates.italicStyle),
                        cmp.text("http://www.facebook.com/allcardecor").setStyle(Templates.italicStyle),
                        cmp.text("Email: info@allcardecor.com").setStyle(Templates.italicStyle));

        return cmp.horizontalList()
                .add(
                        dynamicReportsComponent,
                        cmp.verticalList().add(
                                cmp.verticalGap(80),
                                cmp.text(label).setStyle(invoiceNoLableStyle),
                                cmp.text("Customer ID : "+data.getInvoice().getCustomer().getCustomerId())
                                        .setStyle(Templates.bold12CenteredStyle).setHorizontalAlignment(HorizontalAlignment.RIGHT),
                                cmp.text(typeStr).setStyle(Templates.bold12CenteredStyle).setHorizontalAlignment(HorizontalAlignment.RIGHT),
                                cmp.text(data.getInvoice().getDate()).setStyle(Templates.bold12CenteredStyle).setHorizontalAlignment(HorizontalAlignment.RIGHT))
                )
                .newRow()
                .add(cmp.line())
                .newRow()
                //.add(cmp.verticalGap(10))
                ;
    }

    public JasperReportBuilder build() throws DRException {
        JasperReportBuilder report = report();

        //init style
        StyleBuilder columnStyle = stl.style(Templates.columnStyle)
                .setBorder(stl.border().setLeftPen(stl.pen().setLineWidth(0.1f)).setRightPen(stl.pen().setLineWidth(0.1f)));
        StyleBuilder subtotalStyle = stl.style()
                .bold();//.setAlignment(HorizontalAlignment.JUSTIFIED, VerticalAlignment.JUSTIFIED);
        shippingStyle = stl.style(Templates.boldStyle);


        totalPaymentStyle = stl.style()
                .setFontSize(13)
                .setBackgroundColor(Color.white)
                .bold()
        //    .setPadding(5)
        ;

        //init columns
        TextColumnBuilder<Integer> rowNumberColumn = col.column(" # ", "invoiceRowNumber", type.integerType())
                .setFixedColumns(2)
                .setHorizontalAlignment(HorizontalAlignment.CENTER);


        TextColumnBuilder<Integer> serialColumn = col.column("Serial No.", "serial", type.integerType())
                .setHorizontalAlignment(HorizontalAlignment.LEFT)
                .setFixedColumns(6)
                .setDataType(new PlainIntegerType());

        TextColumnBuilder<String> productNameColumn = col.column("Name", "productName", type.stringType())
                .setHorizontalAlignment(HorizontalAlignment.LEFT)
                .setFixedColumns(11);

        TextColumnBuilder<String> descriptionColumn = col.column("Description", "description", type.stringType())
                .setHorizontalAlignment(HorizontalAlignment.LEFT);
        //.setFixedColumns(6);

        TextColumnBuilder<Integer> warrantyColumn = col.column("Warranty (days)", "warranty", type.integerType())
                .setHorizontalAlignment(HorizontalAlignment.LEFT)
                .setFixedColumns(6);

        TextColumnBuilder<String> manIDColumn = col.column("Man. ID", "manID", type.stringType())
                .setFixedColumns(5);

        TextColumnBuilder<Integer> priceColumn = col.column("Price", "cost", type.integerType())
                .setFixedColumns(8).setFixedRows(3);

        StyleBuilder subtotalStyleBuilder = stl.style();


        subTotal = sbt.sum(priceColumn)
                .setLabel("Sub Total:")
                .setLabelPosition(Position.TOP)
                .setStyle(stl.style().setVerticalAlignment(VerticalAlignment.TOP))
        ;


        report//.setTemplate(Templates.reportTemplate.setTitleStyle(stl.style().setBorder(stl.penThin())))
                .setColumnTitleStyle(
                        stl.style()
                                //   .setBorder(stl.penThin())
                                .setBackgroundColor(Color.lightGray)
                                .setHorizontalAlignment(HorizontalAlignment.CENTER)
                                .setVerticalAlignment(VerticalAlignment.MIDDLE)
                                .bold()
                )

                .setColumnStyle(columnStyle)
                .setDetailOddRowStyle(stl.simpleStyle().setBackgroundColor(new Color(237, 231, 226)))
                .setHighlightDetailOddRows(true)
                .setSubtotalStyle(subtotalStyle)
                        //columns
                .columns(rowNumberColumn, serialColumn, productNameColumn, descriptionColumn, warrantyColumn, manIDColumn, priceColumn)
                        //     .subtotalsAtSummary(subTotal)
                        //band components
/*                .title(createTitleComponent("Invoice No. : " + data.getInvoice().getId()),
                        cmp.horizontalList().setStyle(stl.style(10)).setGap(50).add(
                                cmp.hListCell(createCustomerComponent("Bill To", data.getInvoice().getCustomer())).heightFixedOnTop(),
                                cmp.hListCell(createShiptoComponent("Ship To"))),
                        cmp.verticalGap(10)
                )*/
                        //    .subtotalsAtGroupFooter(grp.group())
                .pageHeader(createTitleComponent("Invoice No. : " + data.getInvoice().getId()),
                        cmp.horizontalList().setStyle(stl.style(10)).setGap(50).add(
                                cmp.hListCell(createCustomerComponent("Bill To", data.getInvoice().getCustomer())).heightFixedOnTop(),
                                cmp.hListCell(createShiptoComponent("Ship To"))),
                        cmp.verticalGap(1)
                )
                .detail(

                        cmp.pageBreak().setPrintWhenExpression(new DetailPageBreakPrintWhenExpression())
                        //   cmp.line().setPrintWhenExpression(new LastRowLinePrintWhenExpression())
                )
                        //      .columnFooter(cmp.line())
                .setDetailSplitType(SplitType.PREVENT)

                .pageFooter(
                        cmp.horizontalList().add(
                                cmp.text("Page ").setStyle(stl.style().bold().setHorizontalAlignment(HorizontalAlignment.RIGHT)),
                                cmp.pageXofY().setStyle(stl.style().bold().setHorizontalAlignment(HorizontalAlignment.LEFT))
                        )
                )
                .lastPageFooter(
                        createFooterTotalsComponent(),
                        cmp.verticalGap(10),
                        cmp.text("Thank you for your business").setStyle(Templates.bold12CenteredStyle)
                )
                .setDataSource(data.createDataSource());

        report.setPageMargin(margin(40));
        return report;
    }


    private ComponentBuilder<?, ?> createCustomerComponent(String label, Customer customer) {
        HorizontalListBuilder list = cmp.horizontalList().setBaseStyle(stl.style().setTopBorder(stl.pen1Point()).setLeftPadding(10));
        addCustomerAttribute(list, "Name", customer.getName());
        addCustomerAttribute(list, "Address", customer.getAddress());
        addCustomerAttribute(list, "Contact", customer.getContact());
        return cmp.verticalList(
                cmp.text(label).setStyle(Templates.boldStyle.setFontSize(9)).setHorizontalAlignment(HorizontalAlignment.LEFT),
                list);
    }

    private ComponentBuilder<?, ?> createShiptoComponent(String label) {
        HorizontalListBuilder list = cmp.horizontalList().setBaseStyle(stl.style().setTopBorder(stl.pen1Point()).setLeftPadding(10));
        list.add( cmp.text(data.getInvoice().getShipTo())).newRow();
        list.add(cmp.text(data.getInvoice().getShippingAddress())).newRow();
        list.add(cmp.verticalGap(10));
        return cmp.verticalList(cmp.text(label).setStyle(Templates.boldStyle.setFontSize(9)).setHorizontalAlignment(HorizontalAlignment.LEFT),
                list);
    }

    private void addCustomerAttribute(HorizontalListBuilder list, String label, String value) {
        if (value != null) {
            list.add(cmp.text(value).setStyle(stl.style().setFontSize(9))).newRow();
        }
    }

    private ComponentBuilder<?, ?> createFooterTotalsComponent() {
        VerticalListBuilder holderList = cmp.verticalList();
        HorizontalListBuilder parentHorizontalList = cmp.horizontalList();
        HorizontalListBuilder innerList = cmp.horizontalList();
        parentHorizontalList.add(cmp.horizontalGap(330), innerList);

        holderList.add(parentHorizontalList);
        innerList.add(
                //title list
                cmp.verticalList().add(
                        cmp.text("Sub-Total").setStyle(shippingStyle),
                        cmp.text("Discount").setStyle(shippingStyle),
                        cmp.text("Delivery Charge").setStyle(shippingStyle),
                        cmp.text("Total Payment").setStyle(totalPaymentStyle)
                ).setStyle(stl.style().setHorizontalAlignment(HorizontalAlignment.RIGHT)),
                //value list
                cmp.verticalList().add(
                        cmp.text("  " + new CurrencyType().valueToString(data.getInvoice().getSubTotal(), Locale.CHINA)).setStyle(shippingStyle).setHorizontalAlignment(HorizontalAlignment.RIGHT),
                        cmp.text("  " + new CurrencyType().valueToString(data.getInvoice().getDiscount(), Locale.CHINA)).setStyle(shippingStyle).setHorizontalAlignment(HorizontalAlignment.RIGHT),
                        cmp.text("  " + new CurrencyType().valueToString(data.getInvoice().getDeliveryCharge(), Locale.CHINA)).setStyle(shippingStyle).setHorizontalAlignment(HorizontalAlignment.RIGHT),
                        cmp.verticalGap(10),
                        cmp.text("  " + new CurrencyType().valueToString(data.getInvoice().getTotal(), Locale.CANADA)).setStyle(totalPaymentStyle).setHorizontalAlignment(HorizontalAlignment.RIGHT)
                )
        );
        return holderList;
    }


    private class CurrencyType extends BigDecimalType {

        private static final long serialVersionUID = 1L;

        @Override

        public String getPattern() {
            return "\u09F3  #,###";
        }
    }

    private class PlainIntegerType extends IntegerType {
        public String getPattern() {
            return "#";
        }
    }

    /*
        private class DetailPageBreakPrintWhenExpression extends AbstractSimpleExpression<Boolean>{
            @Override
            public Boolean evaluate(ReportParameters reportParameters) {
                Debug.print(reportParameters.getPageRowNumber() + "", "Page row number inside PrintWhenExpression");
                if(reportParameters.getPageNumber()==1){
                    Debug.print((reportParameters.getPageRowNumber() % 8)+"", "Check with MOD 8" );
                    if(reportParameters.getPageRowNumber() % 8 == 0){
                        return true;
                    }else
                        return false;
                }
                if((reportParameters.getPageRowNumber())%7==0)
                    return true;
                else {
                    return false;
                }
            }
        }
    */
    private class DetailPageBreakPrintWhenExpression extends AbstractSimpleExpression<Boolean> {
        @Override
        public Boolean evaluate(ReportParameters reportParameters) {
            Debug.print(reportParameters.getPageRowNumber() + "", "Page row number inside PrintWhenExpression");
            if (reportParameters.getPageNumber() == 1) {
                Debug.print((reportParameters.getPageRowNumber() % 9) + "", "Check with MOD 8");
                if (reportParameters.getPageRowNumber() % 9 == 0) {
                    return true;
                } else
                    return false;
            }
            if ((reportParameters.getPageRowNumber()) % 8 == 0)
                return true;
            else {
                return false;
            }
        }
    }

    private class LastRowLinePrintWhenExpression extends AbstractSimpleExpression<Boolean> {
        @Override
        public Boolean evaluate(ReportParameters reportParameters) {
            if (reportParameters.getPageRowNumber() == 9)
                return true;
            else {
                return false;
            }
        }
    }

}
