package ui.report.invoice.dynamicreport;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Tareq on 6/5/2014.
 */

public class WaitDialog extends JDialog {
    public WaitDialog(JDialog parent, String title, String message) {
        super(parent, title, true);
        if (parent != null) {
            Dimension parentSize = parent.getSize();
            Point p = parent.getLocation();
            setLocation(p.x + parentSize.width / 4, p.y + parentSize.height / 4);
        }
        JPanel messagePane = new JPanel();
        messagePane.add(new JLabel(message));
        getContentPane().add(messagePane);
        /*JPanel buttonPane = new JPanel();
        JButton button = new JButton("OK");
        buttonPane.add(button);
        getContentPane().add(buttonPane, BorderLayout.SOUTH);*/
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();

    }
}