package ui.report.invoice.dynamicreport;

import core.debug.Debug;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Tareq on 6/6/2014.
 */
public class InvoiceData {
    private Invoice invoice;
    private String typeStr;


    public InvoiceData(int invoiceID) {
        invoice = createInvoice(invoiceID);
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    private Invoice createInvoice(int id) {
        Invoice invoice = new Invoice();

        String customerName = null;
        Connection connection = null;
        Statement statement = null;
        ResultSet rs;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            statement = connection.createStatement();
            String sql = "SELECT * FROM SELL_INVOICE WHERE ID = " + id + ";";
            rs = statement.executeQuery(sql);

            if (rs.next()) {
                invoice.setId(id);
                customerName = rs.getString("CUSTOMER");
                String strDate = rs.getString("DATE");
                Debug.print(strDate, "Date from sql Database");
                invoice.setDate(rs.getString("DATE"));
                invoice.setTotal(new BigDecimal(rs.getInt("TOTAL")));
                invoice.setDiscount(new BigDecimal(rs.getInt("DISCOUNT")));
                invoice.setShippingAddress(rs.getString("SHIPPING_ADDRESS"));
                invoice.setShipTo(rs.getString("SHIP_TO"));
                invoice.setDeliveryCharge(new BigDecimal(rs.getInt("DELIVERY_CHARGE")));
                if (rs.getString("ISRETAIL").equals("true")) {
                    typeStr = "Retail Sale";
                } else {
                    typeStr = "Whole Sale";
                }
            }
            rs.close();

            sql = "SELECT * FROM CUSTOMER WHERE NAME = '" + customerName + "';";
            rs = statement.executeQuery(sql);
            if (rs.next()) {
                Customer customer = new Customer();
                customer.setId(rs.getInt("ID"));
                customer.setName(customerName);
                customer.setAddress(rs.getString("ADDRESS"));
                customer.setContact(rs.getString("CONTACT"));
                customer.setDiscount(rs.getInt("DISCOUNT") + "%");
                customer.setCustomerId(rs.getInt("ID"));
                invoice.setCustomer(customer);
            }
            rs.close();
            sql = "SELECT * FROM SELL_RECORDS, PRODUCTS WHERE SELL_RECORDS.PRODUCT = PRODUCTS.NAME AND SELL_RECORDS.INVOICE_ID = " + id + ";";
            rs = statement.executeQuery(sql);
            ArrayList<Item> list = new ArrayList<Item>();
            int subTotal = 0;
            int rowFromDBCounter = 0;
            while (rs.next()) {
                Item item = new Item();
                item.setSerial(rs.getInt("SERIAL"));
                item.setDescription(rs.getString("DESCRIPTION"));
                item.setInvoiceRowNumber(rowFromDBCounter+1);

                //if this invoice is being generated from Invoice Wise Sale Report,
                //Then check for if any product is returned or not from this invoice.
                //if returned, mark product name cell as returned
                if(rs.getString("ISRETURNED").equals("true")){
                    item.setProductName(rs.getString("PRODUCT") + " (RETURNED)");
                }else{
                    item.setProductName(rs.getString("PRODUCT"));
                }

                item.setCost(new Integer(rs.getInt("COST")));
                subTotal += item.getCost();
                item.setManID(rs.getString("MAN_ID"));
                item.setWarranty(rs.getInt("WARRANTY"));
                list.add(item);
                rowFromDBCounter++;
            }

/*

            //TEST CODE TO DETERMINE DEFAULT ROW COUNT
            //TODO SHOULD BE DELETED BEFORE RELEASE

            for(int i=0; i<6; i++){
                Item item = new Item();
                item.setSerial(i + 1555);
                item.setDescription("Dummy description Dummy Description Dummy Description Dummy Dummy Description");
                item.setProductName("Dummy ProductName");
                item.setCost(5000);
                list.add(item);
                rowFromDBCounter++;
            }
     //       -----------------------------
*/
            Debug.print(rowFromDBCounter + "", "rowFromDBCounter");
            int emptyRowsShouldBeAdded = 0;
            if (rowFromDBCounter <= 8) {
                emptyRowsShouldBeAdded = 8 - rowFromDBCounter;
            } else {
                emptyRowsShouldBeAdded = 8 - (rowFromDBCounter % 8);
            }

            for (int i = 0; i < emptyRowsShouldBeAdded; i++) {
                list.add(new Item());
            }
            Debug.print(emptyRowsShouldBeAdded + "", "emptyRowsShouldBeAdded");

            invoice.setSubTotal(new BigDecimal(subTotal));
            invoice.setItems(list);


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Database closing error");
            }

            // JOptionPane.showMessageDialog(, "Something terribly wrong. Contact developer", "Critical Error", JOptionPane.ERROR_MESSAGE);
        }

        return invoice;
    }


    public JRDataSource createDataSource() {
        return new JRBeanCollectionDataSource(invoice.getItems());
    }


    public String getTypeStr() {
        return typeStr;
    }
}
