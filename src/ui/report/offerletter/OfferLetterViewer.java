package ui.report.offerletter;

import net.sf.dynamicreports.examples.Templates;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.builder.component.VerticalListBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

/**
 * Created by Tareq on 6/4/2014.
 */
public class OfferLetterViewer {

    public JasperReportBuilder build() {

        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String sql = "SELECT STOCK_INVENTORY.PRODUCT, PRODUCTS.DESCRIPTION, PRODUCTS.RETAIL, " +
                "PRODUCTS.WHOLESALE, STOCK_INVENTORY.WARRANTY " +
                "FROM STOCK_INVENTORY, PRODUCTS " +
                "WHERE PRODUCTS.NAME = STOCK_INVENTORY.PRODUCT " +
                " AND SOLD = 'false' " +
                "GROUP BY STOCK_INVENTORY.PRODUCT;";


        StyleBuilder columnStyle = stl.style(Templates.columnStyle)
                .setBorder(stl.pen1Point());

        TextColumnBuilder<Integer> rowId = col.reportRowNumberColumn("#").setFixedColumns(2);
        TextColumnBuilder<String> productNameCol = col.column("Product Name", "PRODUCT", type.stringType()).setFixedColumns(15);
        TextColumnBuilder<String> descriptionCol = col.column("Description", "DESCRIPTION", type.stringType());
        //     TextColumnBuilder<Integer> retailCol = col.column("Retail Price", "RETAIL", type.integerType()).setFixedColumns(6);
        TextColumnBuilder<Integer> wholesaleCol = col.column("Wholesale Price", "WHOLESALE", type.integerType()).setFixedColumns(6);
        TextColumnBuilder<Integer> warrantyCol = col.column("Warranty(days)", "WARRANTY", type.integerType()).setFixedColumns(6);

        return report().setTemplate(Templates.reportTemplate)
                .title(createTitleComponent(),
                        cmp.text("Available products for sale").setStyle(Templates.bold18CenteredStyle),
                        cmp.line(),
                        cmp.verticalGap(10)
                )
                .columns(rowId, productNameCol, descriptionCol, wholesaleCol, warrantyCol)
                .setColumnStyle(columnStyle)
                .setDataSource(sql, connection).setPageMargin(margin(40));

    }

    private ComponentBuilder<?, ?> createTitleComponent() {
        VerticalListBuilder list = cmp.verticalList();
        list.add(
                cmp.image("res\\logo.png").setFixedDimension(150, 60),
                cmp.text("All Car Decor").setStyle(stl.style().bold().setFontSize(18)).setHorizontalAlignment(HorizontalAlignment.LEFT),
                cmp.text("145, South Shahjahanpur "),
                cmp.text("Dhaka, Bangladesh"),
                cmp.text("+880 1713115944, +880 1613115944"),
                cmp.text(new SimpleDateFormat("MMM dd, yyyy").format(new java.util.Date(System.currentTimeMillis()))).setHorizontalAlignment(HorizontalAlignment.RIGHT).setStyle(Templates.boldStyle.setFontSize(14)),
                cmp.line(),
                cmp.verticalGap(10)

        );
        return list;
    }
}
