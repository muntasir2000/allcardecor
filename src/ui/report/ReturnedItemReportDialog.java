/*
 * Created by JFormDesigner on Wed Apr 08 18:51:50 BDT 2015
 */

package ui.report;

import net.proteanit.sql.DbUtils;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author unknown
 */
public class ReturnedItemReportDialog extends JDialog {
    public ReturnedItemReportDialog(Frame owner) {
        super(owner);
        initComponents();
        loadData();

        //make comboboxes editable
        AutoCompleteDecorator.decorate(cbProduct);
        this.setTitle("Returned Items Report");
    }


    private void loadData(){
        //first add an empty item to combobox. because users should be able
        //to search without selecting any  product
        cbProduct.addItem("");

        //load customer names from db
        Connection connection = null;
        Statement statement = null;

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            statement = connection.createStatement();
            String productNameSQL = "SELECT NAME FROM PRODUCTS;";

            ResultSet rs = statement.executeQuery(productNameSQL);

            while (rs.next()){
                String productName = rs.getString("NAME");
                cbProduct.addItem(productName);
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Database closing error");
            }
        }
    }

    private String generateSQLFromSearchCriteria(){
        StringBuilder baseSQL = new StringBuilder("SELECT PRODUCT, SERIAL, CAUSE, DATE" +
                " FROM RETURNED_ITEMS WHERE ");

        //check whether any product is selected or not
        String productName = cbProduct.getSelectedItem().toString();
        if(!productName.equals("")){
            baseSQL.append(" PRODUCT = '" + productName + "' AND ");
        }

        int serialTo = 0, serialFrom = 0;
        try{
            serialTo = Integer.parseInt(tfSerialTo.getText());
            serialFrom = Integer.parseInt(tfSerialFrom.getText());
        }catch (Exception e){
            e.printStackTrace();
        }

        if(serialTo != 0 && serialFrom != 0){
            baseSQL.append(" SERIAL BETWEEN " + serialFrom + " AND " + serialTo +" AND ");
        }

        Date from = datePickerDateFrom.getDate();
        Date to = datePickerDateTo.getDate();
        if(from != null && to != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateFromString = dateFormat.format(from);
            String dateToString = dateFormat.format(to);

            baseSQL.append(" DATE BETWEEN '" + dateFromString + "' AND '" + dateToString + "' AND ");
        }

        if(baseSQL.lastIndexOf("AND")!=-1){
            baseSQL.delete(baseSQL.lastIndexOf("AND"), baseSQL.lastIndexOf("AND")+3);
        }else{
            //no criterion is selected. so WHERE clause is invalid
            baseSQL.delete(baseSQL.indexOf("WHERE"), baseSQL.indexOf("WHERE")+5);
        }

        return baseSQL.toString();
    }

    private void btnSearchActionPerformed(ActionEvent ex) {
        Connection connection = null;
        Statement statement = null;

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            statement = connection.createStatement();

            ResultSet rs = statement.executeQuery(generateSQLFromSearchCriteria());
            table1.setModel(DbUtils.resultSetToTableModel(rs));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void btnOKActionPerformed(ActionEvent e) {
        this.dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        panel1 = new JPanel();
        label2 = new JLabel();
        cbProduct = new JComboBox();
        label7 = new JLabel();
        tfSerialFrom = new JTextField();
        label8 = new JLabel();
        tfSerialTo = new JTextField();
        label10 = new JLabel();
        datePickerDateFrom = new JXDatePicker();
        label11 = new JLabel();
        label12 = new JLabel();
        datePickerDateTo = new JXDatePicker();
        btnSearch = new JButton();
        label13 = new JLabel();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();
        btnOK = new JButton();

        //======== this ========
        Container contentPane = getContentPane();

        //======== panel1 ========
        {
            panel1.setBorder(new TitledBorder("Search Criteria"));



            //---- label2 ----
            label2.setText("Product");

            //---- label7 ----
            label7.setText("From");

            //---- label8 ----
            label8.setText("To");

            //---- label10 ----
            label10.setText("Date");

            //---- label11 ----
            label11.setText("From");

            //---- label12 ----
            label12.setText("To");

            //---- btnSearch ----
            btnSearch.setText("Search");
            btnSearch.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    btnSearchActionPerformed(e);
                }
            });

            //---- label13 ----
            label13.setText("Serial");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
                panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGroup(panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                .addGroup(panel1Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(label10)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(label12)
                                    .addGap(18, 18, 18)
                                    .addComponent(datePickerDateTo, GroupLayout.PREFERRED_SIZE, 244, GroupLayout.PREFERRED_SIZE))
                                .addGroup(panel1Layout.createSequentialGroup()
                                    .addGap(4, 4, 4)
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                            .addGroup(panel1Layout.createSequentialGroup()
                                                .addGap(35, 35, 35)
                                                .addComponent(datePickerDateFrom, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
                                            .addComponent(label11, GroupLayout.Alignment.LEADING))
                                        .addGroup(panel1Layout.createSequentialGroup()
                                            .addComponent(label13)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(label7)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(tfSerialFrom, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
                                            .addGap(12, 12, 12)
                                            .addComponent(label8)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(tfSerialTo, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)))
                                    .addGap(18, 18, 18)
                                    .addComponent(label2)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(cbProduct, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(254, 254, 254)
                                .addComponent(btnSearch, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(21, Short.MAX_VALUE))
            );
            panel1Layout.setVerticalGroup(
                panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(label8)
                            .addComponent(tfSerialTo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfSerialFrom, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addComponent(label7)
                            .addComponent(label13)
                            .addComponent(label2)
                            .addComponent(cbProduct, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(label10)
                            .addComponent(label11)
                            .addComponent(datePickerDateFrom, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addComponent(datePickerDateTo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addComponent(label12))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnSearch)
                        .addGap(0, 10, Short.MAX_VALUE))
            );
        }

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(table1);
        }

        //---- btnOK ----
        btnOK.setText("OK");
        btnOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnOKActionPerformed(e);
            }
        });

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                            .addGap(0, 0, Short.MAX_VALUE)
                            .addComponent(btnOK, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE))
                        .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 641, Short.MAX_VALUE))
                    .addContainerGap())
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 216, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                    .addComponent(btnOK)
                    .addContainerGap())
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel panel1;
    private JLabel label2;
    private JComboBox cbProduct;
    private JLabel label7;
    private JTextField tfSerialFrom;
    private JLabel label8;
    private JTextField tfSerialTo;
    private JLabel label10;
    private JXDatePicker datePickerDateFrom;
    private JLabel label11;
    private JLabel label12;
    private JXDatePicker datePickerDateTo;
    private JButton btnSearch;
    private JLabel label13;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JButton btnOK;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
