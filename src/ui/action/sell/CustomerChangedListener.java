package ui.action.sell;

import core.debug.Debug;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class CustomerChangedListener implements ItemListener {
    JTextField discount, customerId;

    HashMap<String, Integer> discountMap;
    HashMap<Integer, String> customerIdNameMap;

    public CustomerChangedListener(JTextField discount, JTextField customerId,
                                   HashMap<String, Integer> discountMap,
                                   HashMap<Integer, String> customerIdNameMap) {
        this.discount = discount;
        this.discountMap = discountMap;
        this.customerId = customerId;
        this.customerIdNameMap = customerIdNameMap;


    }



    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            Debug.print(e.getItem().toString(), "XXXXXXXX");
            if (e.getItem() == null) {
                Debug.print("getintem null", "XXXXXX");
            }
            if (discountMap == null) {
                Debug.print("discountMap null", "XXXXXX");
            }
            if (discountMap.get(e.getItem()) == null) {
                Debug.print("get discountMap item null", "XXXXXXX");
            }
            String name = e.getItem().toString();
            Integer integer = discountMap.get(name);
            customerId.setText(getCustomerID(name) + "");
            discount.setText(integer.toString().concat("%"));
        }
    }

    private int getCustomerID(String name){
        Set<Map.Entry<Integer, String>> set = customerIdNameMap.entrySet();

        for(Map.Entry<Integer, String> singlePair: set){
            if(singlePair.getValue().toString().equals(name))
                return singlePair.getKey();
        }
        return 0;
    }

}
