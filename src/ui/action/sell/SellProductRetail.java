/*
 * Created by JFormDesigner on Sun Apr 13 00:12:21 ALMT 2014
 */
package ui.action.sell;

import com.toedter.calendar.JDateChooser;
import core.debug.Debug;
import net.sf.dynamicreports.report.exception.DRException;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import ui.init.NewCustomerDialog;
import ui.report.invoice.dynamicreport.InvoiceGenerator;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;

/**
 * @author Tareq  Al Muntasir
 */
public class SellProductRetail extends JDialog {
    SellTableModel tableModel;
    long invoiceID;
    HashMap<String, Integer> discountMap;
    HashMap<Integer, String> customerIdNameMap;

    public SellProductRetail(Frame owner) {
        super(owner);
        initComponents();
        loadData();
        resizeColumns();
        addListeners();
        setupMisc();
    }

    private void setupMisc() {
        table1.setRowHeight(25);
        DiscountInputVerifier inputVerifier = new DiscountInputVerifier();
        discount.setInputVerifier(inputVerifier);
        discount.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                btnRecord.setEnabled(false);
                btnRecordPrint.setEnabled(false);
            }

            @Override
            public void focusLost(FocusEvent e) {
                btnRecordPrint.setEnabled(true);
                btnRecord.setEnabled(true);
            }
        });

        if (cbCustomer.getSelectedItem() != null) {
            String initialCustomer = cbCustomer.getSelectedItem().toString();

            discount.setText(discountMap.get(initialCustomer).toString() + "%");
        }
        //init the dateChooser with current date/time
        //java.util.Date  currentDate = new Date(Calendar.getInstance().getTime());
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Dhaka"));
        dateChooser1.setDate(cal.getTime());
        AutoCompleteDecorator.decorate(cbCustomer);

    }

    private void resizeColumns() {
        TableColumnModel tableColumnModel = table1.getColumnModel();
        tableColumnModel.getColumn(0).setPreferredWidth(100);
        tableColumnModel.getColumn(1).setPreferredWidth(140);
        tableColumnModel.getColumn(3).setPreferredWidth(10);
        tableColumnModel.getColumn(4).setPreferredWidth(20);
    }

    private void loadData() {
        discountMap = new HashMap<String, Integer>();
        customerIdNameMap = new HashMap<Integer, String>();

        Connection connection = null;
        Statement statement = null;
        ResultSet rs;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");

            String sql = "SELECT * FROM CUSTOMER;";
            statement = connection.createStatement();
            rs = statement.executeQuery(sql);

            while (rs.next()) {
                String id = rs.getInt("ID") + "";
                Debug.print(id, "id from sellproduct customer");
                String name = rs.getString("NAME");
                /*cbCustomer.addItem(id + " : " + name);
                discountMap.put(id + " : " + name, rs.getInt("DISCOUNT"));*/
                cbCustomer.addItem(name);
                customerIdNameMap.put(rs.getInt("ID"), name);
                discountMap.put(name, rs.getInt("DISCOUNT"));
            }
            rs.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();

                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Database closing error");
            }
        }
    }

    private boolean validateRows() {
        //iterate through the last row and validate input
        for (int i = tableModel.getRowCount(); i >= 1; i--) {
            for (int j = tableModel.getColumnCount(); j >= 1; j--) {
                if (tableModel.getValueAt(i - 1, j - 1) == null) {
                    JOptionPane.showMessageDialog(table1, "Field empty. Insert proper value.");
                    return false;
                }
            }
        }
        return true;
    }

    private void saveToDB() {
        Connection connection = null;
        PreparedStatement statement = null;
        String strDate;

        /*if (date.equals("")) {
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Dhaka"));
            SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
            strDate = format.format(cal.getTime());
            Debug.print(strDate, "DATEtiME");
        }*/

        int discountedAmount = 0;
        try {
            int totalPrice = Integer.parseInt(total.getText());

            int subTotalPrice = Integer.parseInt(subtotal.getText());
            discountedAmount = totalPrice - subTotalPrice;
            Debug.print("SubTotalPrice = " + subTotalPrice + " totalPrice = " + totalPrice + " Discount = " + discountedAmount, "SellProductRetial");
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        int deliveryInt = 0;
        try {
            deliveryInt = Integer.parseInt(delivery.getText());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Dhaka"));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        java.util.Date chosenDate = dateChooser1.getDate();
        //    chosenDate.setTime(cal.getTimeInMillis());
        strDate = simpleDateFormat.format(chosenDate);

        Debug.print("Discount before abs() = " + discountedAmount, "SellProductRetail");

        discountedAmount -= deliveryInt;
        discountedAmount = Math.abs(discountedAmount);

        Debug.print("Discount after abs() = " + discountedAmount, "SellProductRetail");

        try {
            Class.forName("org.sqlite.JDBC");

            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            String sql = "INSERT INTO SELL_INVOICE  (CUSTOMER, DATE, TOTAL, DISCOUNT, COUNT, ISRETAIL, SHIPPING_ADDRESS, SHIP_TO, DELIVERY_CHARGE) VALUES(?, ?, ?,?, ?, ?, ?, ?, ?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, cbCustomer.getSelectedItem().toString());
            statement.setString(2, strDate);
            statement.setInt(3, Integer.parseInt(total.getText().toString()));
            statement.setInt(4, discountedAmount);
            statement.setInt(5, tableModel.getRowCount());
            statement.setString(6, "true");
            statement.setString(7, shippingAddress.getText());
            statement.setString(8, shippingName.getText());
            statement.setInt(9, deliveryInt);
            statement.executeUpdate();
            connection.commit();
            String sql2 = "SELECT last_insert_rowid();";


            boolean rowIDfound = false;
            Statement legacyStatement = connection.createStatement();

            ResultSet RSrowID = legacyStatement.executeQuery(sql2);
            if (RSrowID.next()) {

                rowIDfound = true;
                invoiceID = RSrowID.getLong(1);
            }

            if (!rowIDfound) {
                JOptionPane.showMessageDialog(this, "Operation unsuccessful. Something is terribly wrong!", "Error", JOptionPane.ERROR_MESSAGE);
            }

            RSrowID.close();


            ArrayList<ItemRowSellTable> list = fetchDataFromTable();

            for (ItemRowSellTable item : list) {
                legacyStatement.executeUpdate(item.generateInsertSQLforSellEntry());
            }

            for (ItemRowSellTable item : list) {
                legacyStatement.executeUpdate(item.generateUpdateSQLforStockInventory());
            }
            connection.commit();
            statement.close();
            legacyStatement.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }

                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Database closing error");
            }
        }

    }

    private ArrayList<ItemRowSellTable> fetchDataFromTable() {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Dhaka"));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        java.util.Date chosenDate = dateChooser1.getDate();
        //  chosenDate.setTime(cal.getTimeInMillis());
        String strSelectedDate = simpleDateFormat.format(chosenDate);


        int row = tableModel.getRowCount();
        ArrayList<ItemRowSellTable> list = new ArrayList<ItemRowSellTable>();
        for (int i = 0; i < row; i++) {
            String product = tableModel.getValueAt(i, 0).toString();
            int serial = Integer.parseInt(tableModel.getValueAt(i, 2).toString());
            int warranty = Integer.parseInt(tableModel.getValueAt(i, 4).toString());
            int price = Integer.parseInt(tableModel.getValueAt(i, 5).toString());
            //     int warranty = Integer.parseInt(tableModel.getValueAt(i, 3).toString());
            String manID = tableModel.getValueAt(i, 3).toString();


            ItemRowSellTable item = new ItemRowSellTable(invoiceID, serial, cbCustomer.getSelectedItem().toString(), product, price, warranty, strSelectedDate, manID);
            list.add(item);
        }
        return list;
    }

    private void addListeners() {
        cbCustomer.addItemListener(new CustomerChangedListener(discount, tfCustomerId, discountMap, customerIdNameMap));
        discount.getDocument().addDocumentListener(new DiscountChangedListener(subtotal, total, discount, delivery));
        tableModel.addTableModelListener(new PriceChangedListenerMulti(subtotal, discount, total, delivery));
        delivery.getDocument().addDocumentListener(new DiscountChangedListener(subtotal, total, discount, delivery));

    }

    private void addActionPerformed(ActionEvent e) {
        if (!validateRows()) {
            return;
        }
        tableModel.addRow(new Object[]{null, null, null, new Integer(0), new Integer(0)});
        tableModel.fireTableDataChanged();
    }

    private void removeActionPerformed(ActionEvent e) {

        int[] rows = table1.getSelectedRows();
        for (int i = 0; i < rows.length; i++) {
            tableModel.removeRow(rows[i] - i);
        }
    }

    private void btnNewCustomerActionPerformed(ActionEvent e) {
        NewCustomerDialog newCustomerDialog = new NewCustomerDialog(this, cbCustomer, discountMap);
        newCustomerDialog.setVisible(true);
    }

    private void btnRecordActionPerformed(ActionEvent e) {
        if (!validateRows()) {
            return;
        }
        ;
        if (tableModel.getValueAt(0, 0) == null) {
            JOptionPane.showMessageDialog(this, "No items added. Nothing to record", "Nothing to record", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        saveToDB();
        dispose();
    }

    private void cancelButtonActionPerformed(ActionEvent e) {
        this.dispose();
    }

    private void btnRecordPrintActionPerformed(ActionEvent e) {
        if (!validateRows()) {
            return;
        }
        ;
        if (tableModel.getValueAt(0, 0) == null) {
            JOptionPane.showMessageDialog(this, "No items added. Nothing to record", "Nothing to record", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        saveToDB();
        InvoiceGenerator invoiceGenerator = new InvoiceGenerator(new Integer(invoiceID + ""));
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        try {
            invoiceGenerator.build().show(false);
        } catch (DRException e1) {
            e1.printStackTrace();
        }

        this.setCursor(Cursor.getDefaultCursor());
        dispose();
    }

    private void btnSearchCustomerIDActionPerformed(ActionEvent e) {
        int id = 0;
        try{
            id = Integer.parseInt(tfCustomerId.getText());
        }catch (NumberFormatException ex){
            JOptionPane.showMessageDialog(this, "Please enter a valid Customer ID. Only number is allowed","Invalid Input", JOptionPane.INFORMATION_MESSAGE);
        }

        if(id!=0){
            String customerName = customerIdNameMap.get(id);
            cbCustomer.setSelectedItem(customerName);
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        dialogPane = new JPanel();
        panell = new JPanel();
        panel1 = new JPanel();
        label1 = new JLabel();
        cbCustomer = new JComboBox();
        btnNewCustomer = new JButton();
        label2 = new JLabel();
        dateChooser1 = new JDateChooser();
        label9 = new JLabel();
        tfCustomerId = new JTextField();
        btnSearchCustomerID = new JButton();
        panel2 = new JPanel();
        scrollPane1 = new JScrollPane();
        Object[] columnNames = {"Item Name", "Description", "Serial No.","Man.ID", "Warranty (days)","Price"};
        tableModel = new SellTableModel(columnNames, 1);
        table1 = new JTable(tableModel);
        TableColumn col = table1.getColumnModel().getColumn(2);
        col.setCellEditor(new SellTableCellEditor(true));
        //table1.getColumnModel().getColumn(1).setPreferredWidth(130);
        add = new JButton();
        remove = new JButton();
        label3 = new JLabel();
        subtotal = new JTextField();
        label4 = new JLabel();
        discount = new JTextField();
        separator1 = new JSeparator();
        label5 = new JLabel();
        total = new JFormattedTextField();
        label8 = new JLabel();
        delivery = new JFormattedTextField();
        panel4 = new JPanel();
        label6 = new JLabel();
        shippingName = new JTextField();
        label7 = new JLabel();
        scrollPane2 = new JScrollPane();
        shippingAddress = new JTextArea();
        buttonBar = new JPanel();
        cancelButton = new JButton();
        btnRecord = new JButton();
        btnRecordPrint = new JButton();

        //======== this ========
        setTitle("Sell Product (Retail)");
        setModal(true);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));

            dialogPane.setLayout(new BorderLayout());

            //======== panell ========
            {

                //======== panel1 ========
                {
                    panel1.setBorder(new TitledBorder("Customer Info"));

                    //---- label1 ----
                    label1.setText("Customer Name");

                    //---- btnNewCustomer ----
                    btnNewCustomer.setText("New");
                    btnNewCustomer.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnNewCustomerActionPerformed(e);
                        }
                    });

                    //---- label2 ----
                    label2.setText("Date");

                    //---- label9 ----
                    label9.setText("Customer ID");

                    //---- btnSearchCustomerID ----
                    btnSearchCustomerID.setText("Search");
                    btnSearchCustomerID.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnSearchCustomerIDActionPerformed(e);
                        }
                    });

                    GroupLayout panel1Layout = new GroupLayout(panel1);
                    panel1.setLayout(panel1Layout);
                    panel1Layout.setHorizontalGroup(
                        panel1Layout.createParallelGroup()
                            .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addGroup(panel1Layout.createParallelGroup()
                                            .addComponent(label1)
                                            .addComponent(label9)
                                            .addComponent(label2))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                            .addComponent(dateChooser1, GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                                            .addComponent(cbCustomer, GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                                            .addGroup(panel1Layout.createSequentialGroup()
                                                    .addComponent(tfCustomerId, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                                                    .addGap(19, 19, 19)
                                                    .addComponent(btnSearchCustomerID, GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE)))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnNewCustomer, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
                                    .addContainerGap())
                    );
                    panel1Layout.setVerticalGroup(
                        panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(cbCustomer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(btnNewCustomer)
                                            .addComponent(label1))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(label9)
                                            .addComponent(tfCustomerId, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(btnSearchCustomerID))
                                    .addGap(15, 15, 15)
                                    .addGroup(panel1Layout.createParallelGroup()
                                            .addComponent(dateChooser1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(label2))
                                    .addContainerGap(14, Short.MAX_VALUE))
                    );
                }

                //======== panel2 ========
                {
                    panel2.setBorder(new TitledBorder("Items Info"));

                    //======== scrollPane1 ========
                    {
                        scrollPane1.setViewportView(table1);
                    }

                    //---- add ----
                    add.setText("Add Item");
                    add.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            addActionPerformed(e);
                        }
                    });

                    //---- remove ----
                    remove.setText("Remove Item");
                    remove.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            removeActionPerformed(e);
                        }
                    });

                    //---- label3 ----
                    label3.setText("Sub Total");

                    //---- subtotal ----
                    subtotal.setText("0");
                    subtotal.setEditable(false);

                    //---- label4 ----
                    label4.setText("Discount");

                    //---- discount ----
                    discount.setText("0");

                    //---- label5 ----
                    label5.setText("Total");

                    //---- total ----
                    total.setText("0");
                    total.setEditable(false);

                    //---- label8 ----
                    label8.setText("Delivery Charge");

                    GroupLayout panel2Layout = new GroupLayout(panel2);
                    panel2.setLayout(panel2Layout);
                    panel2Layout.setHorizontalGroup(
                        panel2Layout.createParallelGroup()
                            .addGroup(panel2Layout.createSequentialGroup()
                                    .addGroup(panel2Layout.createParallelGroup()
                                            .addComponent(scrollPane1, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 877, Short.MAX_VALUE)
                                            .addGroup(panel2Layout.createSequentialGroup()
                                                    .addContainerGap()
                                                    .addGroup(panel2Layout.createParallelGroup()
                                                            .addGroup(GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                                                                    .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                                            .addGroup(panel2Layout.createSequentialGroup()
                                                                                    .addComponent(add, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                                                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                                                    .addComponent(remove)
                                                                                    .addGap(267, 267, 267)
                                                                                    .addGroup(panel2Layout.createParallelGroup()
                                                                                            .addComponent(label3, GroupLayout.Alignment.TRAILING)
                                                                                            .addComponent(label4, GroupLayout.Alignment.TRAILING)))
                                                                            .addComponent(label8))
                                                                    .addGap(18, 18, 18)
                                                                    .addGroup(panel2Layout.createParallelGroup()
                                                                            .addComponent(subtotal)
                                                                            .addComponent(discount, GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
                                                                            .addComponent(delivery, GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)))
                                                            .addGroup(panel2Layout.createSequentialGroup()
                                                                    .addGap(0, 700, Short.MAX_VALUE)
                                                                    .addComponent(label5)
                                                                    .addGap(18, 18, 18)
                                                                    .addComponent(total, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE))))
                                            .addGroup(GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                                                    .addGap(0, 668, Short.MAX_VALUE)
                                                    .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 209, GroupLayout.PREFERRED_SIZE)))
                                    .addContainerGap())
                    );
                    panel2Layout.linkSize(SwingConstants.HORIZONTAL, new Component[] {add, remove});
                    panel2Layout.setVerticalGroup(
                        panel2Layout.createParallelGroup()
                            .addGroup(GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel2Layout.createParallelGroup()
                                            .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                    .addComponent(add)
                                                    .addComponent(remove))
                                            .addGroup(panel2Layout.createSequentialGroup()
                                                    .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                            .addComponent(label3)
                                                            .addComponent(subtotal, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                    .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                            .addComponent(label4)
                                                            .addComponent(discount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(delivery, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(label8))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 5, GroupLayout.PREFERRED_SIZE)
                                    .addGap(9, 9, 9)
                                    .addGroup(panel2Layout.createParallelGroup()
                                            .addComponent(label5)
                                            .addComponent(total, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addContainerGap())
                    );
                }

                //======== panel4 ========
                {
                    panel4.setBorder(new TitledBorder("Shipping Info"));

                    //---- label6 ----
                    label6.setText("Name");

                    //---- label7 ----
                    label7.setText("Address");

                    //======== scrollPane2 ========
                    {
                        scrollPane2.setViewportView(shippingAddress);
                    }

                    GroupLayout panel4Layout = new GroupLayout(panel4);
                    panel4.setLayout(panel4Layout);
                    panel4Layout.setHorizontalGroup(
                        panel4Layout.createParallelGroup()
                            .addGroup(panel4Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addGroup(panel4Layout.createParallelGroup()
                                            .addComponent(label6)
                                            .addComponent(label7))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 70, Short.MAX_VALUE)
                                    .addGroup(panel4Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                            .addComponent(shippingName, GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE)
                                            .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE))
                                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    );
                    panel4Layout.setVerticalGroup(
                        panel4Layout.createParallelGroup()
                            .addGroup(panel4Layout.createSequentialGroup()
                                    .addGroup(panel4Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(label6)
                                            .addComponent(shippingName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel4Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(label7)
                                            .addComponent(scrollPane2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                                    .addGap(0, 0, Short.MAX_VALUE))
                    );
                }

                GroupLayout panellLayout = new GroupLayout(panell);
                panell.setLayout(panellLayout);
                panellLayout.setHorizontalGroup(
                    panellLayout.createParallelGroup()
                        .addGroup(GroupLayout.Alignment.TRAILING, panellLayout.createSequentialGroup()
                                .addGroup(panellLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                        .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(panellLayout.createSequentialGroup()
                                                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(panel4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addContainerGap())
                );
                panellLayout.setVerticalGroup(
                    panellLayout.createParallelGroup()
                        .addGroup(panellLayout.createSequentialGroup()
                                .addGroup(panellLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(panel4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );
            }
            dialogPane.add(panell, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(null);

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        cancelButtonActionPerformed(e);
                    }
                });

                //---- btnRecord ----
                btnRecord.setText("Record ");
                btnRecord.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnRecordActionPerformed(e);
                    }
                });

                //---- btnRecordPrint ----
                btnRecordPrint.setText("Record & Print");
                btnRecordPrint.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnRecordPrintActionPerformed(e);
                    }
                });

                GroupLayout buttonBarLayout = new GroupLayout(buttonBar);
                buttonBar.setLayout(buttonBarLayout);
                buttonBarLayout.setHorizontalGroup(
                    buttonBarLayout.createParallelGroup()
                        .addGroup(GroupLayout.Alignment.TRAILING, buttonBarLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(btnRecord, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnRecordPrint, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 531, Short.MAX_VALUE)
                                .addComponent(cancelButton, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                                .addGap(16, 16, 16))
                );
                buttonBarLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {btnRecord, btnRecordPrint});
                buttonBarLayout.setVerticalGroup(
                    buttonBarLayout.createParallelGroup()
                        .addGroup(GroupLayout.Alignment.TRAILING, buttonBarLayout.createSequentialGroup()
                                .addContainerGap(18, Short.MAX_VALUE)
                                .addGroup(buttonBarLayout.createParallelGroup()
                                        .addComponent(cancelButton, GroupLayout.Alignment.TRAILING)
                                        .addGroup(buttonBarLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                .addComponent(btnRecord)
                                                .addComponent(btnRecordPrint)))
                                .addContainerGap())
                );
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel dialogPane;
    private JPanel panell;
    private JPanel panel1;
    private JLabel label1;
    private JComboBox cbCustomer;
    private JButton btnNewCustomer;
    private JLabel label2;
    private JDateChooser dateChooser1;
    private JLabel label9;
    private JTextField tfCustomerId;
    private JButton btnSearchCustomerID;
    private JPanel panel2;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JButton add;
    private JButton remove;
    private JLabel label3;
    private JTextField subtotal;
    private JLabel label4;
    private JTextField discount;
    private JSeparator separator1;
    private JLabel label5;
    private JFormattedTextField total;
    private JLabel label8;
    private JFormattedTextField delivery;
    private JPanel panel4;
    private JLabel label6;
    private JTextField shippingName;
    private JLabel label7;
    private JScrollPane scrollPane2;
    private JTextArea shippingAddress;
    private JPanel buttonBar;
    private JButton cancelButton;
    private JButton btnRecord;
    private JButton btnRecordPrint;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}

