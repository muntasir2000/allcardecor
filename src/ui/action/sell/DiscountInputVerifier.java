package ui.action.sell;

import javax.swing.*;
import java.awt.*;
import java.text.ParseException;

/**
 * Created by Tareq on 5/1/2014.
 */

public class DiscountInputVerifier extends InputVerifier {
    public boolean verify(JComponent input) {
        if (input instanceof JFormattedTextField) {
            JFormattedTextField ftf = (JFormattedTextField) input;
            JFormattedTextField.AbstractFormatter formatter = ftf.getFormatter();
            if (formatter != null) {
                String text = ftf.getText();
                try {
                    String str = (String) formatter.stringToValue(text);
                    if (str.matches("[0-9]+")) {
                        input.setBackground(UIManager.getColor("TextField.background"));
                        return true;
                    } else if (!str.matches("[0-9]{1,2}%")) {
                        input.setBackground(Color.red);
                        return false;
                    }
                    return true;
                } catch (ParseException pe) {
                    input.setBackground(Color.red);
                    return false;
                }
            }
        }
        return true;
    }

    public boolean shouldYieldFocus(JComponent input) {
        return verify(input);
    }
}