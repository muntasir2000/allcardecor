package ui.action.sell;

import javax.swing.table.DefaultTableModel;

/**
 * Created by Muntasir on 4/24/2014.
 */

class SellTableModel extends DefaultTableModel {
    public SellTableModel(Object[] columnNames, int rowCount) {
        super(columnNames, rowCount);
    }

    @Override
    public Class getColumnClass(int columnIndex) {
//        return ((Class) dataVector.get(columnIndex)).getClass();
        if (columnIndex <= 1) {
            return String.class;
        } else return Integer.class;
    }

    @Override
    public void addRow(Object[] rowData) {
        super.addRow(new Object[]{rowData[0], rowData[1], rowData[2], rowData[3], rowData[4]});
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        if (column <= 1 || column > 2)
            return false;
        else return true;

    }
}