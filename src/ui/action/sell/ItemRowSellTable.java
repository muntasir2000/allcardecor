package ui.action.sell;

import core.debug.Debug;

/**
 * Created by Tareq on 5/8/2014.
 */
public class ItemRowSellTable {
    float invoiceID;
    int serial;
    String productName;
    String customerName;
    String date;
    int cost;
    int warranty;
    String manID;
    boolean isRetail = true;

    public ItemRowSellTable(long invoiceID, int serial, String customerName, String productName, int cost, int warranty, String date, String manID) {
        this.invoiceID = invoiceID;
        this.serial = serial;
        this.customerName = customerName;
        this.productName = productName;
        this.cost = cost;
        this.warranty = warranty;
        this.date = date;
        this.manID = manID;
        Debug.print(date, "date in ItemRowSellTable");
    }

    public ItemRowSellTable(long invoiceID, int serial, String customerName, String productName, int cost, int warranty, String date, String manID, boolean isRetail) {
        this(invoiceID, serial, customerName, productName, cost, warranty, date, manID);
        this.isRetail = false;
    }

    public String generateInsertSQLforSellEntry() {

        if (!isRetail)
            return "INSERT INTO SELL_RECORDS (INVOICE_ID, SERIAL, CUSTOMER, PRODUCT, COST, WARRANTY, ISRETAIL, DATE, MAN_ID) VALUES(" + invoiceID + ", " + serial + ", '" + customerName + "', '" + productName + "', " + cost + "," + warranty + ",'false', '" + date + "','" + manID + "');";
        else
            return "INSERT INTO SELL_RECORDS (INVOICE_ID, SERIAL, CUSTOMER, PRODUCT, COST, WARRANTY, ISRETAIL, DATE, MAN_ID) VALUES(" + invoiceID + ", " + serial + ", '" + customerName + "', '" + productName + "', " + cost + "," + warranty + ",'true', '" + date + "','" + manID + "');";
    }

    public String generateUpdateSQLforStockInventory() {
        return "UPDATE STOCK_INVENTORY SET SOLD = 'true' WHERE SERIAL = " + serial + ";";

    }
}

