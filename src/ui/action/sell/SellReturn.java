/*
 * Created by JFormDesigner on Sun Apr 27 00:51:18 ALMT 2014
 */

package ui.action.sell;

import core.debug.Debug;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

/**
 * @author Tareq  Al Muntasir
 */
public class SellReturn extends JDialog {
    String strProductName, strInvoiceID, strSellingDate, strDescription, strCustomer;
    boolean found = false;
    ReturnTableModel tableModel;
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JPanel panel1;
    private JLabel label1;
    private JTextField serial;
    private JLabel label2;
    private JTextField product;
    private JLabel label3;
    private JTextField sellingDate;
    private JLabel label4;
    private JTextField description;
    private JLabel label5;
    private JButton btnAdd;
    private JButton btnSearch;
    private JLabel label6;
    private JTextField customer;
    private JLabel label7;
    private JTextField invoice;
    private JScrollPane scrollPane2;
    private JTextArea cause;
    private JPanel panel2;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JButton remove;
    private JPanel buttonBar;
    private JButton btnRecord;
    private JButton okButton;
    private JButton cancelButton;

    public SellReturn(Frame owner) {
        super(owner);
        initComponents();
    }

    public SellReturn(Dialog owner) {
        super(owner);
        initComponents();
    }

    boolean searchDB(String strSerial) {
        int serialNo;
        Connection connection = null;
        Statement statement = null;
        try {
            serialNo = Integer.parseInt(strSerial);
        } catch (NumberFormatException e) {
            return false;
        }


        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");

            statement = connection.createStatement();

            String sql = "SELECT SELL_RECORDS.CUSTOMER, SELL_RECORDS.PRODUCT, SELL_RECORDS.INVOICE_ID, SELL_RECORDS.DATE, PRODUCTS.DESCRIPTION FROM SELL_RECORDS, PRODUCTS WHERE SELL_RECORDS.SERIAL = " + serialNo + " AND PRODUCTS.NAME = SELL_RECORDS.PRODUCT AND SELL_RECORDS.ISRETURNED = 'false';";
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                strProductName = resultSet.getString("PRODUCT");
                strCustomer = resultSet.getString("CUSTOMER");
                strDescription = resultSet.getString("DESCRIPTION");
                strSellingDate = resultSet.getString("DATE");
                strInvoiceID = resultSet.getInt("INVOICE_ID") + "";

                found = true;
            } else {
                found = false;
                JOptionPane.showMessageDialog(panel1, "No records found regarding the entered serial number", "Not found", JOptionPane.ERROR_MESSAGE);
            }
            resultSet.close();

        } catch (SQLException e) {
            found = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return found;


    }

    private void btnAddActionPerformed(ActionEvent e) {
        Debug.print(" " + found, "found");
        if (found) {
            if (!isExistsOnTable(serial.getText())) {
                tableModel.addRow(new Object[]{serial.getText(), strProductName, strSellingDate, cause.getText()});
                tableModel.fireTableDataChanged();

                customer.setText("");
                invoice.setText("");
                description.setText("");
                product.setText("");
                sellingDate.setText("");
                cause.setText("");
                serial.setText("");
            }
        }
    }

    private boolean isExistsOnTable(String s) {
        for (int i = 0; i < tableModel.getRowCount(); i++) {
            String str = (String) tableModel.getValueAt(i, 0);
            if (str.equals(s))
                return true;
        }
        return false;
    }

    private void btnSearchActionPerformed(ActionEvent e) {
        String str = serial.getText();
        if (searchDB(str)) {
            customer.setText(strCustomer);
            invoice.setText(strInvoiceID);
            description.setText(strDescription);
            product.setText(strProductName);
            sellingDate.setText(strSellingDate);
        } else {
            customer.setText("");
            invoice.setText("");
            description.setText("");
            product.setText("");
            sellingDate.setText("");
        }
    }

    private void removeActionPerformed(ActionEvent e) {
        int[] rows = table1.getSelectedRows();
        for (int i = 0; i < rows.length; i++) {
            tableModel.removeRow(rows[i] - i);
        }
        tableModel.fireTableDataChanged();
    }

    private void btnRecordActionPerformed(ActionEvent e) {

        Connection connection = null;
        PreparedStatement preparedStatement = null, preparedStatement1 = null, insertReturnedItemsStatement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            String updateSellRecordsSQL = "UPDATE SELL_RECORDS SET ISRETURNED = 'true' WHERE SERIAL = ?;";
            String updateStockInventorySQL = "UPDATE STOCK_INVENTORY SET SOLD = 'false' WHERE SERIAL = ?;";
            String insertSQL = "INSERT INTO RETURNED_ITEMS ( SERIAL, CAUSE, PRODUCT) VALUES(?, ?,?);";
            preparedStatement1 = connection.prepareStatement(updateStockInventorySQL);
            insertReturnedItemsStatement = connection.prepareStatement(insertSQL);
            preparedStatement = connection.prepareStatement(updateSellRecordsSQL);
            for (int i = 0; i < tableModel.getRowCount(); i++) {
                int s = Integer.parseInt(tableModel.getValueAt(i, 0).toString());
                preparedStatement.setInt(1, s);
                preparedStatement.executeUpdate();

                preparedStatement1.setInt(1, s);
                preparedStatement1.executeUpdate();

                insertReturnedItemsStatement.setInt(1, s);
                insertReturnedItemsStatement.setString(2, tableModel.getValueAt(i, 3).toString());
                insertReturnedItemsStatement.setString(3, tableModel.getValueAt(i, 1).toString());
                insertReturnedItemsStatement.executeUpdate();
            }
            preparedStatement.close();
            preparedStatement1.close();
            insertReturnedItemsStatement.close();

            connection.commit();
        } catch (SQLException e1) {
            e1.printStackTrace();
            JOptionPane.showMessageDialog(btnRecord, "Error occured. Database not updated. Please try again.", "Critical Error", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (preparedStatement1 != null) {
                    preparedStatement1.close();
                }
                if (insertReturnedItemsStatement != null) {
                    insertReturnedItemsStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e2) {
                e2.printStackTrace();
                Debug.print("Error in database closing. ", "SellReturn");
            }
        }

        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        panel1 = new JPanel();
        label1 = new JLabel();
        serial = new JTextField();
        label2 = new JLabel();
        product = new JTextField();
        label3 = new JLabel();
        sellingDate = new JTextField();
        label4 = new JLabel();
        description = new JTextField();
        label5 = new JLabel();
        btnAdd = new JButton();
        btnSearch = new JButton();
        label6 = new JLabel();
        customer = new JTextField();
        label7 = new JLabel();
        invoice = new JTextField();
        scrollPane2 = new JScrollPane();
        cause = new JTextArea();
        panel2 = new JPanel();
        scrollPane1 = new JScrollPane();
        String[] columns = {"Serial", "Product", "Selling Date", "Cause"};
        tableModel = new ReturnTableModel(columns, 0);
        table1 = new JTable(tableModel);
        remove = new JButton();
        buttonBar = new JPanel();
        btnRecord = new JButton();
        okButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        setTitle("Return Sold Product");
        setModal(true);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));

            // JFormDesigner evaluation mark

            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {

                //======== panel1 ========
                {
                    panel1.setBorder(new TitledBorder("Item Info"));

                    //---- label1 ----
                    label1.setText("Enter Serial to Search");

                    //---- label2 ----
                    label2.setText("Product");

                    //---- product ----
                    product.setEditable(false);

                    //---- label3 ----
                    label3.setText("Selling Date");

                    //---- sellingDate ----
                    sellingDate.setEditable(false);

                    //---- label4 ----
                    label4.setText("Description");

                    //---- description ----
                    description.setEditable(false);

                    //---- label5 ----
                    label5.setText("Returning Cause");

                    //---- btnAdd ----
                    btnAdd.setText("Add");
                    btnAdd.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnAddActionPerformed(e);
                        }
                    });

                    //---- btnSearch ----
                    btnSearch.setText("Search");
                    btnSearch.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnSearchActionPerformed(e);
                        }
                    });

                    //---- label6 ----
                    label6.setText("Customer Name");

                    //---- customer ----
                    customer.setEditable(false);

                    //---- label7 ----
                    label7.setText("Invoice ID");

                    //---- invoice ----
                    invoice.setEditable(false);

                    //======== scrollPane2 ========
                    {
                        scrollPane2.setViewportView(cause);
                    }

                    GroupLayout panel1Layout = new GroupLayout(panel1);
                    panel1.setLayout(panel1Layout);
                    panel1Layout.setHorizontalGroup(
                            panel1Layout.createParallelGroup()
                                    .addGroup(panel1Layout.createSequentialGroup()
                                            .addContainerGap()
                                            .addGroup(panel1Layout.createParallelGroup()
                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                            .addGroup(panel1Layout.createParallelGroup()
                                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                                            .addGroup(panel1Layout.createParallelGroup()
                                                                                    .addComponent(label1)
                                                                                    .addComponent(label6)
                                                                                    .addComponent(label2))
                                                                            .addGap(26, 26, 26)
                                                                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                                                                    .addComponent(serial)
                                                                                    .addComponent(customer, GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE)
                                                                                    .addComponent(product, GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE))
                                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                                            .addGroup(panel1Layout.createParallelGroup()
                                                                                    .addComponent(btnSearch, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                                                                                    .addGroup(panel1Layout.createParallelGroup()
                                                                                            .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                                                                                    .addComponent(label7)
                                                                                                    .addGap(17, 17, 17)
                                                                                                    .addComponent(invoice, GroupLayout.PREFERRED_SIZE, 228, GroupLayout.PREFERRED_SIZE))
                                                                                            .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                                                                                    .addComponent(label3)
                                                                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                                                                    .addComponent(sellingDate, GroupLayout.PREFERRED_SIZE, 228, GroupLayout.PREFERRED_SIZE))))
                                                                            .addGap(0, 0, Short.MAX_VALUE))
                                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                                            .addGroup(panel1Layout.createParallelGroup()
                                                                                    .addComponent(label4)
                                                                                    .addComponent(label5))
                                                                            .addGap(49, 49, 49)
                                                                            .addGroup(panel1Layout.createParallelGroup()
                                                                                    .addComponent(description)
                                                                                    .addComponent(scrollPane2))))
                                                            .addContainerGap())
                                                    .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                                            .addGap(0, 0, Short.MAX_VALUE)
                                                            .addComponent(btnAdd, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
                                                            .addGap(288, 288, 288))))
                    );
                    panel1Layout.linkSize(SwingConstants.HORIZONTAL, new Component[]{customer, product, sellingDate});
                    panel1Layout.setVerticalGroup(
                            panel1Layout.createParallelGroup()
                                    .addGroup(panel1Layout.createSequentialGroup()
                                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                    .addComponent(serial, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(btnSearch)
                                                    .addComponent(label1))
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                    .addComponent(customer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(label7)
                                                    .addComponent(label6)
                                                    .addComponent(invoice, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                    .addComponent(sellingDate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(product, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(label2)
                                                    .addComponent(label3))
                                            .addGap(18, 18, 18)
                                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                    .addComponent(label4)
                                                    .addComponent(description, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(panel1Layout.createParallelGroup()
                                                    .addComponent(label5)
                                                    .addComponent(scrollPane2, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE))
                                            .addGap(18, 18, 18)
                                            .addComponent(btnAdd)
                                            .addGap(0, 22, Short.MAX_VALUE))
                    );
                }

                //======== panel2 ========
                {
                    panel2.setBorder(new TitledBorder("Selected Items"));

                    //======== scrollPane1 ========
                    {
                        scrollPane1.setViewportView(table1);
                    }

                    //---- remove ----
                    remove.setText("Remove");
                    remove.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            removeActionPerformed(e);
                        }
                    });

                    GroupLayout panel2Layout = new GroupLayout(panel2);
                    panel2.setLayout(panel2Layout);
                    panel2Layout.setHorizontalGroup(
                            panel2Layout.createParallelGroup()
                                    .addGroup(panel2Layout.createSequentialGroup()
                                            .addContainerGap()
                                            .addGroup(panel2Layout.createParallelGroup()
                                                    .addGroup(panel2Layout.createSequentialGroup()
                                                            .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 675, Short.MAX_VALUE)
                                                            .addContainerGap())
                                                    .addGroup(GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                                                            .addGap(0, 307, Short.MAX_VALUE)
                                                            .addComponent(remove, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
                                                            .addGap(278, 278, 278))))
                    );
                    panel2Layout.setVerticalGroup(
                            panel2Layout.createParallelGroup()
                                    .addGroup(panel2Layout.createSequentialGroup()
                                            .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(remove, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addContainerGap())
                    );
                }

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 697, Short.MAX_VALUE)
                                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );
                contentPanelLayout.setVerticalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[]{0, 0, 85, 80};
                ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[]{1.0, 0.0, 0.0, 0.0};

                //---- btnRecord ----
                btnRecord.setText("Record");
                btnRecord.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnRecordActionPerformed(e);
                    }
                });
                buttonBar.add(btnRecord, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 5), 0, 0));

                //---- okButton ----
                okButton.setText("OK");
                buttonBar.add(okButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                buttonBar.add(cancelButton, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
