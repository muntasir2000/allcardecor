package ui.action.sell;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableModel;
import java.awt.*;
import java.sql.*;

/**
 * Created by Muntasir on 4/25/2014.
 */
public class SellTableCellEditor extends AbstractCellEditor implements TableCellEditor {
    // This is the component that will handle the editing of the cell value
    JComponent component = new JTextField();

    JTable jTable = null;
    int rowIndex, columnIndex;
    String productName, description, manID;
    int warranty, cost;
    boolean isRetail = false;

    public SellTableCellEditor(boolean isRetail) {
        this.isRetail = isRetail;
    }

    // This method is called when a cell value is edited by the user.
    public Component getTableCellEditorComponent(JTable table, Object value,
                                                 boolean isSelected, int rowIndex, int vColIndex) {
        // 'value' is value contained in the cell located at (rowIndex, vColIndex)
        jTable = table;
        this.rowIndex = rowIndex;
        this.columnIndex = columnIndex;
        if (isSelected) {
            // cell (and perhaps other cells) are selected
        }
        // Configure the component with the specified value
        ((JTextField) component).setText((String) value);

        // Return the configured component
        return component;
    }

    // This method is called when editing is completed.
    // It must return the new value to be stored in the cell.
    public Object getCellEditorValue() {
//        jTable.getModel().setValueAt("Test",rowIndex, 0);
        String serial = ((JTextField) component).getText();
        jTable.clearSelection();
        int s = 0;
        try {
            s = Integer.parseInt(serial);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
       /* if(isExistsOnTable(s)){
            JOptionPane.showMessageDialog(component, "Duplicate", "Item not found", JOptionPane.OK_OPTION);
            return "";
        }*/
        if (searchDB(serial)) {
            jTable.getModel().setValueAt(productName, rowIndex, 0);
            jTable.getModel().setValueAt(description, rowIndex, 1);
            jTable.getModel().setValueAt(manID, rowIndex, 3);
            jTable.getModel().setValueAt(new Integer(warranty), rowIndex, 4);
            jTable.getModel().setValueAt(cost, rowIndex, 5);
            return serial;
        } else {
            JOptionPane.showMessageDialog(component, "Item not found in stock. Recheck Serial number", "Item not found", JOptionPane.OK_OPTION);
            return "";
        }
    }

    private boolean isExistsOnTable(Integer integer) {
        TableModel tableModel = jTable.getModel();
        if (tableModel.getRowCount() <= 1) {
            return false;
        }
        for (int i = 0; i < tableModel.getRowCount(); i++) {
            Integer ss = 0;
            try {
                ss = Integer.parseInt(tableModel.getValueAt(i, 2).toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            if (ss.intValue() == integer.intValue())
                return true;
        }
        return false;
    }

    private boolean searchDB(String s) {
        Connection connection = null;
        Statement statement = null;
        int serial = 0;
        try {
            serial = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        boolean found = false;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");

            statement = connection.createStatement();

            String sql = "SELECT PRODUCT, WARRANTY, MAN_ID FROM STOCK_INVENTORY WHERE SERIAL = " + serial + " AND SOLD = 'false';";
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                productName = resultSet.getString("PRODUCT");
                warranty = resultSet.getInt("WARRANTY");
                manID = resultSet.getString("MAN_ID");
                found = true;
            }
            resultSet.close();
            if (found) {
                String sql2 = "SELECT RETAIL, DESCRIPTION, WHOLESALE FROM PRODUCTS WHERE NAME = '" + productName + "';";
                ResultSet rs2 = statement.executeQuery(sql2);

                if (rs2.next()) {
                    if (isRetail)
                        cost = rs2.getInt("RETAIL");
                    else
                        cost = rs2.getInt("WHOLESALE");
                    description = resultSet.getString("DESCRIPTION");
                }
                rs2.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return found;
    }
}