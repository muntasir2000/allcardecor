/*
 * Created by JFormDesigner on Sun Apr 27 14:39:03 ALMT 2014
 */

package ui.action.sell;

import com.toedter.calendar.JDateChooser;
import core.debug.Debug;
import net.sf.dynamicreports.report.exception.DRException;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import ui.init.NewCustomerDialog;
import ui.report.invoice.dynamicreport.InvoiceGenerator;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.sql.*;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;

/**
 * @author Tareq  Al Muntasir
 */
public class SellProductWholesale extends JDialog {
    SellTableModel tableModel;
    HashMap<String, Integer> discountMap;
    HashMap<Integer, String> customerIdNameMap;

    private long invoiceID;

    public SellProductWholesale(Frame owner) {
        super(owner);
        initComponents();
        loadData();
        addListeners();
        setupMisc();
        table1.setRowHeight(25);
    }

    public SellProductWholesale(Dialog owner) {
        super(owner);
        initComponents();
    }

    private void addListeners() {

        cbCustomer.addItemListener(new CustomerChangedListener(discount, tfCustomerId, discountMap, customerIdNameMap));
        discount.getDocument().addDocumentListener(new DiscountChangedListener(subtotal, total, discount, delivery));
        tableModel.addTableModelListener(new PriceChangedListenerMulti(subtotal, discount, total, delivery));
        delivery.getDocument().addDocumentListener(new DiscountChangedListener(subtotal, total, discount, delivery));
    }

    private void setupMisc() {

        DiscountInputVerifier inputVerifier = new DiscountInputVerifier();
        discount.setInputVerifier(inputVerifier);
        discount.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                btnRecord.setEnabled(false);
                btnRecordAndPrint.setEnabled(false);
            }

            @Override
            public void focusLost(FocusEvent e) {
                btnRecord.setEnabled(true);
                btnRecordAndPrint.setEnabled(true);
            }
        });

        String initialCustomer = cbCustomer.getSelectedItem().toString();
        discount.setText(discountMap.get(initialCustomer).toString() + "%");

        //init the dateChooser with current date/time
        //java.util.Date  currentDate = new Date(Calendar.getInstance().getTime());
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Dhaka"));
        dateChooser1.setDate(cal.getTime());
        AutoCompleteDecorator.decorate(cbCustomer);
    }

    private void loadData() {
        discountMap = new HashMap<String, Integer>();
        customerIdNameMap = new HashMap<Integer, String>();
        Connection connection = null;
        Statement statement = null;
        ResultSet rs;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");

            String sql = "SELECT * FROM CUSTOMER;";
            statement = connection.createStatement();
            rs = statement.executeQuery(sql);

            while (rs.next()) {
                String name = rs.getString("NAME");
                cbCustomer.addItem(name);

                customerIdNameMap.put(rs.getInt("ID"), name);
                discountMap.put(name, rs.getInt("DISCOUNT"));
            }
            rs.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    Debug.print("statement null", "dbmanager");
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Database closing error");
            }
        }
    }

    private void btnNewCustomerActionPerformed(ActionEvent e) {
        NewCustomerDialog newCustomerDialog = new NewCustomerDialog(this, cbCustomer, discountMap);
        newCustomerDialog.setVisible(true);
    }

    private boolean validateRows() {
        Debug.print(tableModel.getRowCount() + "   " + tableModel.getColumnCount(), "row column count");
        //iterate through the last row and validate input
        for (int i = tableModel.getRowCount(); i >= 1; i--) {
            for (int j = tableModel.getColumnCount(); j >= 1; j--) {
                if (tableModel.getValueAt(i - 1, j - 1) == null) {
                    JOptionPane.showMessageDialog(table1, "Field empty. Insert proper value.");
                    return false;
                }
            }
        }
        return true;
    }

    private void saveToDB() {
        Connection connection = null;
        PreparedStatement statement = null;
        String strDate;

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Dhaka"));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        java.util.Date chosenDate = dateChooser1.getDate();
        //  chosenDate.setTime(cal.getTimeInMillis());
        strDate = simpleDateFormat.format(chosenDate);

        //calculate discounted amount for database entry
        int discountedAmount = 0;
        try {
            int totalPrice = Integer.parseInt(total.getText());

            int subTotalPrice = Integer.parseInt(subtotal.getText());
            discountedAmount = totalPrice - subTotalPrice;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        int deliveryCharge = 0;
        try {
            deliveryCharge = Integer.parseInt(delivery.getText());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }


        discountedAmount -= deliveryCharge;
        discountedAmount = Math.abs(discountedAmount);

        Debug.print(strDate, " date check");

        try {
            Class.forName("org.sqlite.JDBC");

            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            String sql = "INSERT INTO SELL_INVOICE  (CUSTOMER, DATE, TOTAL, DISCOUNT, COUNT, ISRETAIL, SHIPPING_ADDRESS, SHIP_TO, DELIVERY_CHARGE) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, cbCustomer.getSelectedItem().toString());
            statement.setString(2, strDate);
            statement.setInt(3, Integer.parseInt(total.getText().toString()));
            statement.setInt(4, discountedAmount);
            statement.setInt(5, tableModel.getRowCount());
            statement.setString(6, "false");
            statement.setString(7, shippingName.getText());
            statement.setString(8, shippingAddress.getText());
            statement.setInt(9, deliveryCharge);
            statement.executeUpdate();
            connection.commit();
            String sql2 = "SELECT last_insert_rowid();";


            boolean rowIDfound = false;
            Statement legacyStatement = connection.createStatement();

            ResultSet RSrowID = legacyStatement.executeQuery(sql2);
            if (RSrowID.next()) {
                Debug.print("last row insert id found", "XX");
                rowIDfound = true;
                invoiceID = RSrowID.getLong(1);
            }

            if (!rowIDfound) {
                JOptionPane.showMessageDialog(this, "Operation unsuccessful. Something is terribly wrong!", "Fatal Error", JOptionPane.ERROR_MESSAGE);
                connection.close();
                return;
            }

            RSrowID.close();


            ArrayList<ItemRowSellTable> list = fetchDataFromTable();

            for (ItemRowSellTable item : list) {
                legacyStatement.executeUpdate(item.generateInsertSQLforSellEntry());
            }

            for (ItemRowSellTable item : list) {
                legacyStatement.executeUpdate(item.generateUpdateSQLforStockInventory());
            }
            connection.commit();
            statement.close();
            legacyStatement.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Database closing error");
            }
        }

    }

    private ArrayList<ItemRowSellTable> fetchDataFromTable() {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Dhaka"));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        java.util.Date chosenDate = dateChooser1.getDate();
        //    chosenDate.setTime(cal.getTimeInMillis());
        String strSelectedDate = simpleDateFormat.format(chosenDate);

        int row = tableModel.getRowCount();
        ArrayList<ItemRowSellTable> list = new ArrayList<ItemRowSellTable>();
        for (int i = 0; i < row; i++) {
            String product = tableModel.getValueAt(i, 0).toString();
            int serial = Integer.parseInt(tableModel.getValueAt(i, 2).toString());
            int warranty = Integer.parseInt(tableModel.getValueAt(i, 4).toString());
            int price = Integer.parseInt(tableModel.getValueAt(i, 5).toString());
            //     int warranty = Integer.parseInt(tableModel.getValueAt(i, 3).toString());
            String manID = tableModel.getValueAt(i, 3).toString();
            Debug.print("Price: " + price + ", Warranty: " + warranty, "date check in init object");
            ItemRowSellTable item = new ItemRowSellTable(invoiceID, serial, cbCustomer.getSelectedItem().toString(), product, price, warranty, strSelectedDate, manID, false);
            list.add(item);
        }
        return list;
    }

    private void addActionPerformed(ActionEvent e) {
        if (!validateRows()) {
            return;
        }

        tableModel.addRow(new Object[]{null, null, null, new Integer(0), new Integer(0)});
        tableModel.fireTableDataChanged();
    }

    private void removeActionPerformed(ActionEvent e) {

        int[] rows = table1.getSelectedRows();
        for (int i = 0; i < rows.length; i++) {
            tableModel.removeRow(rows[i] - i);
        }
        //  tableModel.fireTableRowsDeleted(rows[0], rows[rows.length-1]);
        tableModel.fireTableDataChanged();
    }

    private void btnRecordActionPerformed(ActionEvent e) {
        if (tableModel.getValueAt(0, 0) == null) {
            JOptionPane.showMessageDialog(this, "No items added. Nothing to record", "Nothing to record", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        saveToDB();
        dispose();
    }

    private void btnRecordAndPrintActionPerformed(ActionEvent e) {
        saveToDB();
        InvoiceGenerator invoiceGenerator = new InvoiceGenerator(new Integer(invoiceID + ""));
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        try {
            invoiceGenerator.build().show(false);
        } catch (DRException e1) {
            e1.printStackTrace();
        }

        this.setCursor(Cursor.getDefaultCursor());
        dispose();
    }

    private void cancelBtnActionPerformed(ActionEvent e) {
        dispose();
    }

    private void btnSearchCustomerIDActionPerformed(ActionEvent e) {
        int id = 0;
        try{
            id = Integer.parseInt(tfCustomerId.getText());
        }catch (NumberFormatException ex){
            JOptionPane.showMessageDialog(this, "Please enter a valid Customer ID. Only number is allowed","Invalid Input", JOptionPane.INFORMATION_MESSAGE);
        }

        if(id!=0){
            String customerName = customerIdNameMap.get(id);
            cbCustomer.setSelectedItem(customerName);
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        panel1 = new JPanel();
        label1 = new JLabel();
        label2 = new JLabel();
        cbCustomer = new JComboBox();
        btnNewCustomer = new JButton();
        dateChooser1 = new JDateChooser();
        label9 = new JLabel();
        tfCustomerId = new JTextField();
        btnSearchCustomerID = new JButton();
        panel2 = new JPanel();
        scrollPane1 = new JScrollPane();
        Object[] columnNames = {"Item Name", "Description", "Serial No.","Man.ID", "Warranty (days)","Price"};
        tableModel = new SellTableModel(columnNames, 1);
        table1 = new JTable(tableModel);
        TableColumn col = table1.getColumnModel().getColumn(2);
        col.setCellEditor(new SellTableCellEditor(false));
        //table1.getColumnModel().getColumn(1).setPreferredWidth(130);
        add = new JButton();
        remove = new JButton();
        label3 = new JLabel();
        label4 = new JLabel();
        subtotal = new JTextField();
         RegexFormatter regexFormatter = new RegexFormatter("\\d+%?");

        discount = new JFormattedTextField(regexFormatter);
        separator1 = new JSeparator();
        label5 = new JLabel();
        total = new JFormattedTextField();
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        delivery = new JFormattedTextField(numberFormat);
        label8 = new JLabel();
        panel3 = new JPanel();
        label6 = new JLabel();
        shippingName = new JTextField();
        label7 = new JLabel();
        scrollPane2 = new JScrollPane();
        shippingAddress = new JTextArea();
        btnRecord = new JButton();
        btnRecordAndPrint = new JButton();
        cancelBtn = new JButton();

        //======== this ========
        setTitle("Sell Wholesale Product");
        setModal(true);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));


            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {

                //======== panel1 ========
                {
                    panel1.setBorder(new TitledBorder("Customer Info"));

                    //---- label1 ----
                    label1.setText("Customer Name");

                    //---- label2 ----
                    label2.setText("Date");

                    //---- btnNewCustomer ----
                    btnNewCustomer.setText("New");
                    btnNewCustomer.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnNewCustomerActionPerformed(e);
                        }
                    });

                    //---- label9 ----
                    label9.setText("Customer ID");

                    //---- btnSearchCustomerID ----
                    btnSearchCustomerID.setText("Search");
                    btnSearchCustomerID.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnSearchCustomerIDActionPerformed(e);
                        }
                    });

                    GroupLayout panel1Layout = new GroupLayout(panel1);
                    panel1.setLayout(panel1Layout);
                    panel1Layout.setHorizontalGroup(
                        panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(panel1Layout.createParallelGroup()
                                    .addComponent(label1)
                                    .addComponent(label2)
                                    .addComponent(label9))
                                .addGap(29, 29, 29)
                                .addGroup(panel1Layout.createParallelGroup()
                                    .addGroup(panel1Layout.createSequentialGroup()
                                        .addComponent(cbCustomer, GroupLayout.PREFERRED_SIZE, 224, GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnNewCustomer, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panel1Layout.createSequentialGroup()
                                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                            .addComponent(dateChooser1, GroupLayout.PREFERRED_SIZE, 279, GroupLayout.PREFERRED_SIZE)
                                            .addGroup(panel1Layout.createSequentialGroup()
                                                .addComponent(tfCustomerId, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(btnSearchCustomerID, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(55, 55, 55)))
                                        .addGap(0, 0, Short.MAX_VALUE)))
                                .addContainerGap())
                    );
                    panel1Layout.setVerticalGroup(
                        panel1Layout.createParallelGroup()
                            .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(panel1Layout.createParallelGroup()
                                    .addComponent(label1)
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(cbCustomer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnNewCustomer)))
                                .addGroup(panel1Layout.createParallelGroup()
                                    .addGroup(panel1Layout.createSequentialGroup()
                                        .addGap(9, 9, 9)
                                        .addComponent(label9))
                                    .addGroup(panel1Layout.createSequentialGroup()
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(tfCustomerId, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(btnSearchCustomerID))))
                                .addGap(18, 18, 18)
                                .addGroup(panel1Layout.createParallelGroup()
                                    .addComponent(label2)
                                    .addComponent(dateChooser1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(14, Short.MAX_VALUE))
                    );
                }

                //======== panel2 ========
                {
                    panel2.setBorder(new TitledBorder("Item Info"));

                    //======== scrollPane1 ========
                    {

                        //---- table1 ----
                        table1.setToolTipText("Enter Serial Number to search for item");
                        scrollPane1.setViewportView(table1);
                    }

                    //---- add ----
                    add.setText("Add Item");
                    add.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            addActionPerformed(e);
                        }
                    });

                    //---- remove ----
                    remove.setText("Remove Item");
                    remove.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            removeActionPerformed(e);
                        }
                    });

                    //---- label3 ----
                    label3.setText("Sub Total");

                    //---- label4 ----
                    label4.setText("Discount");

                    //---- subtotal ----
                    subtotal.setText("0");
                    subtotal.setEditable(false);

                    //---- discount ----
                    discount.setText("0");
                    discount.setToolTipText("Enter discount in Plain amount e.g. 1000 or in Percentage format. e.g. 30%");

                    //---- label5 ----
                    label5.setText("Total");

                    //---- total ----
                    total.setText("0");
                    total.setEditable(false);

                    //---- label8 ----
                    label8.setText("Delivery Charge");

                    GroupLayout panel2Layout = new GroupLayout(panel2);
                    panel2.setLayout(panel2Layout);
                    panel2Layout.setHorizontalGroup(
                        panel2Layout.createParallelGroup()
                            .addComponent(scrollPane1, GroupLayout.Alignment.TRAILING)
                            .addGroup(panel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(panel2Layout.createParallelGroup()
                                    .addGroup(panel2Layout.createSequentialGroup()
                                        .addGap(596, 596, 596)
                                        .addGroup(panel2Layout.createParallelGroup()
                                            .addGroup(GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                                                .addComponent(label5)
                                                .addGap(18, 18, 18)
                                                .addComponent(total, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
                                            .addComponent(separator1, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(panel2Layout.createSequentialGroup()
                                        .addGroup(panel2Layout.createParallelGroup()
                                            .addGroup(panel2Layout.createSequentialGroup()
                                                .addComponent(add, GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(remove, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
                                                .addGap(299, 299, 299))
                                            .addGroup(GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                                                .addComponent(label8)
                                                .addGap(18, 18, 18)))
                                        .addComponent(delivery, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                    .addComponent(label3)
                                    .addComponent(label4))
                                .addGap(18, 18, 18)
                                .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                    .addComponent(subtotal, GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)
                                    .addComponent(discount))
                                .addContainerGap())
                    );
                    panel2Layout.linkSize(SwingConstants.HORIZONTAL, new Component[] {discount, subtotal});
                    panel2Layout.setVerticalGroup(
                        panel2Layout.createParallelGroup()
                            .addGroup(panel2Layout.createSequentialGroup()
                                .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 186, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                    .addComponent(add)
                                    .addComponent(remove))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                                .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                    .addComponent(subtotal, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(label3))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                    .addComponent(discount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(label4))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                    .addComponent(delivery, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(label8))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 5, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panel2Layout.createParallelGroup()
                                    .addComponent(label5)
                                    .addComponent(total, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addGap(3, 3, 3))
                    );
                }

                //======== panel3 ========
                {
                    panel3.setBorder(new TitledBorder("Shipping Info"));

                    //---- label6 ----
                    label6.setText("Name");

                    //---- label7 ----
                    label7.setText("Address");

                    //======== scrollPane2 ========
                    {
                        scrollPane2.setViewportView(shippingAddress);
                    }

                    GroupLayout panel3Layout = new GroupLayout(panel3);
                    panel3.setLayout(panel3Layout);
                    panel3Layout.setHorizontalGroup(
                        panel3Layout.createParallelGroup()
                            .addGroup(panel3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(panel3Layout.createParallelGroup()
                                    .addGroup(panel3Layout.createSequentialGroup()
                                        .addComponent(label6)
                                        .addGap(31, 31, 31)
                                        .addComponent(shippingName))
                                    .addGroup(panel3Layout.createSequentialGroup()
                                        .addComponent(label7)
                                        .addGap(18, 18, 18)
                                        .addComponent(scrollPane2)))
                                .addContainerGap())
                    );
                    panel3Layout.setVerticalGroup(
                        panel3Layout.createParallelGroup()
                            .addGroup(panel3Layout.createSequentialGroup()
                                .addGroup(panel3Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                    .addComponent(label6)
                                    .addComponent(shippingName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panel3Layout.createParallelGroup()
                                    .addComponent(label7)
                                    .addComponent(scrollPane2, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))
                    );
                }

                //---- btnRecord ----
                btnRecord.setText("Record");
                btnRecord.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnRecordActionPerformed(e);
                    }
                });

                //---- btnRecordAndPrint ----
                btnRecordAndPrint.setText("Record & Print");
                btnRecordAndPrint.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnRecordAndPrintActionPerformed(e);
                    }
                });

                //---- cancelBtn ----
                cancelBtn.setText("Cancel");
                cancelBtn.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        cancelBtnActionPerformed(e);
                    }
                });

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPanelLayout.createSequentialGroup()
                            .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 437, GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(panel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addContainerGap())
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(btnRecord, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(btnRecordAndPrint, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cancelBtn, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                            .addGap(12, 12, 12))
                );
                contentPanelLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {btnRecord, btnRecordAndPrint});
                contentPanelLayout.setVerticalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(panel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addGroup(GroupLayout.Alignment.TRAILING, contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                    .addComponent(btnRecordAndPrint)
                                    .addComponent(btnRecord))
                                .addGroup(GroupLayout.Alignment.TRAILING, contentPanelLayout.createSequentialGroup()
                                    .addComponent(cancelBtn)
                                    .addContainerGap())))
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JPanel panel1;
    private JLabel label1;
    private JLabel label2;
    private JComboBox cbCustomer;
    private JButton btnNewCustomer;
    private JDateChooser dateChooser1;
    private JLabel label9;
    private JTextField tfCustomerId;
    private JButton btnSearchCustomerID;
    private JPanel panel2;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JButton add;
    private JButton remove;
    private JLabel label3;
    private JLabel label4;
    private JTextField subtotal;
    private JFormattedTextField discount;
    private JSeparator separator1;
    private JLabel label5;
    private JFormattedTextField total;
    private JFormattedTextField delivery;
    private JLabel label8;
    private JPanel panel3;
    private JLabel label6;
    private JTextField shippingName;
    private JLabel label7;
    private JScrollPane scrollPane2;
    private JTextArea shippingAddress;
    private JButton btnRecord;
    private JButton btnRecordAndPrint;
    private JButton cancelBtn;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
