package ui.action.sell;

import javax.swing.table.DefaultTableModel;

/**
 * Created by Tareq on 5/16/2014.
 */
public class ReturnTableModel extends DefaultTableModel {

    public ReturnTableModel(Object[] columnNames, int rowCount) {
        super(columnNames, rowCount);
    }

    @Override
    public Class getColumnClass(int columnIndex) {
//        return ((Class) dataVector.get(columnIndex)).getClass();

        return String.class;
        //    } else return Integer.class;
    }


    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
