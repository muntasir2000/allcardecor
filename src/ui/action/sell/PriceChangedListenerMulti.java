package ui.action.sell;

/**
 * Created by Muntasir on 4/26/2014.
 */

import core.debug.Debug;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by Muntasir on 4/26/2014.
 */
public class PriceChangedListenerMulti implements TableModelListener {
    private JTextField sub, discount, total, delivery;


    public PriceChangedListenerMulti(JTextField sub, JTextField discount, JTextField total, JTextField delivery) {
        super();
        this.sub = sub;
        this.discount = discount;
        this.total = total;
        this.delivery = delivery;
    }

   /* private int  getDiscount(String s) {
        //TODO ADD NEGATIVE VALUE HANDLING
        String text = discount.getText();
        String subTotal = sub.getText();
        int temp = Integer.parseInt(subTotal);
        Debug.print(temp + "", "temp ");
        Float sub = new Float(temp);
        if (text.contains("%") && text.length() > 1) {
            int pos = text.indexOf('%');
            String text2 = text.substring(0,pos ) + text.substring(pos+1);
            float percentage = Integer.parseInt(text2);
            if (percentage > 0) {
                percentage = percentage / 100;
                Debug.print(percentage + "", "Percentage in PriceChangedListener with %");
                sub = sub * percentage;
                return  sub.intValue();
            }
        } else {
            int percentage = Integer.parseInt(text);

            Debug.print(percentage + "", "Percentage in PriceChangedListener");
            sub = sub - percentage;
            return sub.intValue();
        }


        *//*Float discountNum;
        String discountStr = discount.getText();
        if(discountStr.contains("%") && discountStr.length()>1){
            int pos = discountStr.indexOf('%');
            String trimmedDiscountStr = discountStr.substring(0,pos ) + discountStr.substring(pos+1);
            String subStr = sub.getText();
            int subTotal = Integer.parseInt(subStr);
            discountNum = new Float(Integer.parseInt(discountStr));
        }
*//*
        return  sub.intValue();
    }*/

    public static int getDiscount(String s, int sub) {

        Float discount = new Float(0.0);
        if (s.contains("%")) {
            int percentSignIndex = s.indexOf('%');
            StringBuilder stringBuilder = new StringBuilder(s);
            stringBuilder.deleteCharAt(percentSignIndex);
            try {
                discount = (float) Integer.parseInt(stringBuilder.toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            discount = Math.abs(discount);
            discount = discount / 100;
            discount = discount * sub;
        } else {
            try {
                discount = (float) Integer.parseInt(s);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            /*discount = discount /100;
            discount = discount * sub;*/
        }
        return discount.intValue();
    }

    @Override
    public void tableChanged(TableModelEvent e) {


        int column = e.getColumn();
        TableModel tableModel = (TableModel) e.getSource();
        int lastRow = tableModel.getRowCount();
        String columnName = tableModel.getColumnName(column);
        int sum;

        //  sum = Integer.parseInt(sub.getText());
        sum = 0;
        int deliveryCharge = 0;
        try {
            deliveryCharge = Integer.parseInt(delivery.getText());
        } catch (NumberFormatException er) {
            er.printStackTrace();
        }
        if (columnName.equals("Price")) {
            for (int i = 1; i <= lastRow; i++) {
                sum += Integer.parseInt(tableModel.getValueAt(i - 1, 5).toString());
            }
            //Debug.print(getDiscount(discount.getText()) );
            sub.setText(sum + "");
            int d = getDiscount(discount.getText(), sum);
            int t = sum - d;
            Debug.print(d + "", "PRiceChangedListener getdiscount");
            t += deliveryCharge;
            total.setText(t + "");
        }

        if (e.getType() == TableModelEvent.DELETE) {
            Debug.print("Deletion detected", "XXXXXXXXX");

            for (int i = 1; i <= lastRow; i++) {
                sum += Integer.parseInt(tableModel.getValueAt(i - 1, 5).toString());
            }
            //Debug.print(getDiscount(discount.getText()) );
            sub.setText(sum + "");
            int t = sum - getDiscount(discount.getText(), sum);
            Debug.print(t + "", "PRiceChangedListener t");
            t += deliveryCharge;
            total.setText(t + "");
        }

    }
}

