package ui.action.sell;

import core.debug.Debug;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Created by Tareq on 4/29/2014.
 */
public class DiscountChangedListener implements DocumentListener {
    JTextField subtotal, total, discount, delivery;

    public DiscountChangedListener(JTextField subtotal, JTextField total, JTextField discount, JTextField delivery) {
        this.subtotal = subtotal;
        this.total = total;
        this.discount = discount;
        this.delivery = delivery;
    }

    void handle() {
        //TODO ADD NON NEGATIVE VALUE HANDLING

        Debug.print("In DiscountChangedListener handle()", "DeliveryCharge");
        String text = discount.getText();
        String subTotal = subtotal.getText();
        float sub = Integer.parseInt(subTotal);
        if (sub == 0) {
            return;
        }
        if (text.contains("%") && text.length() > 1) {
            int pos = text.indexOf('%');
            String text2 = text.substring(0, pos) + text.substring(pos + 1);
            //     Debug.print(text2, "after replace in handle()");
            float percentage = 0;
            try {
                percentage = Integer.parseInt(text2);
            } catch (NumberFormatException e) {

            }
            percentage = Math.abs(percentage);
            int deliveryCost = 0;
            try {
                deliveryCost = Integer.parseInt(delivery.getText());
                Debug.print("DeliveryCost = " + deliveryCost, "DeliveryCost");
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            deliveryCost = Math.abs(deliveryCost);
            if (percentage > 0) {
                percentage = percentage / 100;
                Debug.print(percentage + "", "percentage in handle() with %");
                sub = sub - (sub * percentage) + deliveryCost;
                total.setText((int) sub + "");
            } else {
                sub = sub + deliveryCost;
                total.setText((int) sub + "");
            }


            /*if(!delivery.getText().equals("")){
                int totalInt = Integer.parseInt(total.getText());
                int deliveryInt = Integer.parseInt(delivery.getText());
                int adjusted = totalInt + deliveryInt;
                total.setText(adjusted + "");
            }*/
        } else if (!text.contains("%")) {
            float percentage = 0;
            if (!text.equals("")) {
                try {
                    percentage = Integer.parseInt(text);
                } catch (NumberFormatException e) {

                }
            } else {
                percentage = 0;
            }
            percentage = Math.abs(percentage);
            //       Debug.print(percentage + "", "percentage in handle()");
            int deliveryCost = 0;
            try {
                deliveryCost = Integer.parseInt(delivery.getText());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            deliveryCost = Math.abs(deliveryCost);
            sub = sub - percentage + deliveryCost;
            total.setText((int) sub + "");

         /*   if(!delivery.getText().equals("")){
                int totalInt = Integer.parseInt(total.getText());
                int deliveryInt = Integer.parseInt(delivery.getText());
                int adjusted = totalInt + deliveryInt;
                total.setText(adjusted + "");
            }*/
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        handle();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        handle();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        handle();
    }
}
