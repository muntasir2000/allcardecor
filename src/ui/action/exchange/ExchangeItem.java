package ui.action.exchange;

import core.debug.Debug;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

/**
 * @author Tareq Muntasir
 */
public class ExchangeItem extends JDialog {

    private boolean foundWith = false;
    private boolean foundXchange = false;
    private String strNewProductName, strNewDescription;
    private int retailPrice, wholeSalePrice, newWarranty = 0;
    private boolean isRetail = false, isWholesale = false;
    private String strProductName, strCustomer, strDescription, strSellingDate, strInvoiceID;
    private int oldSerialNo, newSerialNo, invoiceID, previousTotal, newTotal;
    private String oldManID, newManID;

    private String strPrice;
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel panel1;
    private JLabel label1;
    private JFormattedTextField serialWith;
    private JLabel label2;
    private JTextField productWith;
    private JLabel label3;
    private JTextField descriptionWith;
    private JLabel label4;
    private JTextField customer;
    private JLabel label5;
    private JTextField price;
    private JLabel label6;
    private JTextField sellingDate;
    private JButton btnSearch;
    private JPanel panel2;
    private JLabel label7;
    private JLabel label8;
    private JTextField newProduct;
    private JLabel label9;
    private JTextField newDescription;
    private JFormattedTextField newSerial;
    private JLabel label11;
    private JTextField newPrice;
    private JButton btnSearchXchange;
    private JLabel label10;
    private JLabel label12;
    private JLabel retailWholesaleLabel;
    private JButton btnCancel;
    private JButton btnRecord;

    public ExchangeItem(Frame owner) {
        super(owner);
        initComponents();
        setupMisc();
    }

    void setupMisc() {
      /*  ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(btnRetail);
        buttonGroup.add(btnWholesale);
        btnRetail.setSelected(true);
        btnRetail.setEnabled(false);
        btnWholesale.setEnabled(false);*/
    }

    boolean searchDB(String strSerial) {
        int serialNo;
        Connection connection = null;
        Statement statement = null;
        try {
            serialNo = Integer.parseInt(strSerial);
        } catch (NumberFormatException e) {
            return false;
        }
        oldSerialNo = serialNo;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");

            statement = connection.createStatement();

            String sql = "SELECT SELL_RECORDS.CUSTOMER, SELL_RECORDS.PRODUCT, SELL_RECORDS.INVOICE_ID, SELL_RECORDS.DATE, SELL_RECORDS.ISRETAIL, SELL_RECORDS.COST, PRODUCTS.DESCRIPTION, SELL_RECORDS.MAN_ID FROM SELL_RECORDS, PRODUCTS WHERE SELL_RECORDS.SERIAL = " + serialNo + " AND PRODUCTS.NAME = SELL_RECORDS.PRODUCT;";
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                strProductName = resultSet.getString("PRODUCT");
                strCustomer = resultSet.getString("CUSTOMER");
                strDescription = resultSet.getString("DESCRIPTION");
                strSellingDate = resultSet.getString("DATE");
                invoiceID = resultSet.getInt("INVOICE_ID");
                strInvoiceID = invoiceID + "";
                strPrice = resultSet.getInt("COST") + "";
                if (resultSet.getString("ISRETAIL").equals("true")) {
                    isRetail = true;
                } else {
                    isRetail = false;
                }
//                isRetail = resultSet.getBoolean("ISRETAIL");//TODO CHECK HERE, NOT OPERATEOR ADDED ARBITRARILY
                Debug.print(resultSet.getString("ISRETAIL") + "", "XXXXXXXXXXXXXXX isRetail: ");
                oldManID = resultSet.getString("MAN_ID");
                foundWith = true;
            } else {
                foundWith = false;
                JOptionPane.showMessageDialog(this, "No records found regarding the entered serial number", "Not found", JOptionPane.ERROR_MESSAGE);
            }
            resultSet.close();

        } catch (SQLException e) {
            foundWith = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return foundWith;
    }

    boolean searchDBXChage(String strSerial) {
        int serialNo;

        Connection connection = null;
        Statement statement = null;
        try {
            serialNo = Integer.parseInt(strSerial);

        } catch (NumberFormatException e) {
            return false;
        }
        newSerialNo = serialNo;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");

            statement = connection.createStatement();

            //String sql = "SELECT SELL_RECORDS.CUSTOMER, SELL_RECORDS.PRODUCT, SELL_RECORDS.INVOICE_ID, SELL_RECORDS.DATE,SELL_RECORDS.COST, PRODUCTS.DESCRIPTION FROM SELL_RECORDS, PRODUCTS WHERE SELL_RECORDS.SERIAL = " + serialNo + " AND PRODUCTS.NAME = SELL_RECORDS.PRODUCT;";
            String sql = "SELECT STOCK_INVENTORY.PRODUCT, STOCK_INVENTORY.SOLD, STOCK_INVENTORY.SERIAL, STOCK_INVENTORY.WARRANTY,  PRODUCTS.NAME, PRODUCTS.DESCRIPTION, PRODUCTS.RETAIL, PRODUCTS.WHOLESALE, STOCK_INVENTORY.MAN_ID FROM STOCK_INVENTORY, PRODUCTS WHERE STOCK_INVENTORY.SERIAL = " + serialNo + " AND STOCK_INVENTORY.PRODUCT = PRODUCTS.NAME AND STOCK_INVENTORY.SOLD = 'false';";

            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                strNewProductName = resultSet.getString("PRODUCT");
                strNewDescription = resultSet.getString("DESCRIPTION");
                retailPrice = resultSet.getInt("RETAIL");
                //        Debug.print(retailPrice, );
                wholeSalePrice = resultSet.getInt("WHOLESALE");
                newWarranty = resultSet.getInt("WARRANTY");
                newManID = resultSet.getString("MAN_ID");
                foundXchange = true;
            } else {
                foundXchange = false;
                JOptionPane.showMessageDialog(this, "No records found regarding the entered serial number", "Not found", JOptionPane.ERROR_MESSAGE);
            }
            resultSet.close();

        } catch (SQLException e) {
            foundXchange = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return foundXchange;
    }

    private void button3ActionPerformed(ActionEvent e) {
        boolean isFound = searchDB(serialWith.getText());
        if (!isFound) {
            JOptionPane.showMessageDialog(this, "Item not found. Check your serial number. You can exchange a product only once", "Not found", JOptionPane.OK_OPTION);
        } else {
            productWith.setText(strProductName);
            descriptionWith.setText(strDescription);
            customer.setText(strCustomer);
            price.setText(strPrice);
            sellingDate.setText(strSellingDate);
        }

    }

    private void btnSearchXchangeActionPerformed(ActionEvent e) {
        boolean isFoundXchange = searchDBXChage(newSerial.getText());
        if (!isFoundXchange) {
            JOptionPane.showMessageDialog(this, "Item not found. Check your serial number", "Not found", JOptionPane.OK_OPTION);

        } else {
            newProduct.setText(strNewProductName);
            newDescription.setText(strNewDescription);
            if (isRetail) {
                retailWholesaleLabel.setText("Retail");
                newPrice.setText(retailPrice + "");
            } else {
                retailWholesaleLabel.setText("Wholesale");
                newPrice.setText(wholeSalePrice + "");
            }
        }
    }

    void saveToDB() {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            String sql = "UPDATE SELL_RECORDS SET  SERIAL = ?, COST = ?, WARRANTY = ?, PRODUCT = ?, MAN_ID = ? WHERE SERIAL = ?;";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, newSerialNo);
            if (isRetail) {
                statement.setInt(2, retailPrice);
            } else {
                statement.setInt(2, wholeSalePrice);
            }
            statement.setInt(3, newWarranty);
            statement.setString(4, newProduct.getText());
            statement.setInt(6, oldSerialNo);
            statement.setString(5, newManID);
            /*
            Date date = new Date(System.currentTimeMillis());
            statement.setDate(6,date); // QA TESTING: NOT TESTED
*/
            statement.executeUpdate();
            statement.close();
            sql = "UPDATE STOCK_INVENTORY SET SOLD = 'false' WHERE SERIAL = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, oldSerialNo);
            statement.executeUpdate();

            statement.close();
            sql = "UPDATE STOCK_INVENTORY SET SOLD = 'true' WHERE SERIAL = ?;";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, newSerialNo);
            statement.executeUpdate();

            statement.close();
            sql = "INSERT INTO EXCHANGED_ITEM (PRODUCT, SERIAL,  EXCHANGED_SERIAL, EXCHANGED_PRODUCT, INVOICE_ID, MAN_ID) VALUES(?, ?, ?, ?, ?,?);";
            statement = connection.prepareStatement(sql);
            statement.setString(1, strNewProductName);
            statement.setInt(2, newSerialNo);
            statement.setInt(3, oldSerialNo);
            statement.setString(4, strProductName);
            statement.setInt(5, invoiceID);
            statement.setString(6, newManID);
            statement.executeUpdate();
            statement.close();

            sql = "SELECT TOTAL FROM SELL_INVOICE WHERE ID = ?;";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, invoiceID);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                previousTotal = rs.getInt("TOTAL");
            }

            newTotal = getAdjustedTotal(previousTotal);

            sql = "UPDATE SELL_INVOICE SET TOTAL = ? WHERE ID = ?;";
            statement = connection.prepareStatement(sql);
            statement.setInt(2, invoiceID);
            statement.setInt(1, newTotal);
            statement.executeUpdate();

            connection.commit();
        } catch (SQLException e) {
            foundXchange = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    int getAdjustedTotal(int previousTotal) {
        //TODO DONE ADJUST SELL_INVOICE TABLE TOTAL

        //TODO DONE TEST FOR CORRECTNESS
        int new__total_price = 0;
        int new_item_price, old_item_price = 0;
        if (isRetail) {
            new_item_price = retailPrice;
        } else {
            new_item_price = wholeSalePrice;
        }

        try {
            new__total_price = Integer.parseInt(newPrice.getText());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        try {
            old_item_price = Integer.parseInt(strPrice);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        previousTotal = previousTotal - old_item_price;
        previousTotal += new_item_price;
        Debug.print(previousTotal + " ", "XX");
        return previousTotal;


    }

    private void btnCancelActionPerformed(ActionEvent e) {
        this.dispose();
    }

    private void btnRecordActionPerformed(ActionEvent e) {
        if (foundWith && foundXchange) {
            saveToDB();
            this.dispose();
        } else {
            JOptionPane.showMessageDialog(this, "Specify item informations correctly before recording.", "Malformed Information", JOptionPane.OK_OPTION);
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        panel1 = new JPanel();
        label1 = new JLabel();
        serialWith = new JFormattedTextField();
        label2 = new JLabel();
        productWith = new JTextField();
        label3 = new JLabel();
        descriptionWith = new JTextField();
        label4 = new JLabel();
        customer = new JTextField();
        label5 = new JLabel();
        price = new JTextField();
        label6 = new JLabel();
        sellingDate = new JTextField();
        btnSearch = new JButton();
        panel2 = new JPanel();
        label7 = new JLabel();
        label8 = new JLabel();
        newProduct = new JTextField();
        label9 = new JLabel();
        newDescription = new JTextField();
        newSerial = new JFormattedTextField();
        label11 = new JLabel();
        newPrice = new JTextField();
        btnSearchXchange = new JButton();
        label10 = new JLabel();
        label12 = new JLabel();
        retailWholesaleLabel = new JLabel();
        btnCancel = new JButton();
        btnRecord = new JButton();

        //======== this ========
        setTitle("Exchange Item");
        setModal(true);
        Container contentPane = getContentPane();

        //======== panel1 ========
        {
            panel1.setBorder(new TitledBorder("Exchange With"));


            //---- label1 ----
            label1.setText("Serial No. ");

            //---- label2 ----
            label2.setText("Product");

            //---- productWith ----
            productWith.setEditable(false);

            //---- label3 ----
            label3.setText("Description");

            //---- descriptionWith ----
            descriptionWith.setEditable(false);

            //---- label4 ----
            label4.setText("Customer");

            //---- customer ----
            customer.setEditable(false);

            //---- label5 ----
            label5.setText("Price");

            //---- price ----
            price.setEditable(false);

            //---- label6 ----
            label6.setText("Selling Date");

            //---- sellingDate ----
            sellingDate.setEditable(false);

            //---- btnSearch ----
            btnSearch.setText("Search");
            btnSearch.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    button3ActionPerformed(e);
                }
            });

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
                    panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addGroup(panel1Layout.createParallelGroup()
                                            .addGroup(panel1Layout.createSequentialGroup()
                                                    .addComponent(label6)
                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(sellingDate))
                                            .addGroup(panel1Layout.createSequentialGroup()
                                                    .addGroup(panel1Layout.createParallelGroup()
                                                            .addComponent(label4)
                                                            .addComponent(label5))
                                                    .addGap(18, 18, 18)
                                                    .addGroup(panel1Layout.createParallelGroup()
                                                            .addComponent(customer)
                                                            .addComponent(price)))
                                            .addGroup(panel1Layout.createSequentialGroup()
                                                    .addGroup(panel1Layout.createParallelGroup()
                                                            .addComponent(label1)
                                                            .addComponent(label2))
                                                    .addGap(18, 18, 18)
                                                    .addGroup(panel1Layout.createParallelGroup()
                                                            .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                                                    .addComponent(serialWith, GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE)
                                                                    .addGap(28, 28, 28)
                                                                    .addComponent(btnSearch, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                                                                    .addGap(30, 30, 30))
                                                            .addComponent(productWith)))
                                            .addGroup(panel1Layout.createSequentialGroup()
                                                    .addComponent(label3)
                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(descriptionWith, GroupLayout.DEFAULT_SIZE, 423, Short.MAX_VALUE)))
                                    .addContainerGap())
            );
            panel1Layout.setVerticalGroup(
                    panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(label1)
                                            .addComponent(btnSearch)
                                            .addComponent(serialWith, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel1Layout.createParallelGroup()
                                            .addComponent(label2)
                                            .addComponent(productWith, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel1Layout.createParallelGroup()
                                            .addComponent(label3)
                                            .addComponent(descriptionWith, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(label4)
                                            .addComponent(customer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(label5)
                                            .addComponent(price, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel1Layout.createParallelGroup()
                                            .addComponent(label6)
                                            .addComponent(sellingDate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
        }

        //======== panel2 ========
        {
            panel2.setBorder(new TitledBorder("New Item"));

            //---- label7 ----
            label7.setText("Serial No.");

            //---- label8 ----
            label8.setText("Product");

            //---- newProduct ----
            newProduct.setEditable(false);

            //---- label9 ----
            label9.setText("Description");

            //---- newDescription ----
            newDescription.setEditable(false);

            //---- label11 ----
            label11.setText("Price");

            //---- newPrice ----
            newPrice.setEditable(false);

            //---- btnSearchXchange ----
            btnSearchXchange.setText("Search");
            btnSearchXchange.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    btnSearchXchangeActionPerformed(e);
                }
            });

            //---- label12 ----
            label12.setText("Sale Type");

            //---- retailWholesaleLabel ----
            retailWholesaleLabel.setText(" text");

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
                    panel2Layout.createParallelGroup()
                            .addGroup(panel2Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addGroup(panel2Layout.createParallelGroup()
                                            .addGroup(panel2Layout.createSequentialGroup()
                                                    .addGroup(panel2Layout.createParallelGroup()
                                                            .addComponent(label7)
                                                            .addComponent(label8))
                                                    .addGap(18, 18, 18)
                                                    .addGroup(panel2Layout.createParallelGroup()
                                                            .addComponent(newProduct, GroupLayout.DEFAULT_SIZE, 427, Short.MAX_VALUE)
                                                            .addGroup(panel2Layout.createSequentialGroup()
                                                                    .addComponent(newSerial, GroupLayout.PREFERRED_SIZE, 286, GroupLayout.PREFERRED_SIZE)
                                                                    .addGap(27, 27, 27)
                                                                    .addComponent(btnSearchXchange, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                                                                    .addGap(0, 30, Short.MAX_VALUE))))
                                            .addGroup(panel2Layout.createSequentialGroup()
                                                    .addComponent(label9)
                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(newDescription))
                                            .addGroup(panel2Layout.createSequentialGroup()
                                                    .addGroup(panel2Layout.createParallelGroup()
                                                            .addComponent(label10)
                                                            .addGroup(panel2Layout.createSequentialGroup()
                                                                    .addComponent(label12)
                                                                    .addGap(18, 18, 18)
                                                                    .addComponent(retailWholesaleLabel, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)))
                                                    .addGap(0, 362, Short.MAX_VALUE))
                                            .addGroup(panel2Layout.createSequentialGroup()
                                                    .addComponent(label11)
                                                    .addGap(40, 40, 40)
                                                    .addComponent(newPrice, GroupLayout.DEFAULT_SIZE, 429, Short.MAX_VALUE)))
                                    .addContainerGap())
            );
            panel2Layout.setVerticalGroup(
                    panel2Layout.createParallelGroup()
                            .addGroup(panel2Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(label7)
                                            .addComponent(newSerial, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(btnSearchXchange))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel2Layout.createParallelGroup()
                                            .addComponent(label8)
                                            .addComponent(newProduct, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel2Layout.createParallelGroup()
                                            .addComponent(label9)
                                            .addComponent(newDescription, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(label10)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(label12)
                                            .addComponent(retailWholesaleLabel))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                            .addComponent(label11)
                                            .addComponent(newPrice, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addGap(20, 20, 20))
            );
        }

        //---- btnCancel ----
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnCancelActionPerformed(e);
            }
        });

        //---- btnRecord ----
        btnRecord.setText("Record");
        btnRecord.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRecordActionPerformed(e);
            }
        });

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                .addContainerGap(349, Short.MAX_VALUE)
                                .addComponent(btnRecord, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                                .addGap(16, 16, 16))
        );
        contentPaneLayout.linkSize(SwingConstants.HORIZONTAL, new Component[]{btnCancel, btnRecord});
        contentPaneLayout.setVerticalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(btnCancel)
                                        .addComponent(btnRecord))
                                .addContainerGap())
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
