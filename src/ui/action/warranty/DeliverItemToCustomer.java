/*
 * Created by JFormDesigner on Thu Jun 05 02:09:47 ALMT 2014
 */

package ui.action.warranty;

import core.debug.Debug;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * @author Tareq Muntasir
 */
public class DeliverItemToCustomer extends JDialog {

    String customer;
    String product;
    String manID;
    String description;
    String defect;
    String receivingDate;
    int serial = 0;
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JLabel label1;
    private JFormattedTextField tfSerial;
    private JButton btnSearch;
    private JLabel label3;
    private JTextField tfCustomer;
    private JLabel label4;
    private JTextField tfProduct;
    private JLabel label5;
    private JScrollPane scrollPane1;
    private JTextArea tfDescription;
    private JLabel label6;
    private JScrollPane scrollPane2;
    private JTextArea tfStatement;
    private JButton btnRecord;
    private JButton btnCancel;
    private JLabel label2;
    private JTextField tfManID;
    private JLabel label8;

    public DeliverItemToCustomer(Dialog owner) {
        super(owner);
        initComponents();

    }

    private boolean search() {
        boolean success = false;
        Connection connection = null;
        Statement statement = null;
        try {
            serial = Integer.parseInt(tfSerial.getText());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            //     connection.setAutoCommit(false);
            String sql = "SELECT * FROM WARRANTY_PROCESSING, PRODUCTS WHERE (SERIAL = " + serial + " OR MAN_ID = '" + tfManID.getText() + "') AND IS_RECEIVED_REPLACEMENT = 'true' AND IS_DELIVERED_TO_CUSTOMER = 'false' AND WARRANTY_PROCESSING.PRODUCT = PRODUCTS.NAME;";
            statement = connection.createStatement();
            Debug.print(sql, "XXXX");
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                customer = resultSet.getString("CUSTOMER");
                product = resultSet.getString("PRODUCT");
                manID = resultSet.getString("MAN_ID");
                description = resultSet.getString("DESCRIPTION");
                defect = resultSet.getString("STATEMENT");
                receivingDate = resultSet.getString("DATE");
                serial = resultSet.getInt("SERIAL");
                success = true;

            } else {
                success = false;
                JOptionPane.showMessageDialog(this, "No warranty claim found with entered Serial no.", "Invalid Serial no.", JOptionPane.OK_OPTION);
            }
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
            success = false;
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return success;
    }

    private void btnSearchActionPerformed(ActionEvent ex) {
        if (search()) {
            tfCustomer.setText(customer);
            tfDescription.setText(description);
            tfManID.setText(manID);
            tfProduct.setText(product);
            tfSerial.setText(serial + "");
            tfStatement.setText(defect);

        } else {
            tfCustomer.setText("");
            tfProduct.setText("");
            tfManID.setText("");
            tfStatement.setText("");
            tfDescription.setText("");
            tfSerial.setText("");

        }
    }

    private void btnRecordActionPerformed(ActionEvent ex) {

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Dhaka"));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        simpleDateFormat.setCalendar(cal);

        String dateStr = simpleDateFormat.format(new Date(System.currentTimeMillis()));

        Connection connection = null;
        Statement statement = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            String sql = "UPDATE WARRANTY_PROCESSING SET IS_DELIVERED_TO_CUSTOMER = 'true', DELIVERY_DATE = '" + dateStr + "' WHERE SERIAL = " + serial + " OR MAN_ID = '" + manID + "';";
            statement = connection.createStatement();

            statement.executeUpdate(sql);
            connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        this.dispose();
    }

    private void btnCancelActionPerformed(ActionEvent e) {
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        label1 = new JLabel();
        tfSerial = new JFormattedTextField();
        btnSearch = new JButton();
        label3 = new JLabel();
        tfCustomer = new JTextField();
        label4 = new JLabel();
        tfProduct = new JTextField();
        label5 = new JLabel();
        scrollPane1 = new JScrollPane();
        tfDescription = new JTextArea();
        label6 = new JLabel();
        scrollPane2 = new JScrollPane();
        tfStatement = new JTextArea();
        btnRecord = new JButton();
        btnCancel = new JButton();
        label2 = new JLabel();
        tfManID = new JTextField();
        label8 = new JLabel();

        //======== this ========
        setTitle("Deliver Item To Customer");
        setModal(true);
        Container contentPane = getContentPane();

        //---- label1 ----
        label1.setText("Enter serial");

        //---- btnSearch ----
        btnSearch.setText("Search");
        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnSearchActionPerformed(e);
            }
        });

        //---- label3 ----
        label3.setText("Customer");

        //---- tfCustomer ----
        tfCustomer.setEditable(false);

        //---- label4 ----
        label4.setText("Product");

        //---- tfProduct ----
        tfProduct.setEditable(false);

        //---- label5 ----
        label5.setText("Description");

        //======== scrollPane1 ========
        {

            //---- tfDescription ----
            tfDescription.setEditable(false);
            scrollPane1.setViewportView(tfDescription);
        }

        //---- label6 ----
        label6.setText("Statement/Defect");

        //======== scrollPane2 ========
        {

            //---- tfStatement ----
            tfStatement.setEditable(false);
            scrollPane2.setViewportView(tfStatement);
        }

        //---- btnRecord ----
        btnRecord.setText("Record");
        btnRecord.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRecordActionPerformed(e);
            }
        });

        //---- btnCancel ----
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnCancelActionPerformed(e);
            }
        });

        //---- label2 ----
        label2.setText("Man. ID");

        //---- label8 ----
        label8.setText("Or");

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                                .addGap(0, 193, Short.MAX_VALUE)
                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                                                .addComponent(label8)
                                                                .addGap(114, 114, 114)
                                                                .addComponent(btnSearch, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                                                                .addGap(20, 20, 20))
                                                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                                                .addComponent(btnRecord, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                                                                .addContainerGap())))
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                                .addComponent(label1)
                                                                .addGap(57, 57, 57)
                                                                .addComponent(tfSerial, GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                                .addComponent(label2)
                                                                .addGap(73, 73, 73)
                                                                .addComponent(tfManID, GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                                        .addComponent(label3)
                                                                        .addComponent(label4))
                                                                .addGap(65, 65, 65)
                                                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                                        .addComponent(tfCustomer, GroupLayout.DEFAULT_SIZE, 285, Short.MAX_VALUE)
                                                                        .addComponent(tfProduct))))
                                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                        .addComponent(label5)
                                                        .addComponent(label6))
                                                .addGap(25, 25, 25)
                                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(scrollPane1)
                                                        .addComponent(scrollPane2, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE))
                                                .addGap(0, 5, Short.MAX_VALUE))))
        );
        contentPaneLayout.linkSize(SwingConstants.HORIZONTAL, new Component[]{btnCancel, btnRecord});
        contentPaneLayout.setVerticalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label1)
                                        .addComponent(tfSerial, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addGap(9, 9, 9)
                                                .addComponent(btnSearch)
                                                .addGap(3, 3, 3))
                                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(label8)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)))
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addComponent(label2, GroupLayout.Alignment.TRAILING)
                                        .addComponent(tfManID, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(tfCustomer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(label3))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(tfProduct, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(label4))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(label5))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addComponent(scrollPane2, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(label6))
                                .addGap(38, 38, 38)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(btnCancel)
                                        .addComponent(btnRecord))
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        contentPaneLayout.linkSize(SwingConstants.VERTICAL, new Component[]{scrollPane1, scrollPane2});
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
