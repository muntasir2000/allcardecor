/*
 * Created by JFormDesigner on Thu Jun 05 12:44:45 ALMT 2014
 */

package ui.action.warranty;

import core.debug.Debug;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.*;
import java.util.Vector;

/**
 * @author Tareq Muntasir
 */
public class ProcessWarrantyDialog extends JDialog {


    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel panel1;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JButton entryClaimedItemBtn;
    private JButton sendToManufacturerBtn;
    private JButton receiveFromManufacturerBtn;
    private JButton deliverItemToCustomerBtn;

    public ProcessWarrantyDialog(Frame owner) {
        super(owner);
        initComponents();
        loadData();
    }

    public ProcessWarrantyDialog(Dialog owner) {
        super(owner);
        initComponents();
    }

    private void loadData() {
        Connection connection = null;
        Statement statement = null;
        ResultSet rs;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");

            String sql = "SELECT CUSTOMER, PRODUCT, SERIAL,MAN_ID, DATE, IS_SENT_TO_MANU, IS_RECEIVED_REPLACEMENT FROM WARRANTY_PROCESSING WHERE IS_DELIVERED_TO_CUSTOMER = 'false';";
            statement = connection.createStatement();
            rs = statement.executeQuery(sql);
            table1.setModel(warrantyClaimRS2TableModel(rs));


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    Debug.print("statement null", "dbmanager");
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Database closing error");
            }
        }
    }

    public TableModel warrantyClaimRS2TableModel(ResultSet rs) {
        try {
            ResultSetMetaData metaData = rs.getMetaData();
            int numberOfColumns = metaData.getColumnCount();
            Vector<String> columnNames = new Vector<String>();

            // Get the column names
            for (int column = 0; column < numberOfColumns; column++) {
                String columnName = metaData.getColumnLabel(column + 1);

                if (columnName.equals("ID")) {
                    columnNames.addElement("ID");
                } else if (columnName.equals("CUSTOMER")) {
                    columnNames.addElement("Customer");
                } else if (columnName.equals("PRODUCT")) {
                    columnNames.addElement("Product");
                } else if (columnName.equals("SERIAL")) {
                    columnNames.addElement("Serial");
                } else if (columnName.equals("DATE")) {
                    columnNames.addElement("Date");
                } else if (columnName.equals("IS_SENT_TO_MANU")) {
                    columnNames.addElement("Sent to Manu.?");
                } else if (columnName.equals("IS_RECEIVED_REPLACEMENT")) {
                    columnNames.addElement("Replaced From Manu.?");
                } else if (columnName.equals("IS_DELIVERED_TO_CUSTOMER")) {
                    columnNames.addElement("Delivered To Customer?");
                } else if (columnName.equals("STATEMENT")) {
                    columnNames.addElement("Statement");
                } else if (columnName.equals("DELIVERY_DATE")) {
                    columnNames.addElement("Delivery Date");
                } else {
                    columnNames.addElement(columnName);
                }


            }

            // Get all rows.
            Vector<Vector<Object>> rows = new Vector<Vector<Object>>();

            while (rs.next()) {
                Vector<Object> newRow = new Vector<Object>();

                for (int i = 1; i <= numberOfColumns; i++) {
                    newRow.addElement(rs.getObject(i));
                }

                rows.addElement(newRow);
            }

            return new DefaultTableModel(rows, columnNames) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    private void entryClaimedItemBtnActionPerformed(ActionEvent e) {
        EntryClaimedProductDialog entryClaimedProductDialog = new EntryClaimedProductDialog(this);
        entryClaimedProductDialog.setVisible(true);
    }

    private void sendToManufacturerBtnActionPerformed(ActionEvent e) {
        SendToManufacturerDialog sendToManufacturerDialog = new SendToManufacturerDialog(this);
        sendToManufacturerDialog.setVisible(true);
    }

    private void receiveFromManufacturerBtnActionPerformed(ActionEvent e) {
        ReceiveFromManufacturerDialog receiveFromManufacturerDialog = new ReceiveFromManufacturerDialog(this);
        receiveFromManufacturerDialog.setVisible(true);
    }

    private void deliverItemToCustomerBtnActionPerformed(ActionEvent e) {
        DeliverItemToCustomer deliverItemToCustomer = new DeliverItemToCustomer(this);
        deliverItemToCustomer.setVisible(true);
    }

    private void thisWindowGainedFocus(WindowEvent e) {
        loadData();
    }

    private void initComponents() {

        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        panel1 = new JPanel();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();
        String htmlButton = "<html><center>" + "Entry Claimed Item " + "<br>" + "from Customer" + "</center></html>";
        entryClaimedItemBtn = new JButton(htmlButton);
        sendToManufacturerBtn = new JButton();
        receiveFromManufacturerBtn = new JButton();
        deliverItemToCustomerBtn = new JButton();

        //======== this ========
        setTitle("Process Warranty Claims");
        setModal(true);
        addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowGainedFocus(WindowEvent e) {
                thisWindowGainedFocus(e);
            }
        });
        Container contentPane = getContentPane();

        //======== panel1 ========
        {
            panel1.setBorder(new TitledBorder("Pending Warranty Claims"));

            // JFormDesigner evaluation mark

            //======== scrollPane1 ========
            {
                scrollPane1.setViewportView(table1);
            }

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
                    panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 760, Short.MAX_VALUE)
                                    .addContainerGap())
            );
            panel1Layout.setVerticalGroup(
                    panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 291, Short.MAX_VALUE)
                                    .addContainerGap())
            );
        }

        //---- entryClaimedItemBtn ----
        entryClaimedItemBtn.setText("Entry Claimed Item");
        entryClaimedItemBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                entryClaimedItemBtnActionPerformed(e);
            }
        });

        //---- sendToManufacturerBtn ----
        sendToManufacturerBtn.setText("Send To Manufacturer");
        sendToManufacturerBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendToManufacturerBtnActionPerformed(e);
            }
        });

        //---- receiveFromManufacturerBtn ----
        receiveFromManufacturerBtn.setText("Receive From Manufacturer");
        receiveFromManufacturerBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                receiveFromManufacturerBtnActionPerformed(e);
            }
        });

        //---- deliverItemToCustomerBtn ----
        deliverItemToCustomerBtn.setText("Deliver Item to Customer");
        deliverItemToCustomerBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deliverItemToCustomerBtnActionPerformed(e);
            }
        });

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addGap(6, 6, 6)
                                                .addComponent(entryClaimedItemBtn, GroupLayout.PREFERRED_SIZE, 168, GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(sendToManufacturerBtn, GroupLayout.PREFERRED_SIZE, 168, GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(receiveFromManufacturerBtn, GroupLayout.PREFERRED_SIZE, 168, GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(deliverItemToCustomerBtn, GroupLayout.PREFERRED_SIZE, 168, GroupLayout.PREFERRED_SIZE)
                                                .addContainerGap(56, Short.MAX_VALUE))
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addContainerGap())))
        );
        contentPaneLayout.setVerticalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(entryClaimedItemBtn, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(sendToManufacturerBtn, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(receiveFromManufacturerBtn, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(deliverItemToCustomerBtn, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(52, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents


    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
