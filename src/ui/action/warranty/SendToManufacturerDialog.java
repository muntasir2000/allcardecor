/*
 * Created by JFormDesigner on Thu Jun 05 17:35:39 ALMT 2014
 */

package ui.action.warranty;

import core.debug.Debug;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

/**
 * @author Tareq Muntasir
 */
public class SendToManufacturerDialog extends JDialog {


    String customer;
    String product;
    String manID;
    String description;
    String defect;
    String receivingDate;
    int serial = 0;
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JLabel label1;
    private JFormattedTextField tfSerial;
    private JButton btnSearch;
    private JLabel label2;
    private JLabel label3;
    private JTextField tfCustomer;
    private JLabel label4;
    private JTextField tfProduct;
    private JLabel label5;
    private JScrollPane scrollPane1;
    private JTextArea tfDescription;
    private JLabel label6;
    private JScrollPane scrollPane2;
    private JTextArea tfStatement;
    private JButton btnRecord;
    private JButton btnCancel;
    private JTextField tfDate;
    private JLabel label7;
    private JTextField tfManuID;
    private JLabel label8;

    public SendToManufacturerDialog(Frame owner) {
        super(owner);
        initComponents();
    }

    public SendToManufacturerDialog(Dialog owner) {
        super(owner);
        initComponents();
    }

    private boolean search() {
        boolean success = false;
        Connection connection = null;
        Statement statement = null;
        try {
            serial = Integer.parseInt(tfSerial.getText());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");

            String sql = "SELECT * FROM WARRANTY_PROCESSING, PRODUCTS WHERE ( SERIAL = " + serial + " OR MAN_ID = '" + tfManuID.getText() + "') AND IS_SENT_TO_MANU = 'false' AND WARRANTY_PROCESSING.PRODUCT = PRODUCTS.NAME;";
            statement = connection.createStatement();
            Debug.print(sql, "SQL  ");
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                customer = resultSet.getString("CUSTOMER");
                product = resultSet.getString("PRODUCT");
                manID = resultSet.getString("MAN_ID");
                description = resultSet.getString("DESCRIPTION");
                defect = resultSet.getString("STATEMENT");
                receivingDate = resultSet.getString("DATE");
                serial = resultSet.getInt("SERIAL");
                success = true;

            } else {
                success = false;
                JOptionPane.showMessageDialog(this, "No warranty claim found with entered Serial no.", "Invalid Serial no.", JOptionPane.OK_OPTION);
            }
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
            success = false;
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return success;
    }

    private void btnRecordActionPerformed(ActionEvent ex) {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            String sql = "UPDATE WARRANTY_PROCESSING SET IS_SENT_TO_MANU = 'true' WHERE SERIAL = " + serial + ";";
            statement = connection.createStatement();

            statement.executeUpdate(sql);
            connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        this.dispose();
    }

    private void btnSearchActionPerformed(ActionEvent e) {
        if (search()) {
            tfCustomer.setText(customer);
            tfDescription.setText(description);
            tfManuID.setText(manID);
            tfProduct.setText(product);
            tfSerial.setText(serial + "");
            tfStatement.setText(defect);
            tfDate.setText(receivingDate);
            tfSerial.setText(serial + "");
        } else {
            tfCustomer.setText("");
            tfProduct.setText("");
            tfManuID.setText("");
            tfStatement.setText("");
            tfDescription.setText("");
            tfSerial.setText("");
            tfDate.setText("");
            tfSerial.setText("");
        }
    }

    private void btnCancelActionPerformed(ActionEvent e) {
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        label1 = new JLabel();
        tfSerial = new JFormattedTextField();
        btnSearch = new JButton();
        label2 = new JLabel();
        label3 = new JLabel();
        tfCustomer = new JTextField();
        label4 = new JLabel();
        tfProduct = new JTextField();
        label5 = new JLabel();
        scrollPane1 = new JScrollPane();
        tfDescription = new JTextArea();
        label6 = new JLabel();
        scrollPane2 = new JScrollPane();
        tfStatement = new JTextArea();
        btnRecord = new JButton();
        btnCancel = new JButton();
        tfDate = new JTextField();
        label7 = new JLabel();
        tfManuID = new JTextField();
        label8 = new JLabel();

        //======== this ========
        setTitle("Send Item to Manufacturer ");
        setModal(true);
        Container contentPane = getContentPane();

        //---- label1 ----
        label1.setText("Enter serial");

        //---- btnSearch ----
        btnSearch.setText("Search");
        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnSearchActionPerformed(e);
            }
        });

        //---- label2 ----
        label2.setText("Receiving Date");

        //---- label3 ----
        label3.setText("Customer");

        //---- tfCustomer ----
        tfCustomer.setEditable(false);

        //---- label4 ----
        label4.setText("Product");

        //---- tfProduct ----
        tfProduct.setEditable(false);

        //---- label5 ----
        label5.setText("Description");

        //======== scrollPane1 ========
        {

            //---- tfDescription ----
            tfDescription.setEditable(false);
            scrollPane1.setViewportView(tfDescription);
        }

        //---- label6 ----
        label6.setText("Statement/Defect");

        //======== scrollPane2 ========
        {

            //---- tfStatement ----
            tfStatement.setEditable(false);
            scrollPane2.setViewportView(tfStatement);
        }

        //---- btnRecord ----
        btnRecord.setText("Record");
        btnRecord.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRecordActionPerformed(e);
            }
        });

        //---- btnCancel ----
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnCancelActionPerformed(e);
            }
        });

        //---- tfDate ----
        tfDate.setEditable(false);

        //---- label7 ----
        label7.setText("Manufacturer ID");

        //---- label8 ----
        label8.setText("Or");

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                        .addComponent(label6)
                                                        .addComponent(label5))
                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 139, Short.MAX_VALUE)
                                                                .addComponent(btnRecord, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                                .addGap(25, 25, 25)
                                                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                                        .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 285, Short.MAX_VALUE)
                                                                        .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 285, Short.MAX_VALUE))
                                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 5, Short.MAX_VALUE))))
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                                .addComponent(label1)
                                                                .addGap(57, 57, 57)
                                                                .addComponent(tfSerial, GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                                        .addComponent(label2)
                                                                        .addComponent(label3)
                                                                        .addComponent(label4)
                                                                        .addComponent(label7))
                                                                .addGap(32, 32, 32)
                                                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                                        .addComponent(tfDate, GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(tfCustomer)
                                                                        .addComponent(tfProduct, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(tfManuID, GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE))))
                                                .addGap(0, 5, Short.MAX_VALUE)))
                                .addContainerGap())
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addGap(197, 197, 197)
                                .addComponent(label8)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 111, Short.MAX_VALUE)
                                .addComponent(btnSearch, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                                .addGap(25, 25, 25))
        );
        contentPaneLayout.linkSize(SwingConstants.HORIZONTAL, new Component[]{btnCancel, btnRecord});
        contentPaneLayout.setVerticalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label1)
                                        .addComponent(tfSerial, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label8)
                                        .addComponent(btnSearch))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label7)
                                        .addComponent(tfManuID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label2)
                                        .addComponent(tfDate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label3)
                                        .addComponent(tfCustomer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label4)
                                        .addComponent(tfProduct, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addComponent(label5)
                                                .addGap(45, 45, 45)
                                                .addComponent(label6))
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(scrollPane2, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                        .addComponent(btnCancel)
                                                        .addComponent(btnRecord))))
                                .addContainerGap())
        );
        contentPaneLayout.linkSize(SwingConstants.VERTICAL, new Component[]{scrollPane1, scrollPane2});
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
