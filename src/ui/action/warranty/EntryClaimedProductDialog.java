/*
 * Created by JFormDesigner on Thu Jun 05 21:46:53 ALMT 2014
 */

package ui.action.warranty;

import com.toedter.calendar.JDateChooser;
import core.debug.Debug;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.text.NumberFormat;
import java.util.Date;

/**
 * @author Tareq Muntasir
 */
public class EntryClaimedProductDialog extends JDialog {
    boolean isWarrantyVaild = false;
    int serial = 0;
    String product = null, customer = null, description = null;
    int invoiceID = 0;

    NumberFormat numberFormatForTextFields = NumberFormat.getNumberInstance();
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JLabel label1;
    private JFormattedTextField tfSerial;
    private JButton btnCheck;
    private JLabel label2;
    private JDateChooser todayDateChooser;
    private JLabel label3;
    private JScrollPane scrollPane1;
    private JTextPane textPaneStatement;
    private JLabel label4;
    private JTextField tfCustomer;
    private JLabel label5;
    private JTextField tfProduct;
    private JLabel label6;
    private JScrollPane scrollPane2;
    private JTextArea tfDescription;
    private JLabel label7;
    private JTextField tfInvoiceID;
    private JTextField statusLabel;
    private JButton btnCancel;
    private JButton btnRecord;
    private JLabel label8;
    private JTextField tfManID;

    public EntryClaimedProductDialog(Frame owner) {
        super(owner);
        initComponents();
    }

    public EntryClaimedProductDialog(Dialog owner) {
        super(owner);
        initComponents();
        todayDateChooser.setDate(new Date());
        statusLabel.setHorizontalAlignment(SwingConstants.CENTER);

    }

    private void btnCheckActionPerformed(ActionEvent ex) {
        if (!isWarrantyValid()) {
            clearFields();
        }

    }

    boolean isWarrantyValid() {

        try {
            serial = Integer.parseInt(tfSerial.getText().replaceAll(",", ""));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        java.util.Date utilTodayDate = todayDateChooser.getDate();
        if (utilTodayDate == null) {
            utilTodayDate = new java.util.Date();
        }
      /*  Date todayDate = new Date(utilTodayDate.getTime());
        Date sellingDate = null;
*/
        DateTime sellingDate = null;

        Connection connection = null;
        Statement statement = null;
        ResultSet rs;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");

            //      String sql = "SELECT SERIAL FROM "

            String sql = "SELECT * FROM EXCHANGED_ITEM WHERE SERIAL = " + serial + "; ";
            statement = connection.createStatement();
            rs = statement.executeQuery(sql);

            if (rs.next()) {
                sellingDate = parseDateString(rs.getString("DATE"));
            }
            rs.close();
            int warrantyDays = 0;
            sql = "SELECT * FROM SELL_RECORDS, PRODUCTS WHERE SELL_RECORDS.PRODUCT = PRODUCTS.NAME AND SELL_RECORDS.SERIAL = " + serial + ";";
            rs = statement.executeQuery(sql);
            if (rs.next()) {
                if (sellingDate == null) {
                    Debug.print(rs.getString("DATE").toString(), "XX");
                    sellingDate = parseDateString(rs.getString("DATE"));
                }
                serial = rs.getInt("SERIAL");
                warrantyDays = rs.getInt("WARRANTY");
                Debug.print(warrantyDays + "", "XX");
                product = rs.getString("PRODUCT");
                customer = rs.getString("CUSTOMER");
                invoiceID = rs.getInt("INVOICE_ID");
                description = rs.getString("DESCRIPTION");
                isWarrantyVaild = true;
            } else {
                JOptionPane.showMessageDialog(this, "Serial Not found. Check serial no.", "Error", JOptionPane.INFORMATION_MESSAGE);
                isWarrantyVaild = false;

            }

            DateTime jodaDateTimeToday = new DateTime(utilTodayDate);

            if (!sellingDate.isBefore(jodaDateTimeToday)) {
                JOptionPane.showMessageDialog(this, "Invalid date. Today is before selling date. Check Today date", "Not allowed", JOptionPane.ERROR_MESSAGE);
                return false;
            }

            int dayPassed = Days.daysBetween(sellingDate.toLocalDate(), jodaDateTimeToday.toLocalDate()).getDays();

            if (warrantyDays == 0) {
                JOptionPane.showMessageDialog(this, "This product has no warranty", "Error", JOptionPane.INFORMATION_MESSAGE);
                isWarrantyVaild = false;
                statusLabel.setText("NO WARRANTY");
                statusLabel.setForeground(Color.red);
            } else if (dayPassed > warrantyDays) {
                JOptionPane.showMessageDialog(this, "Warranty period expired. " + Math.abs(dayPassed - warrantyDays) + " days ago.", "Error", JOptionPane.INFORMATION_MESSAGE);
                isWarrantyVaild = false;
                statusLabel.setText("WARRANTY INVALID");
                statusLabel.setForeground(Color.red);

            } else {
                statusLabel.setText("WARRANTY VALID. Remaining " + (warrantyDays - Math.abs(dayPassed)) + " Days.");
                tfDescription.setText(description);
                tfInvoiceID.setText(invoiceID + "");
                tfCustomer.setText(customer);
                tfProduct.setText(product);
                tfSerial.setText(serial + "");
                statusLabel.setForeground(new Color(0, 100, 0));

            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    Debug.print("statement null", "dbmanager");
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                // connection close failed.
                //JOptionPane.showMessageDialog(, "Database closing error. Please contact the developers.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Database closing error");
            }
        }
        return isWarrantyVaild;
    }

    void clearFields() {
        //   tfSerial.setText("");
        tfProduct.setText("");
        tfCustomer.setText("");
        tfInvoiceID.setText("");
        tfDescription.setText("");
        isWarrantyVaild = false;
    }

    DateTime parseDateString(String str) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        return formatter.parseDateTime(str);
    }

    private void btnRecordActionPerformed(ActionEvent e) {
        saveToDB();
        dispose();
    }

    private void saveToDB() {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            String sql = "INSERT INTO WARRANTY_PROCESSING (CUSTOMER, PRODUCT, SERIAL, IS_SENT_TO_MANU, STATEMENT, MAN_ID) VALUES(?,?,?,?,?,?);";
            statement = connection.prepareStatement(sql);
            statement.setString(1, customer);
            statement.setString(2, product);
            statement.setInt(3, serial);
            statement.setString(4, "false");
            statement.setString(5, textPaneStatement.getText());
            statement.setString(6, tfManID.getText());
            statement.executeUpdate();
            connection.commit();
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void btnCancelActionPerformed(ActionEvent e) {
        this.dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        label1 = new JLabel();
        tfSerial = new JFormattedTextField(numberFormatForTextFields);
        btnCheck = new JButton();
        label2 = new JLabel();
        todayDateChooser = new JDateChooser();
        label3 = new JLabel();
        scrollPane1 = new JScrollPane();
        textPaneStatement = new JTextPane();
        label4 = new JLabel();
        tfCustomer = new JTextField();
        label5 = new JLabel();
        tfProduct = new JTextField();
        label6 = new JLabel();
        scrollPane2 = new JScrollPane();
        tfDescription = new JTextArea();
        label7 = new JLabel();
        tfInvoiceID = new JTextField();
        statusLabel = new JTextField();
        btnCancel = new JButton();
        btnRecord = new JButton();
        label8 = new JLabel();
        tfManID = new JTextField();

        //======== this ========
        setTitle("Entry Claimed Item");
        setModal(true);
        Container contentPane = getContentPane();

        //---- label1 ----
        label1.setText("Enter item Serial to check for warranty status");

        //---- btnCheck ----
        btnCheck.setText("Check");
        btnCheck.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnCheckActionPerformed(e);
            }
        });

        //---- label2 ----
        label2.setText("Today's Date");

        //---- label3 ----
        label3.setText("Describe te defects of the Item");

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(textPaneStatement);
        }

        //---- label4 ----
        label4.setText("Customer");

        //---- label5 ----
        label5.setText("Product");

        //---- label6 ----
        label6.setText("Description");

        //======== scrollPane2 ========
        {
            scrollPane2.setViewportView(tfDescription);
        }

        //---- label7 ----
        label7.setText("Invoice ID");

        //---- statusLabel ----
        statusLabel.setEditable(false);
        statusLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));

        //---- btnCancel ----
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnCancelActionPerformed(e);
            }
        });

        //---- btnRecord ----
        btnRecord.setText("Record");
        btnRecord.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRecordActionPerformed(e);
            }
        });

        //---- label8 ----
        label8.setText("Or  Man. ID");

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                                        .addComponent(label4)
                                                                        .addComponent(label5)
                                                                        .addComponent(label6))
                                                                .addGap(122, 122, 122)
                                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                                        .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE)
                                                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                                                                        .addComponent(tfProduct, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)
                                                                                        .addComponent(tfCustomer, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE))
                                                                                .addGap(0, 221, Short.MAX_VALUE))))
                                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                                .addComponent(label3)
                                                                .addGap(25, 25, 25)
                                                                .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 431, Short.MAX_VALUE))
                                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                                                .addComponent(label7)
                                                                                .addGap(126, 126, 126)
                                                                                .addComponent(tfInvoiceID, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE))
                                                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                                                        .addComponent(label1)
                                                                                        .addComponent(label2))
                                                                                .addGap(18, 18, 18)
                                                                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                                                        .addComponent(tfSerial, GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
                                                                                        .addComponent(todayDateChooser, GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE))
                                                                                .addGap(18, 18, 18)
                                                                                .addComponent(label8)
                                                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                                .addComponent(tfManID, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)))
                                                                .addGap(0, 0, Short.MAX_VALUE)))
                                                .addContainerGap())
                                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                                .addGap(0, 142, Short.MAX_VALUE)
                                                .addGroup(contentPaneLayout.createParallelGroup()
                                                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                                                .addComponent(btnCheck)
                                                                .addGap(275, 275, 275))
                                                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                                                .addComponent(statusLabel, GroupLayout.PREFERRED_SIZE, 356, GroupLayout.PREFERRED_SIZE)
                                                                .addGap(124, 124, 124))))))
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                .addContainerGap(422, Short.MAX_VALUE)
                                .addComponent(btnRecord, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
                                .addGap(31, 31, 31)
                                .addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18))
        );
        contentPaneLayout.setVerticalGroup(
                contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label1)
                                        .addComponent(tfSerial, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(label8)
                                        .addComponent(tfManID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                        .addComponent(todayDateChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(label2))
                                .addGap(5, 5, 5)
                                .addComponent(btnCheck)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(statusLabel, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label4)
                                        .addComponent(tfCustomer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label5)
                                        .addComponent(tfProduct, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label6)
                                        .addComponent(scrollPane2, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label7)
                                        .addComponent(tfInvoiceID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(contentPaneLayout.createParallelGroup()
                                        .addComponent(label3)
                                        .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(btnCancel)
                                        .addComponent(btnRecord))
                                .addContainerGap())
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
