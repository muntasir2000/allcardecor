package ui.action.stock;

import core.debug.Debug;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by Muntasir on 4/26/2014.
 */
public class PriceChangedListener implements TableModelListener {
    private JLabel jLabel;
    private JTextField jTextField;

    public PriceChangedListener(JLabel label) {
        super();
        jLabel = label;
    }

    public PriceChangedListener(JTextField textField) {
        super();
        jTextField = textField;

    }

    @Override
    public void tableChanged(TableModelEvent e) {
        int firstRow = e.getFirstRow();
        int lastRow = e.getLastRow();
        int column = e.getColumn();
        TableModel tableModel = (TableModel) e.getSource();
        lastRow = tableModel.getRowCount();
        Debug.print(lastRow + "", "last row count");
        String columnName = tableModel.getColumnName(column);
        int sum = 0;

        if (columnName.equals("Cost")) {
            for (int i = 0; i < lastRow; i++) {
                Object str = tableModel.getValueAt(i, 4);
                if (str != null)
                    sum += Integer.parseInt(str.toString());
            }
            Debug.print(sum + "", "Price in listener");
            if (jLabel != null) {
                jLabel.setText(sum + "");

            } else {
                jTextField.setText(sum + "");
            }
        }


    }
}
