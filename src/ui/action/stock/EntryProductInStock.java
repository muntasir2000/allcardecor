/*
 * Created by JFormDesigner on Wed Apr 23 15:16:34 ALMT 2014
 */

package ui.action.stock;

import com.toedter.calendar.JDateChooser;
import core.debug.Debug;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import ui.init.AddNewSupplier;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Tareq  Al Muntasir
 */
public class EntryProductInStock extends JDialog {
    ArrayList<String> productList;
    JComboBox comboBox;
    ArrayList<String> supplierList;
    StockTableModel tableModel;
    String dateStr;
    private long invoiceID;
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Tareq Muntasir
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JPanel panel1;
    private JLabel label1;
    private JComboBox cbSupplier;
    private JButton btnNewSupplier;
    private JLabel label2;
    private JDateChooser dateChooser1;
    private JPanel panel2;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JButton btnAdd;
    private JButton btnRemove;
    private JLabel label3;
    private JLabel labelTotal;
    private JButton btnRecord;
    private JButton btnCancel;

    public EntryProductInStock(Frame owner) {

        super(owner);
        comboBox = new JComboBox();
        initComponents();
        loadSuppliersProducts();
        tableModel.addTableModelListener(new PriceChangedListener(labelTotal));
        dateChooser1.setDate(new Date());
    }

    private void loadSuppliersProducts() {
        supplierList = new ArrayList<String>();
        productList = new ArrayList<String>();

        Connection connection = null;
        java.sql.Statement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");

            statement = connection.createStatement();

            String sql = "SELECT * FROM SUPPLIER;";
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                String temp = resultSet.getString("NAME");
                supplierList.add(temp);
            }
            resultSet.close();
            sql = "SELECT * FROM PRODUCTS;";
            ResultSet rs2 = statement.executeQuery(sql);
            while (resultSet.next()) {
                productList.add(resultSet.getString("NAME"));
            }

            //ADD ARRAYLIST ELEMENTS TO RESPECTIVE CONTAINERS
            for (String s : supplierList) {
                cbSupplier.addItem(s);
            }
            for (String ss : productList) {
                comboBox.addItem(ss);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    private void saveToDB() {

        Connection connection = null;
        Statement statement = null;
        String supplier = cbSupplier.getSelectedItem().toString();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        java.util.Date datePicked = dateChooser1.getDate();

        dateStr = simpleDateFormat.format(datePicked);
        int total = Integer.parseInt(labelTotal.getText());
        int count = tableModel.getRowCount();

        boolean invoiceIDError = false;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            statement = connection.createStatement();

            String sql = "INSERT INTO STOCK_INVOICE (SUPPLIER, DATE, COUNT, TOTAL) VALUES( '" + supplier + "', " + " '" + dateStr + "', " + count + ", " + total + ");";
            statement.executeUpdate(sql);

            ResultSet rs = statement.executeQuery("SELECT last_insert_rowid();");
            if (rs.next()) {
                invoiceID = rs.getLong(1);
                Debug.print(invoiceID + " ", "rowid");
            } else {
                //raise a high level error. Because if invoice ID cannot be obtained, products will not be added to stock inventory.
                JOptionPane.showMessageDialog(this, "Critical Error occurred. Aborting operation.", "Error!", JOptionPane.ERROR_MESSAGE);
                this.dispose();
                invoiceIDError = true;
            }
            if (!invoiceIDError) {
                ArrayList<ItemRowStockTable> items = fetchDataFromTable();
                for (ItemRowStockTable item : items) {
                    statement.executeUpdate(item.generateInsertSQLforStockEntry());
                }
            }
            connection.commit();
            rs.close();

            statement.close();

        } catch (SQLException e) {
            if (e.getMessage().contains("unique")) {
                JOptionPane.showMessageDialog(this, "Cannot entry items. Serial already exists in database. Try again.", "Critical Error", JOptionPane.ERROR_MESSAGE);
            }
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    private ArrayList<ItemRowStockTable> fetchDataFromTable() {
        int row = tableModel.getRowCount();
        ArrayList<ItemRowStockTable> list = new ArrayList<ItemRowStockTable>();
        for (int i = 0; i < row; i++) {
            String product = tableModel.getValueAt(i, 0).toString();
            int serial = Integer.parseInt(tableModel.getValueAt(i, 1).toString());
            Object manIDObj = tableModel.getValueAt(i, 2);
            String manID;
            if (manIDObj == null) {
                manID = "";
            } else {
                manID = manIDObj.toString();
            }
            int price = Integer.parseInt(tableModel.getValueAt(i, 4).toString());
            int warranty = Integer.parseInt(tableModel.getValueAt(i, 3).toString());

            ItemRowStockTable item = new ItemRowStockTable(product, serial, invoiceID, price, warranty, dateStr, manID);
            list.add(item);
        }
        return list;
    }

    private boolean validateRows() {
        Debug.print(tableModel.getRowCount() + "   " + tableModel.getColumnCount(), "row column count");
        //iterate through the last row and validate input
        for (int i = tableModel.getRowCount(); i >= 1; i--) {
            for (int j = tableModel.getColumnCount(); j >= 1; j--) {
                if (j == 3) continue;
                if (tableModel.getValueAt(i - 1, j - 1) == null) {
                    JOptionPane.showMessageDialog(table1, "Field empty. Insert proper value.");
                    return false;
                }
            }
        }
        return true;

    }

    private void btnAddActionPerformed(ActionEvent e) {
        if (!validateRows()) {
            return;
        }

        tableModel.addRow(new Object[]{null, null, null, 0, null});
        tableModel.fireTableDataChanged();

    }

    private void btnRemoveActionPerformed(ActionEvent e) {
        DefaultTableModel model = (DefaultTableModel) this.table1.getModel();
        int[] rows = table1.getSelectedRows();
        for (int i = 0; i < rows.length; i++) {
            model.removeRow(rows[i] - i);
        }
    }

    private void btnNewSupplierActionPerformed(ActionEvent e) {
        AddNewSupplier newSupplier = new AddNewSupplier(this, cbSupplier);
        newSupplier.setVisible(true);
    }

    private void btnRecordActionPerformed(ActionEvent e) {
        if (!validateRows()) {
            return;
        }
        saveToDB();
        dispose();
    }

    private void btnCancelActionPerformed(ActionEvent e) {
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Tareq Muntasir
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        panel1 = new JPanel();
        label1 = new JLabel();
        cbSupplier = new JComboBox();
        AutoCompleteDecorator.decorate(cbSupplier);
        btnNewSupplier = new JButton();
        label2 = new JLabel();
        dateChooser1 = new JDateChooser();
        panel2 = new JPanel();
        scrollPane1 = new JScrollPane();
        Object[] columnNames = {"Item Name", "Serial No.", "Man. ID", "Warranty (days)", "Cost"};
        tableModel = new StockTableModel(columnNames, 1);
        table1 = new JTable(tableModel);
        AutoCompleteDecorator.decorate(comboBox);
        int vColIndex = 0;
        TableColumn col = table1.getColumnModel().getColumn(vColIndex);
        col.setCellEditor(new DefaultCellEditor(comboBox));
        btnAdd = new JButton();
        btnRemove = new JButton();
        label3 = new JLabel();
        labelTotal = new JLabel();
        btnRecord = new JButton();
        btnCancel = new JButton();

        //======== this ========
        setTitle("Entry Products in Stock");
        setModal(true);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));

            // JFormDesigner evaluation mark

            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {

                //======== panel1 ========
                {
                    panel1.setBorder(new TitledBorder("Supplier Info"));

                    //---- label1 ----
                    label1.setText("Supplier Name");

                    //---- btnNewSupplier ----
                    btnNewSupplier.setText("New Supplier");
                    btnNewSupplier.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnNewSupplierActionPerformed(e);
                        }
                    });

                    //---- label2 ----
                    label2.setText("Date");

                    GroupLayout panel1Layout = new GroupLayout(panel1);
                    panel1.setLayout(panel1Layout);
                    panel1Layout.setHorizontalGroup(
                            panel1Layout.createParallelGroup()
                                    .addGroup(panel1Layout.createSequentialGroup()
                                            .addContainerGap()
                                            .addGroup(panel1Layout.createParallelGroup()
                                                    .addComponent(label1)
                                                    .addComponent(label2))
                                            .addGap(18, 18, 18)
                                            .addGroup(panel1Layout.createParallelGroup()
                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                            .addComponent(cbSupplier, GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE)
                                                            .addGap(29, 29, 29)
                                                            .addComponent(btnNewSupplier)
                                                            .addGap(24, 24, 24))
                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                            .addComponent(dateChooser1, GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE)
                                                            .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    );
                    panel1Layout.setVerticalGroup(
                            panel1Layout.createParallelGroup()
                                    .addGroup(panel1Layout.createSequentialGroup()
                                            .addGap(10, 10, 10)
                                            .addComponent(label1)
                                            .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(panel1Layout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addGroup(panel1Layout.createParallelGroup()
                                                    .addGroup(panel1Layout.createSequentialGroup()
                                                            .addComponent(cbSupplier, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                            .addGroup(panel1Layout.createParallelGroup()
                                                                    .addComponent(label2)
                                                                    .addComponent(dateChooser1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                                                    .addComponent(btnNewSupplier))
                                            .addContainerGap(19, Short.MAX_VALUE))
                    );
                }

                //======== panel2 ========
                {
                    panel2.setBorder(new TitledBorder("Items Info"));
                    panel2.setPreferredSize(new Dimension(615, 395));

                    //======== scrollPane1 ========
                    {
                        scrollPane1.setViewportView(table1);
                    }

                    //---- btnAdd ----
                    btnAdd.setText("Add Item");
                    btnAdd.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnAddActionPerformed(e);
                        }
                    });

                    //---- btnRemove ----
                    btnRemove.setText("Remove Item");
                    btnRemove.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnRemoveActionPerformed(e);
                        }
                    });

                    //---- label3 ----
                    label3.setText("Total");

                    //---- labelTotal ----
                    labelTotal.setText("0");

                    GroupLayout panel2Layout = new GroupLayout(panel2);
                    panel2.setLayout(panel2Layout);
                    panel2Layout.setHorizontalGroup(
                            panel2Layout.createParallelGroup()
                                    .addGroup(panel2Layout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addComponent(btnAdd, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
                                            .addGap(27, 27, 27)
                                            .addComponent(btnRemove, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                                            .addGap(161, 161, 161)
                                            .addComponent(label3)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 82, Short.MAX_VALUE)
                                            .addComponent(labelTotal)
                                            .addGap(90, 90, 90))
                                    .addGroup(panel2Layout.createSequentialGroup()
                                            .addContainerGap()
                                            .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 643, Short.MAX_VALUE)
                                            .addContainerGap())
                    );
                    panel2Layout.linkSize(SwingConstants.HORIZONTAL, new Component[]{btnAdd, btnRemove});
                    panel2Layout.setVerticalGroup(
                            panel2Layout.createParallelGroup()
                                    .addGroup(panel2Layout.createSequentialGroup()
                                            .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 289, Short.MAX_VALUE)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(panel2Layout.createParallelGroup()
                                                    .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                            .addComponent(btnAdd)
                                                            .addComponent(btnRemove))
                                                    .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                            .addComponent(label3)
                                                            .addComponent(labelTotal)))
                                            .addGap(25, 25, 25))
                    );
                }

                //---- btnRecord ----
                btnRecord.setText("Record");
                btnRecord.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnRecordActionPerformed(e);
                    }
                });

                //---- btnCancel ----
                btnCancel.setText("Cancel");
                btnCancel.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnCancelActionPerformed(e);
                    }
                });

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(contentPanelLayout.createParallelGroup()
                                                .addGroup(GroupLayout.Alignment.TRAILING, contentPanelLayout.createSequentialGroup()
                                                        .addComponent(btnRecord, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 477, Short.MAX_VALUE)
                                                        .addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
                                                        .addGap(32, 32, 32))
                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                        .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 665, GroupLayout.PREFERRED_SIZE)
                                                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                );
                contentPanelLayout.setVerticalGroup(
                        contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 375, GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                .addComponent(btnCancel)
                                                .addComponent(btnRecord))
                                        .addContainerGap(1, Short.MAX_VALUE))
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}

class StockTableModel extends DefaultTableModel {
    public StockTableModel(Object[] columnNames, int rowCount) {
        super(columnNames, rowCount);
    }

    @Override
    public Class getColumnClass(int columnIndex) {
//        return ((Class) dataVector.get(columnIndex)).getClass();
        if (columnIndex == 0 || columnIndex == 2) {
            return String.class;
        } else return Integer.class;
    }

    @Override
    public void addRow(Object[] rowData) {
        super.addRow(new Object[]{rowData[0], rowData[1], rowData[2], new Integer(0), rowData[4]});
    }
}


class MyTableCellEditor extends AbstractCellEditor implements TableCellEditor {
    // This is the component that will handle the editing of the cell value
    JComponent component = new JTextField();
    JTable jTable = null;
    int rowIndex, columnIndex;

    public MyTableCellEditor() {

    }

    // This method is called when a cell value is edited by the user.
    public Component getTableCellEditorComponent(JTable table, Object value,
                                                 boolean isSelected, int rowIndex, int vColIndex) {
        // 'value' is value contained in the cell located at (rowIndex, vColIndex)
        jTable = table;
        this.rowIndex = rowIndex;
        this.columnIndex = columnIndex;
        if (isSelected) {
            // cell (and perhaps other cells) are selected
        }

        // Configure the component with the specified value
        ((JTextField) component).setText((String) value);

        // Return the configured component
        return component;
    }

    // This method is called when editing is completed.
    // It must return the new value to be stored in the cell.
    public Object getCellEditorValue() {
//        jTable.getModel().setValueAt("Test",rowIndex, 0);
        if (columnIndex == 1) {

        }

        return ((JTextField) component).getText();
    }
}

