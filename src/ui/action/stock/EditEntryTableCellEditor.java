package ui.action.stock;

import core.debug.Debug;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Tareq on 9/9/2014.
 */
public class EditEntryTableCellEditor extends AbstractCellEditor implements TableCellEditor {
    static ArrayList<String> productList;

    {
        productList = fetchProducts();
    }

    JTable table;
    Object prevValue;
    int row, column;
    int id;
    Object value = null;
    private Component component;

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        this.prevValue = value;
        this.table = table;
        this.row = row;
        this.column = column;
        this.id = Integer.parseInt(table.getValueAt(row, 0).toString());
        if (column == 0)
            return null;
        if (column == 1) {
            component = new JComboBox(productList.toArray());
            return component;
        } else {
            component = new JTextField(value.toString());
            return component;
        }
    }

    @Override
    public Object getCellEditorValue() {
        Connection connection = null;
        Statement statement = null;
        if (component instanceof JComboBox) {
            value = ((JComboBox) component).getSelectedItem();
        } else {
            value = ((JTextField) component).getText().toString();
        }
        table.clearSelection();
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            String sql = generateSQL();
            statement.executeUpdate(sql);

            connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            if (e.getMessage().contains("unique"))
                JOptionPane.showMessageDialog(table, "Serial Number already exists on database. Check your input");
            else
                JOptionPane.showMessageDialog(table, e.getMessage());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(table, "Only number is allowed in this field. Check your input");
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return value.toString();
    }


    private String generateSQL() throws NumberFormatException {
        String string = "UPDATE STOCK_INVENTORY SET ";

        if (column == 1) {
            string = string + "PRODUCT = '" + value.toString() + "' ";
        } else if (column == 2) {
            string = string + "SERIAL = " + Integer.parseInt(value.toString()) + " ";
        } else if (column == 3) {
            string = string + "MAN_ID = '" + value.toString() + "' ";
        } else if (column == 4) {
            string = string + "WARRANTY = " + Integer.parseInt(value.toString()) + " ";
        } else {
            string = string + "COST = " + Integer.parseInt(value.toString()) + " ";
        }
        string = string + " WHERE ID = " + id + " AND SOLD = 'false';";
        Debug.print(string, "EditTableSQL");
        return string;
    }


    private ArrayList<String> fetchProducts() {
        ArrayList<String> list = new ArrayList<String>();
        Connection connection = null;
        Statement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:records.db");
            statement = connection.createStatement();
            String sql = "SELECT NAME FROM PRODUCTS;";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                list.add(resultSet.getString("NAME"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }
}
