package ui.action.stock;

import core.debug.Debug;

/**
 * Created by Muntasir on 4/27/2014.
 */
public class ItemRowStockTable {
    private String product;
    private int serial;
    private int warranty;
    private int price;
    private String date;
    private long invoice_id;
    private String manID;

    public ItemRowStockTable(String pro, int sn, long inv, int pri, int war, String dt, String manID) {
        product = pro;
        serial = sn;
        invoice_id = inv;
        price = pri;
        warranty = war;
        date = dt;
        Debug.print(date, "Date as in Itemrow Object");
        this.manID = manID;
    }

    public String generateInsertSQLforStockEntry() {
        return "INSERT INTO STOCK_INVENTORY (PRODUCT, SERIAL, INVOICE_ID, COST, DATE, WARRANTY, MAN_ID) VALUES('" + product + "', " + serial + " , " + invoice_id + ", " + price + ", '" + date + "', " + warranty + ",  '" + manID + "');";
    }


}
